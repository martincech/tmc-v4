#!/bin/bash -e
#create pull request from source to destination (parameters) on all submodules
if [ $# -lt 2 ]
  then
    echo "usage: " $0 "source destination [title] [reviewers] [close source after merge]"
    exit
fi

source=$1
dest=$2
title=${3:-"Auto generated pull request"}
reviewers=${4:-""}
close=${5:-true}
subrepos=$(git config --file .gitmodules --get-regexp path | awk '{ print $2 }')

generate_post_json()
{
  reviewers_transformed=""
  for reviewer in ${reviewers} ; do
    item=$(cat <<EOF
{"uuid": "$reviewer"}
EOF
)
    if [[ -z  $reviewers_transformed  ]]
    then reviewers_transformed+=$item
    else reviewers_transformed+=","$item
    fi
    
  done
  cat <<EOF
{
  "title": "$title",
  "source": {"branch": {"name": "$source"}},
  "destination": {"branch": {"name": "$dest"}},  
  "close_source_branch": $close,
  "reviewers": [$reviewers_transformed]
}
EOF
}

for subrepo in ${subrepos} ; do
   url=https://api.bitbucket.org/2.0/repositories/$COMPANY_ACCOUNT_NAME/$subrepo/pullrequests      
   cd $subrepo   
   curl $url --silent --show-error -u $BB_API_AUTH_STRING -X POST -H "Content-Type:application/json" -d "$(generate_post_json)"
   cd ..
done
