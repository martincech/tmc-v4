#!/bin/bash
# switches all the submodules to branches named the same as current branch in top repo
# and updates the .gitmodule and does push

branch=$(git branch | grep \* | cut -d ' ' -f2)
subrepos=$(git config --file .gitmodules --get-regexp path | awk '{ print $2 }')

git submodule foreach --recursive "git checkout $branch" # to current branch

for subrepo in ${subrepos} ; do
   git config -f .gitmodules submodule.$subrepo.branch $branch
done

git commit -a -m ".gitmodules update [skip ci]"
git push --recurse-submodules=on-demand
git push