#!/bin/bash -e
#get info about pull request
if [ $# -lt 1 ]
  then
    echo "usage: " $0 "PR_ID"
    exit
fi

id=$1 #id of existing pull request
url=https://api.bitbucket.org/2.0/repositories/$COMPANY_ACCOUNT_NAME/$BITBUCKET_REPO_SLUG/pullrequests/$BITBUCKET_PR_ID

echo $BB_API_AUTH_STRING
json=$(curl $url -u $BB_API_AUTH_STRING)
title=$(echo $json | jq -r '.title')
close_source_branch=$(echo $json | jq -r '.close_source_branch')
reviewers=$(echo $json | jq -r '.reviewers[] .uuid')
sourcebranch=$(echo $json | jq -r '.source.branch.name')
destinationbranch=$(echo $json | jq -r '.destination.branch.name')

_ci/submodules_pull_request.sh "$sourcebranch" "$destinationbranch" "$title" "$reviewers" "$close_source_branch"