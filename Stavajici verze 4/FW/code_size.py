import sys
import re
import argparse
import csv

parser = argparse.ArgumentParser(description='Process .m51 file')
parser.add_argument('-f', dest="file", required=True, help='file to parse')

args = parser.parse_args()

with open(args.file, "r") as file:
   lines = file.readlines()
   lines = list(map(lambda l: l.replace('\n','') ,lines))
   lines = list(map(lambda l: re.sub(' +', ' ',l).lstrip(' ') ,lines))

header = ["TYPE", "BASE", "LENGTH", "RELOCATION", "SEGMENT"]

def processSection(lines, sectionName, validTypes):
   stars = "* * * * * * *"
   sectionsEnd= "OVERLAY MAP OF MODULE"
   sectionHeader = stars+" " + ' '.join(list(sectionName))
   overflowHeader = "*** ERROR 107: ADDRESS SPACE OVERFLOW"
   overflowRestLines = 0
   section=[]
   item={}
   fr=0
   to=1

   for line in lines:
      #get indices of section
      if line.startswith(stars) or line.startswith(sectionsEnd):
         if line.startswith(sectionHeader):
            fr = lines.index(line)
         elif fr != 0 and to == 1:
            to = lines.index(line)
      #process overflow data   
      if line.startswith(overflowHeader):
         overflowRestLines = 3
         for h in header:
            item.update(
               {h: ""}
            )
      elif overflowRestLines != 0:
         columns=line.split(':')
         columns = list(map(lambda c: c.strip(' '), columns))
         overflowRestLines -= 1
         if columns[0] == "SPACE":
            if columns[1] != sectionName:
               overflowRestLines = 0
               item = {}
         if columns[0] == "SEGMENT":
            item["SEGMENT"] = columns[1]
            item["TYPE"] = sectionName
         if columns[0] == "LENGTH":
            item["LENGTH"] = columns[1]
            item.update({"OVERFLOW": columns[1]})      
      
      if overflowRestLines == 0 and item != {}:
         section.append(item)
         item = {}

   item = {"OVERFLOW": ""}
   #process section data   
   for i in range(fr, to-1):
      line = list(filter(None, lines[i].split(' ')))
      if len(line) == 0:
         continue
      if  line[0] not in validTypes:
         if len(section)!= 0:
            if header[len(header)-1] not in section[len(section)-1]:
               section[len(section)-1].update({ header[len(header)-1]: lines[i].replace(">",'')})
            else:
               section[len(section)-1][header[len(header)-1]]+= lines[i].replace(">",'')
         continue   

      for c in range(0, len(line)):
         if(c > len(header)-1):
            item[header[len(header)-1]]+= " " + line[c]
         else:
            item.update(
               {header[c]: line[c]}
            )
      section.append(item)
      item = {"OVERFLOW": ""}
   
   return section

def sumSection(section):   
   sum=0
   sumOverflow=0
   bitbases=[]
   pattern="^([0-9a-fA-F]+)H(\.([\d])){0,1}$"
   for item in section:
      try:
         lenMatch=re.match(pattern, item['LENGTH'])
         baseMatch=re.match(pattern, item['BASE'])         
         if item['TYPE'] != 'BIT':
            l=int(lenMatch[1], 16)   
            if item['BASE'] != '':         
               item['BASE']=int(baseMatch[1], 16)
               sum+=l
         else:
            if baseMatch[1] not in bitbases:
               bitbases.append(baseMatch[1])
            l=0.125
            
         item['LENGTH']=item['LENGTH']=str(l).replace('.',',')         
         if item['OVERFLOW'] != "":
            item['OVERFLOW']=item['LENGTH']
            sumOverflow += l

      except Exception:{
         print("SUM exception")
      }

   return sum + len(bitbases), sumOverflow

def localize_floats(row):
    return [
        str(el).replace('.', ',') if isinstance(el, float) else el 
        for el in row
    ]
    
dataMemory = processSection(lines, "DATA", ["DATA", "REG", "BIT", "IDATA"])
xdataMemory = processSection(lines, "XDATA", ["XDATA"])
codeMemory = processSection(lines, "CODE", ["CODE"])

sortlambda = lambda k: (k['OVERFLOW'], k['LENGTH'])
dataMemory = sorted(dataMemory, key=sortlambda, reverse=True) 
xdataMemory = sorted(xdataMemory, key=sortlambda, reverse=True) 
codeMemory = sorted(codeMemory, key=sortlambda, reverse=True) 

dataMemorySize, dataMemoryOverflow = sumSection(dataMemory)
xdataMemorySize, xdataMemoryOverflow = sumSection(xdataMemory)
codeMemorySize, codeMemoryOverflow = sumSection(codeMemory)

header.append("OVERFLOW")
summaryHeader=['Section','Size','Maximum','Free', 'Overflow']
summary= [
   {
      "Section": "Data memory",
      "Size": dataMemorySize,
      'Maximum':256,
      'Free': "=C2-B2",
      'Overflow': dataMemoryOverflow
   },
   {
      "Section": "XData memory",
      "Size": xdataMemorySize,
      'Maximum':1792,
      'Free': "=C3-B3",
      'Overflow': xdataMemoryOverflow
   },
   {
      "Section": "Code memory",
      "Size": codeMemorySize,
      'Maximum':65536,
      'Free': "=C4-B4",
      'Overflow': codeMemoryOverflow
   }
]
csv_file = "code_size.csv"
try:
   with open(csv_file, 'w', newline='') as csvfile:
      writer = csv.DictWriter(csvfile, fieldnames=summaryHeader, delimiter=';')
      writer.writeheader()
      writer.writerows(summary)      
      writer = csv.DictWriter(csvfile, fieldnames=header, delimiter=';')
      
      csvfile.write("\n* * * * * * * D A T A M E M O R Y * * * * * * *\n")
      writer.writeheader()
      writer.writerows(dataMemory)
      
      csvfile.write("\n* * * * * * * X D A T A M E M O R Y * * * * * * *\n")
      writer.writeheader()
      writer.writerows(xdataMemory)

      csvfile.write("\n* * * * * * *  C O D E M E M O R Y * * * * * * *\n")
      writer.writeheader()
      writer.writerows(codeMemory)

except IOError:
    print("I/O error") 
