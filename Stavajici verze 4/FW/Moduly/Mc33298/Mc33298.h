//*****************************************************************************
//
//    Mc33298.h   -  MC33298 power digital output
//    Version 1.0   (c) VymOs
//
//*****************************************************************************

#ifndef __Mc33298_H__
   #define __Mc33298_H__

#ifndef __Hardware_H__
   #include "Hardware.h"
#endif

// usporadani vystupu :
// LSB je OP0
// MSB je OP7
// signal 1 je zapnuto tj. pripojit k zemi
// signal 0 je vypnuto tj. rozpojit od zeme

//-----------------------------------------------------------------------------
// Funkce
//-----------------------------------------------------------------------------

void DigitalOutInit( void);
// Inicializace modulu

byte DigitalOutWrite( byte Value);
// Zapis vystupu, vraci NO pri chybe

#define DigitalOutError( ReqValue, RetValue)  ((ReqValue & DOUT_MASK) ^ (RetValue & DOUT_MASK))
// Kontroluje navratovou hodnotu DoutWrite <RetValue> proti pozadovanemu stavu
// vystupu <ReqValue>. Vraci YES pri nesouhlasu

#endif
