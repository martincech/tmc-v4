//*****************************************************************************
//
//    TouchCtl.c  -  Prace s touchpanelem
//    Verze 1.0
//
//*****************************************************************************

#ifndef __TouchCtl_H__
   #define __TouchCtl_H__

#include "Hardware.h"

#define TOUCHCTL_ID_EMPTY       0xFF  // Id neplatneho prvku

// Jeden prvek umisteny na displeji
typedef struct {
  byte Id;         // Identifikace prvku, nesmi byt
  word X;          // Souradnice prvku v pixelech
  word Y;          // Souradnice prvku v pixelech
  word Width;      // Sirka prvku v pixelech
  word Height;     // Vyska prvku v pixelech
  byte AutoRepeat; // YES/NO zda obsluhovat prvek pri autorepeatu
} TTouchButton;

// Aktulani pracovni oblast na displeji, kterou touchpanel ovlada
typedef struct {
  TTouchButton code *Buttons;   // Ukazatel na pole prvku
  byte Count;                   // Pocet prvku v poli Buttons
} TTouchWorkplace;


byte TouchCtlCheck(TTouchWorkplace code *Workplace);
  // Otestuje zadane souradnice v zadane pracovni oblasti, pokud souradnice padnou do nejakeho prvku, vrati jeho Id.
  // <AutoRepeat> znaci, zda byl stisk s autorepeatem nebo bez
  // Pokud souradnice nepadnou nikam, vrati TOUCHCTL_ID_EMPTY

#endif

