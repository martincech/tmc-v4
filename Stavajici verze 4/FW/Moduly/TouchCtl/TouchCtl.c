//*****************************************************************************
//
//    TouchCtl.c  -  Prace s touchpanelem
//    Verze 1.0
//
//*****************************************************************************

#include "Ads7846\Ads7846.h"    // touch screen
#include "TouchCtl\TouchCtl.h"

//-----------------------------------------------------------------------------
// Test souradnic
//-----------------------------------------------------------------------------

byte TouchCtlCheck(TTouchWorkplace code *Workplace) {
  // Otestuje zadane souradnice v zadane pracovni oblasti, pokud souradnice padnou do nejakeho prvku, vrati jeho Id.
  // <AutoRepeat> znaci, zda byl stisk s autorepeatem nebo bez
  // Pokud souradnice nepadnou nikam, vrati TOUCHCTL_ID_EMPTY
  byte j;
  TTouchButton code *Button;

  for (j = 0; j < Workplace->Count; j++) {
    Button = &Workplace->Buttons[j];
    if (Touch.Repeat && !Button->AutoRepeat) {
      continue;   // Tento prvek nechci s autorepeatem, nezpracovavam ho
    }
    if (Touch.X >= Button->X && Touch.X <= Button->X + Button->Width && Touch.Y >= Button->Y && Touch.Y <= Button->Y + Button->Height) {
      return Button->Id;
    }
  }//for
  return TOUCHCTL_ID_EMPTY;  // Souradnice nikam nepadly
}
