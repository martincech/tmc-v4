//*****************************************************************************
//
//    Pg320240.c - LCD displej PG320240 s radicem SED1335 (320 x 240 bodu) a s touchpanelem
//    Version 1.0
//
//*****************************************************************************

#include "System.h"
#include "Sed1335\Sed1335.h"   // Radic
#include "Pg320240\Pg320240.h"
#include "Ads7846\Ads7846.h"   // touch screen
#include "TouchCtl\TouchCtl.h"  // Pracovni plocha touch screenu


TBlink __xdata__ Blink;

static byte __xdata__ PosledniSloupec[28];  // Posledni vykresleny sloupec ve fci DisplayCharTahoma18() - slouzi pro vykreslovani proporcionalnich fontu i bez cteni dat z displeje

// Musi definovat hlavni program:
extern byte code STR_OK[];
extern byte code STR_OPAKOVAT[];
extern byte code STR_ZRUSIT[];
extern byte code STR_ANO[];
extern byte code STR_NE[];
extern byte code SYMBOL_DEL[];
void BeepKey();       // Pipnuti po stisku klavesy
byte WaitForKey();    // Cekani na stisk klavesy (touchpanelu)


//-----------------------------------------------------------------------------
// Svisla cara
//-----------------------------------------------------------------------------

#ifdef DISPLAY_VERT_LINE

void DisplayVertLine(byte X, byte Y, byte Vyska, byte Value) {
  // Nakresli svislou caru osmi bitu do nastavene roviny od nastavene pozice.
  // Vyska je vyska v pixelech, <Value> je hodnota osmice
  SetCursor(DisplayOffset(X, Y));
  DisplayWriteCommand(DISPLAY_CMD_MWRITE);
  while (Vyska--) {
    DisplayWriteData(Value);
  }
}

#endif

//-----------------------------------------------------------------------------
// Svisla carkovana cara
//-----------------------------------------------------------------------------

#ifdef DISPLAY_VERT_DOT_LINE

void DisplayVertDotLine(byte X, byte Y, byte Vyska, byte Value) {
  // Nakresli svislou caru osmi bitu do nastavene roviny od nastavene pozice.
  // Vyska je vyska v pixelech, <Value> je hodnota osmice
  SetCursor(DisplayOffset(X, Y));
  DisplayWriteCommand(DISPLAY_CMD_MWRITE);
  for (Y = 0; Y < Vyska; Y += 2) {
    // Je to carkovana cara => jeden radek zapisu, druhy vynecham (tj. zapisu nulu)
    DisplayWriteData(Value);
    DisplayWriteData(0);
  }//for y
}

#endif

//-----------------------------------------------------------------------------
// Vodorovna cara
//-----------------------------------------------------------------------------

#ifdef DISPLAY_HOR_LINE

void DisplayHorLineValue(byte X, byte Y, byte Sirka, byte Hodnota) {
  DisplayWriteCommand(DISPLAY_CMD_CSR_RIGHT);   // Posuv v pameti doprava
  SetCursor(DisplayOffset(X, Y));
  DisplayWriteCommand(DISPLAY_CMD_MWRITE);
  while (Sirka--) {
    DisplayWriteData(Hodnota);
  }
  DisplayWriteCommand(DISPLAY_CMD_CSR_DOWN);    // Zpet na default posuv dolu
}

#endif

//-----------------------------------------------------------------------------
// Symbol
//-----------------------------------------------------------------------------

#ifdef DISPLAY_SYMBOL

void DisplaySymbol(byte X, byte Y, byte code *Symbol) {
  // Zobrazi symbol na pozici X, Y (X je osmice)
  DisplayMoveTo(X, Y);
  DisplayPattern(&Symbol[2], Symbol[0], Symbol[1]);
}

#endif

//-----------------------------------------------------------------------------
// Proporcionalni font
//-----------------------------------------------------------------------------

#ifdef DISPLAY_CHAR_TAHOMA18

void DisplayCharTahoma18(word X, byte Y, byte Znak) {
  // Zobrazi znak Znak na pozici X,Y - proporcionalni
  // Vyska znaku v pixelech je konstantni
  #define VYSKATAHOMA18 28
  byte x;
  byte y;
  bit Zmena=0;               // Flag, ze je treba prejit na novy radic nebo stranku
  byte Bajt;      // Bajt,ktery postupne tvorikm a pak jej zapisu do displeje
  byte Maska;   // Maska bitu, ktery prave vytvarim
  byte PocetBajtu=1;  // Pocet bajtu, ktere jsem uz z definice znaku nacetl (schvalne zacinam od 1, abych vynechal sirku znaku)
  byte MaskaDefinice=128;  // Maska pro definici znaku
  bit Zapsat=1;            // Flag, zda mam vytovreny bajt zapsat nebo ne. Pokud je to mimo zobrazeni (y>127), nesmim vytvoreny bajt zapsat
  byte BajtDefinice;  // Bajt nacteny z pole definice znaku - pro urychleni

  byte xdata Pozadi[4][VYSKATAHOMA18];  // Pozadi znaku - max. sirku ma znak "W" - 22px, takze se muze rozprostrit do sirky 4*8px

  byte StartX;
  byte ZbytekX;  // Zbytek souradnice X po deleni osmi - znaci, kde v bajtu zacinam
  byte PosledniX, PosledniY;
  word DisplejOffset;
  byte PocatecniMaska;

  Znak-=32;    // Zacinam az od mezery

  StartX=(word)(X/8);
  ZbytekX=(word)((word)X-(word)8*(word)StartX);   // Operator % pro int neni definovan => musim vypocitat zbytek rucne

  for (y=0; y<VYSKATAHOMA18; y++) {
    if (ZbytekX>0) Pozadi[0][y]=PosledniSloupec[y]; else Pozadi[0][y]=0;
    for (x=1; x<4; x++) Pozadi[x][y]=0;
  }

  BajtDefinice=FONT_TAHOMA18[Znak][1];

  // Nactu bajt z displeje
  Bajt=Pozadi[0][0];

  PosledniX=0;
  PosledniY=0;

  PocatecniMaska=128;
  for (x=0; x<ZbytekX; x++) PocatecniMaska/=2;

  for (y=0; y<VYSKATAHOMA18; y++) {
    // Nastavim pocatecni masku
    Maska=PocatecniMaska;

    for (x=ZbytekX; x<ZbytekX+FONT_TAHOMA18[Znak][0]; x++) {  // Prvni bajt v definici znaku je jeho sirka
      // Pokud je y mimo zobrazeni, tak vytvoreny bajt do displeje nezapisuju. Schvalne tady mam >64, i kdyz 64 je uz mimo displej. Pri y=64 to totiz
      // v predchozim kroku dovytvorilo posledni bajt, ktery se ma jeste zobrazit a ted, pri y=64, to tam teprve zapise.
      if (y>PosledniY) Zmena=1;  // Prechazim na dalsi radek, kdyz pri vytvareni posledniho bajtu vpravo na predhcozim radku jsem jej cely nedokoncil,
                                 // tj. Maska koncila napr. s hodnotou 8.
      if (y>240) Zapsat=0;
      if (Zmena) {
        Zmena=0;  // Shodim flag
        // Musim zapsat dosud vytvoreny bajt, protoze se budu posouvat a vytvaret novy bajt od zacatku
        if (Zapsat) Pozadi[PosledniX/8][PosledniY]=Bajt;
        Zapsat=1;
        // Vypoctu nove hodnoty pro novy radic/stranku
        // A bajt ted tvorim odznovu
        if (x==ZbytekX) Maska=PocatecniMaska;   // Pro znaky se sirkou mensi nez 8px
          else Maska=128;
        // Nactu bajt z displeje
        Bajt=Pozadi[x/8][y];
      }//if
      // Pridam do bajtu aktualni bit
      if (BajtDefinice & MaskaDefinice) Bajt|=Maska;  // Nastavim bit
//        else Bajt&=(~Maska);  // Vynuluju bit
      if (Maska==1) {
        Maska=128;
        Zmena=1;  // Musim zapsat
      } else {
        Maska>>=1;
      }//else
      // Prejdu na dalsi bit v definici znaku
      if (MaskaDefinice==1) {
        // Musim uz na dalsi bajt
        PocetBajtu++;
        MaskaDefinice=128;
        BajtDefinice=FONT_TAHOMA18[Znak][PocetBajtu];
      } else {
        // Posunu se ve stavajicim bajtu
        MaskaDefinice>>=1;
      }//else
      PosledniX=x;
      PosledniY=y;
    }//for y
    // Pokud by byla vyska znaku mensi nez 1 bajt (tj 8 bodu), musel bych ted vyvolat zmenu rucne, protoze by se sama nevyvolala.
  }//for x
  // Dodelal jsem posledni bit posledniho bajtu, v cyklu for to uz nebude pokracovat, takze posledni bajt do displeje musim zapsat rucne. Pritom
  // uz nemusim obnovovvat zadne promenne, protoze ted uz stejne z fce odejdu.
  Pozadi[PosledniX/8][PosledniY]=Bajt;

  // Zapisu vytvorene pole naraz do displeje
  DisplejOffset = DisplayOffset(StartX, Y);
  SetCursor(DisplejOffset);
  for (x=0; x<(ZbytekX+FONT_TAHOMA18[Znak][0])/8+1; x++) {
    // Jeden sloupec
    SetCursor(DisplejOffset);
    DisplayWriteCommand(DISPLAY_CMD_MWRITE);
    for (y=0; y<VYSKATAHOMA18; y++){
      DisplayWriteData(Pozadi[x][y]);
    }
    DisplejOffset++;                 // dalsi sloupec
  }//for x

  // Vyplnim posledni vykresleny sloupec pro dalsi pouziti
  for (y=0; y<VYSKATAHOMA18; y++) PosledniSloupec[y]=Pozadi[(ZbytekX+FONT_TAHOMA18[Znak][0])/8][y];
}

#endif

#if (defined DISPLAY_STRING_TAHOMA18 || defined DISPLAY_STRING_CENTER_TAHOMA18 || defined DISPLAY_NUMPAD)

static void VymazPosledniSloupec() {
  byte i;
  for (i=0; i<TAHOMA18_HEIGHT; i++) PosledniSloupec[i]=0;
}

#endif

#define TAHOMA18_MEZERA 2

#ifdef DISPLAY_STRING_TAHOMA18

void DisplayStringTahoma18(word x, byte y, byte *s) {
  byte i;
  word ii;  // Musi byt int, kvuli sirce 320

  if (s==NULL) return;

  VymazPosledniSloupec();
  i=0;
  while (1) {
    DisplayCharTahoma18(x, y, s[i]);
    ii=x;  // Zapamatuju
    x+=(word)(FONT_TAHOMA18[(byte)s[i]-32][0]+TAHOMA18_MEZERA);
    if ((ii+FONT_TAHOMA18[(byte)s[i]-32][0]-1)/8<x/8) {
      // Pokud nadchazejici znak nezasahuje so osmice predchoziho znaku, nebudu vyuzivat posledni sloupec a proto ho vynuluju. Pokud tedy posledni pixel
      // predchoziho znaku patri do jine osmice nez prvni pixel nasledujiciho znaku.
      VymazPosledniSloupec();
    }
    i++;
    if (s[i]==0) return;  // Konec stringu
  }// for i
}

#endif

#ifdef DISPLAY_STRING_CENTER_TAHOMA18

void DisplayStringCenterTahoma18(word x, byte y, byte *s) {
  // Vykresli vetu zvolenym fontem zarovnanou nastred
  // String je zakonceny nulou nebo FF (FF=konec radku)
  byte i, Delka=0;
  word ii;  // Musi byt int, kvuli sirce 320

  // Najdu delku stringu
  while (!(s[Delka]==0 || s[Delka]==0)) Delka++;

  ii=0;  // Vyuziju jako sirku vety
  for (i=0; i<Delka; i++) ii+=(word)(FONT_TAHOMA18[(byte)s[i]-32][0]+TAHOMA18_MEZERA);
  ii-=TAHOMA18_MEZERA;   // Mezera za poslednim znakem uz tam neni => odectu
  x-=ii/2;

  VymazPosledniSloupec();
  i=0;
  while (1) {
    DisplayCharTahoma18(x, y, s[i]);
    ii=x;  // Zapamatuju
    x+=(word)(FONT_TAHOMA18[(byte)s[i]-32][0]+TAHOMA18_MEZERA);
    if ((ii+FONT_TAHOMA18[(byte)s[i]-32][0]-1)/8<x/8) {
      // Pokud nadchazejici znak nezasahuje so osmice predchoziho znaku, nebudu vyuzivat posledni sloupec a proto ho vynuluju. Pokud tedy posledni pixel
      // predchoziho znaku patri do jine osmice nez prvni pixel nasledujiciho znaku.
      VymazPosledniSloupec();
    }
    i++;
    if (s[i]==0) return;  // Konec stringu
  }// for i
}

#endif

//-----------------------------------------------------------------------------
// Tlacitka
//-----------------------------------------------------------------------------

#ifdef DISPLAY_BUTTON_FRAME

/*void DisplayButtonFrame(byte X, byte Y, byte Width, byte Height, TYesNo Thick) {
  // Zobrazi obrys tlacitka, X a Width je v osmicich pixelu. Pokud je <Thick> = YES, vykresli obrys tucne (3px).
  // Sirka musi byt vetsi nez 2 (nekontroluju)
  byte j;

  if (!Thick) {
    // Musim smazat zbytky po puvodni tluste care (abych mohl rovnou prekreslit tucne tlacitko tenkym)
    for (j = 1; j <= 2; j++) {
      // Horni cara
      DisplayHorLineValue(X, Y + j, Width, 0);
      // Spodni cara
      DisplayHorLineValue(X, Y + Height -1 - j, Width, 0);
    }//for j
  }//if

  // Leva cara
  if (Thick) {
    DisplayVertLine(X, Y + 1, Height - 2, 0xE0);
  } else {
    DisplayVertLine(X, Y + 1, Height - 2, 0x80);
  }
  // Prava cara
  if (Thick) {
    DisplayVertLine(X + Width - 1, Y + 1, Height - 2, 0x07);
  } else {
    DisplayVertLine(X + Width - 1, Y + 1, Height - 2, 0x01);
  }
  // Horni cara
  DisplayHorLineValue(X, Y, 1, 0x7F);             // Leva cast se zaoblenim
  DisplayHorLine(X + 1, Y, Width - 2);            // Uprostred
  DisplayHorLineValue(X + Width - 1, Y, 1, 0xFE);     // Prava cast se zaoblenim
  // Spodni cara
  DisplayHorLineValue(X, Y + Height - 1, 1, 0x7F);         // Leva cast se zaoblenim
  DisplayHorLine(X + 1, Y + Height - 1, Width - 2);        // Uprostred
  DisplayHorLineValue(X + Width - 1, Y + Height - 1, 1, 0xFE); // Prava cast se zaoblenim
  if (!Thick) {
    return;
  }
  // Tlusta cara
  for (j = 1; j <= 2; j++) {
    // Horni cara
    DisplayHorLine(X, Y + j, Width);
    // Spodni cara
    DisplayHorLine(X, Y + Height -1 - j, Width);
  }//for j
}*/

void DisplayButtonFrame(byte X, byte Y, byte Width, byte Height, TYesNo Thick) {
  // Zobrazi obrys tlacitka, X a Width je v osmicich pixelu. Pokud je <Thick> = YES, vykresli obrys tucne (3px).
  // Sirka musi byt vetsi nez 2 (nekontroluju)

  // Smazu cele tlacitko, horni a spodni vodorovnou linku muzu ponechat, stejne ji budu vykreslovat
  DisplayMoveTo(X, Y + 1);
  DisplayClearBox(Width, Height - 2);

  // Svisle cary - bud tucne, nebo tenke
  DisplayVertLine(X, Y + 1, Height - 2, Thick ? 0xE0 : 0x80);
  DisplayVertLine(X + Width - 1, Y + 1, Height - 2, Thick ? 0x07 : 0x01);

  // Tluste vodorovne cary
  if (Thick) {
    DisplayMoveTo(X, Y + 1);
    DisplayBox(Width, 2);
    DisplayMoveTo(X, Y + Height - 3);
    DisplayBox(Width, 2);
  }

  // Horni cara
  DisplayHorLineValue(X, Y, 1, 0x7F);             // Leva cast se zaoblenim
  DisplayHorLine(X + 1, Y, Width - 2);            // Uprostred
  DisplayHorLineValue(X + Width - 1, Y, 1, 0xFE);     // Prava cast se zaoblenim
  // Spodni cara
  DisplayHorLineValue(X, Y + Height - 1, 1, 0x7F);         // Leva cast se zaoblenim
  DisplayHorLine(X + 1, Y + Height - 1, Width - 2);        // Uprostred
  DisplayHorLineValue(X + Width - 1, Y + Height - 1, 1, 0xFE); // Prava cast se zaoblenim
}

#endif

// -------------------------------------------------------------------------------------------------------------------------------------------
// Vykresleni spodnich tlacitek v menu
// -------------------------------------------------------------------------------------------------------------------------------------------

#ifdef DISPLAY_BUTTON_TEXT

void DisplayButtonText(byte X, byte Y, byte Delka, byte code *Text) {
  // Zobrazi tlacitko s textem uprostred, Delka je v osmicich pixelu, vyska konstantni. X je v osmicich, Y je v pixelech
  DisplayButtonFrame(X, Y, Delka, TLACITKO_VYSKA, NO);
  DisplayStringCenterTahoma18(X*8+Delka*8/2, Y+6, Text);
}

#endif

#ifdef DISPLAY_RETRY_CANCEL
void DisplayRetryCancel() {
  // Zobrazi tlacitka Opakovat a Zrusit vespodu displeje
  DisplayButtonText(TLACITKO_X1, TLACITKO_Y, TLACITKO_SIRKA, STR_OPAKOVAT);
  DisplayButtonText(TLACITKO_X2, TLACITKO_Y, TLACITKO_SIRKA, STR_ZRUSIT);
}
#endif

#ifdef DISPLAY_YES_NO
void DisplayYesNo() {
  // Zobrazi tlacitka Ano a Ne vespodu displeje
  DisplayButtonText(TLACITKO_X1, TLACITKO_Y, TLACITKO_SIRKA, STR_ANO);
  DisplayButtonText(TLACITKO_X2, TLACITKO_Y, TLACITKO_SIRKA, STR_NE);
}
#endif

// -------------------------------------------------------------------------------------------------------------------------------------------
// Obsluha spodnich tlacitek v menu
// -------------------------------------------------------------------------------------------------------------------------------------------

#ifdef DISPLAY_EXECUTE_BUTTONS

TTouchButton code TwoButtons[2] = {
  { // Ok (leve)
    0,
    TLACITKO_X1 * 8,
    TLACITKO_Y,
    TLACITKO_SIRKA * 8,
    TLACITKO_VYSKA,
    NO
  },
  { // Cancel (prave)
    1,
    TLACITKO_X2 * 8,
    TLACITKO_Y,
    TLACITKO_SIRKA * 8,
    TLACITKO_VYSKA,
    NO
  }
};

TTouchWorkplace code TwoButtonsWorkplace = {
  TwoButtons,
  2
};

TYesNo DisplayExecuteButtons(byte Count) {
  // Ceka na stisk jednoho z max 2 tlacitek v menu, pri stisku leveho vrati 1, pri stisku praveho nebo timeoutu vrati 0.
  // Count muze byt 1 nebo 2
  while (1) {
    switch (WaitForKey()) {
      case K_TOUCH: {
        switch (TouchCtlCheck(&TwoButtonsWorkplace)) {
          case 0: {
            if (Count == 1) {
              break;  // Pri pouziti 1 tlacitka je toto neplatne
            }
            BeepKey();
            return 1;
          }
          case 1: {
            BeepKey();
            return 0;
          }
        }//switch
        break;
      }
      case K_TIMEOUT: {
        BeepKey();
        return 0;       // Jakoby stisknul Cancel (prave tlacitko)
      }
    }//switch
  }//while
}
#endif

// -------------------------------------------------------------------------------------------------------------------------------------------
// Editace cisla pomoci numericke klavesnice
// -------------------------------------------------------------------------------------------------------------------------------------------

#define NUMPAD_X   24
#define NUMPAD_DX  5     // Sirka 1 klavesy v 8micich pixelu
#define NUMPAD_Y   75
#define NUMPAD_DY  40    // Vyska 1 klavesy
#define NUMPAD_NUMBER_OFFSET_X  16
#define NUMPAD_NUMBER_OFFSET_Y  7
/*#define NUMPAD_DEL 10    // Klavesa Delete
#define NUMPAD_PLUSMINUS 11    // Klavesa plus/minus
#define NUMPAD_OK  12    // Klavesa Ok
#define NUMPAD_ZRUSIT 13    // Klavesa Zrusit*/
#define NUMPAD_OK_X 4
#define NUMPAD_ZRUSIT_X 15
#define NUMPAD_OK_Y 196
#define NUMPAD_OK_WIDTH 5
#define NUMPAD_OK_HEIGHT 40

#ifdef DISPLAY_NUMPAD

byte code SYMBOL_PLUSMINUS[47] = {3,15,1,1,1,15,15,1,1,1,0,0,0,0,0,1,3,128,128,128,240,240,129,131,134,12,24,48,96,195,131,0,0,0,48,96,192,128,0,0,0,0,0,0,252,252,0};
byte code SYMBOL_OK[202] = {5,40,63,64,128,128,128,128,128,128,128,128,128,128,128,128,128,128,128,128,128,128,128,128,128,128,128,128,128,128,128,128,128,128,128,128,128,128,128,128,64,63,255,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,16,56,28,14,7,3,1,0,0,0,0,0,0,0,0,0,0,255,255,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,3,7,14,28,56,112,224,192,128,0,0,0,0,0,0,0,0,0,0,0,255,255,0,0,0,0,0,0,0,0,0,0,0,4,14,28,56,112,224,192,128,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,255,252,2,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,2,252};
byte code SYMBOL_ZRUSIT[202] = {5,40,63,64,128,128,128,128,128,128,128,128,128,128,128,128,128,128,128,128,128,128,128,128,128,128,128,128,128,128,128,128,128,128,128,128,128,128,128,128,64,63,255,0,0,0,0,0,0,0,0,0,0,8,28,14,7,3,1,0,0,0,0,0,0,1,3,7,14,28,8,0,0,0,0,0,0,0,0,0,0,255,255,0,0,0,0,0,0,0,0,0,0,0,0,0,0,129,195,231,126,60,60,126,231,195,129,0,0,0,0,0,0,0,0,0,0,0,0,0,0,255,255,0,0,0,0,0,0,0,0,0,0,16,56,112,224,192,128,0,0,0,0,0,0,128,192,224,112,56,16,0,0,0,0,0,0,0,0,0,0,255,252,2,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,2,252};

void DisplayDrawNumPad(TYesNo Znamenko) {
  // Nakresli na displej cislicovou klavesnici na pevne pozici
  // Pokud je Znamenko=YES, nakresli i klavesu +/-
  word i;

  DisplayVertLine(NUMPAD_X, NUMPAD_Y, 4*NUMPAD_DY, 128);
  if (Znamenko) {
    DisplayVertLine(NUMPAD_X+1*NUMPAD_DX, NUMPAD_Y, 4*NUMPAD_DY, 128);
  } else {
    DisplayVertLine(NUMPAD_X+1*NUMPAD_DX, NUMPAD_Y, 3*NUMPAD_DY, 128);
  }//else
  DisplayVertLine(NUMPAD_X+2*NUMPAD_DX, NUMPAD_Y, 4*NUMPAD_DY, 128);
  DisplayVertLine(NUMPAD_X+3*NUMPAD_DX, NUMPAD_Y, 4*NUMPAD_DY+1, 128);
  for (i = 0; i < 5; i++) {
    DisplayHorLine(NUMPAD_X, NUMPAD_Y+i*NUMPAD_DY, 3*NUMPAD_DX);
  }//for
  // Cislice
  for (i = 0; i < 3; i++) {
    if (i > 0) {
      DisplayCharTahoma18((NUMPAD_X+i*NUMPAD_DX)*8+NUMPAD_NUMBER_OFFSET_X, NUMPAD_Y+2*NUMPAD_DY+NUMPAD_NUMBER_OFFSET_Y, 0x30+i+1);
    }
    DisplayCharTahoma18((NUMPAD_X+i*NUMPAD_DX)*8+NUMPAD_NUMBER_OFFSET_X, NUMPAD_Y+1*NUMPAD_DY+NUMPAD_NUMBER_OFFSET_Y, 0x30+i+4);
    DisplayCharTahoma18((NUMPAD_X+i*NUMPAD_DX)*8+NUMPAD_NUMBER_OFFSET_X, NUMPAD_Y+NUMPAD_NUMBER_OFFSET_Y, 0x30+i+7);
  }//for
  VymazPosledniSloupec();  // U jednicky to blblo
  DisplayCharTahoma18((NUMPAD_X)*8+NUMPAD_NUMBER_OFFSET_X+2, NUMPAD_Y+2*NUMPAD_DY+NUMPAD_NUMBER_OFFSET_Y, 0x31);  // Jednicku resim zvlast
  if (Znamenko) {
    DisplayCharTahoma18((NUMPAD_X+NUMPAD_DX)*8+NUMPAD_NUMBER_OFFSET_X, NUMPAD_Y+3*NUMPAD_DY+NUMPAD_NUMBER_OFFSET_Y, 0x30);  // Nula
    DisplaySymbol(NUMPAD_X+1, NUMPAD_Y+3*NUMPAD_DY+13, SYMBOL_PLUSMINUS);
  } else {
    VymazPosledniSloupec();  // U nuly taky
    DisplayCharTahoma18((NUMPAD_X+NUMPAD_DX)*8-5, NUMPAD_Y+3*NUMPAD_DY+NUMPAD_NUMBER_OFFSET_Y, 0x30);  // Nula
  }//else
  DisplaySymbol(NUMPAD_X+2*NUMPAD_DX+1, NUMPAD_Y+3*NUMPAD_DY+15, SYMBOL_DEL);
  // Nakresli klavesy Ok a Storno pro numpad
  DisplaySymbol(NUMPAD_OK_X, NUMPAD_OK_Y, SYMBOL_OK);
  DisplaySymbol(NUMPAD_ZRUSIT_X, NUMPAD_OK_Y, SYMBOL_ZRUSIT);
}

enum {
  ID_NUMPAD_0,
  ID_NUMPAD_1,
  ID_NUMPAD_2,
  ID_NUMPAD_3,
  ID_NUMPAD_4,
  ID_NUMPAD_5,
  ID_NUMPAD_6,
  ID_NUMPAD_7,
  ID_NUMPAD_8,
  ID_NUMPAD_9,
  ID_NUMPAD_SIGN,
  ID_NUMPAD_DEL,
  ID_NUMPAD_OK,
  ID_NUMPAD_CANCEL,
  _ID_NUMPAD_COUNT
};

TTouchButton code NumpadButtons[_ID_NUMPAD_COUNT] = {
  {
    ID_NUMPAD_7,
    NUMPAD_X * 8,
    NUMPAD_Y,
    NUMPAD_DX * 8,
    NUMPAD_DY,
    NO
  },
  {
    ID_NUMPAD_8,
    (NUMPAD_X + 1 * NUMPAD_DX) * 8,
    NUMPAD_Y,
    NUMPAD_DX * 8,
    NUMPAD_DY,
    NO
  },
  {
    ID_NUMPAD_9,
    (NUMPAD_X + 2 * NUMPAD_DX) * 8,
    NUMPAD_Y,
    NUMPAD_DX * 8,
    NUMPAD_DY,
    NO
  },
  {
    ID_NUMPAD_4,
    NUMPAD_X * 8,
    NUMPAD_Y + 1 * NUMPAD_DY,
    NUMPAD_DX * 8,
    NUMPAD_DY,
    NO
  },
  {
    ID_NUMPAD_5,
    (NUMPAD_X + 1 * NUMPAD_DX) * 8,
    NUMPAD_Y + 1 * NUMPAD_DY,
    NUMPAD_DX * 8,
    NUMPAD_DY,
    NO
  },
  {
    ID_NUMPAD_6,
    (NUMPAD_X + 2 * NUMPAD_DX) * 8,
    NUMPAD_Y + 1 * NUMPAD_DY,
    NUMPAD_DX * 8,
    NUMPAD_DY,
    NO
  },
  {
    ID_NUMPAD_1,
    NUMPAD_X * 8,
    NUMPAD_Y + 2 * NUMPAD_DY,
    NUMPAD_DX * 8,
    NUMPAD_DY,
    NO
  },
  {
    ID_NUMPAD_2,
    (NUMPAD_X + 1 * NUMPAD_DX) * 8,
    NUMPAD_Y + 2 * NUMPAD_DY,
    NUMPAD_DX * 8,
    NUMPAD_DY,
    NO
  },
  {
    ID_NUMPAD_3,
    (NUMPAD_X + 2 * NUMPAD_DX) * 8,
    NUMPAD_Y + 2 * NUMPAD_DY,
    NUMPAD_DX * 8,
    NUMPAD_DY,
    NO
  },
  {
    ID_NUMPAD_SIGN,
    NUMPAD_X * 8,
    NUMPAD_Y + 3 * NUMPAD_DY,
    NUMPAD_DX * 8,
    NUMPAD_DY,
    NO
  },
  {
    ID_NUMPAD_0,
    (NUMPAD_X + 1 * NUMPAD_DX) * 8,
    NUMPAD_Y + 3 * NUMPAD_DY,
    NUMPAD_DX * 8,
    NUMPAD_DY,
    NO
  },
  {
    ID_NUMPAD_DEL,
    (NUMPAD_X + 2 * NUMPAD_DX) * 8,
    NUMPAD_Y + 3 * NUMPAD_DY,
    NUMPAD_DX * 8,
    NUMPAD_DY,
    NO
  },
  {
    ID_NUMPAD_OK,
    NUMPAD_OK_X * 8,
    NUMPAD_OK_Y,
    NUMPAD_OK_WIDTH * 8,
    NUMPAD_OK_HEIGHT,
    NO
  },
  {
    ID_NUMPAD_CANCEL,
    NUMPAD_ZRUSIT_X * 8,
    NUMPAD_OK_Y,
    NUMPAD_OK_WIDTH * 8,
    NUMPAD_OK_HEIGHT,
    NO
  }
};

TTouchWorkplace code NumpadWorkplace = {
  NumpadButtons,
  _ID_NUMPAD_COUNT
};

static byte CheckNumPad(TYesNo Znamenko) {
  // Zkontroluje, zda zadane souradnice stisku odpovidaji nejake klavese
  byte b;

  b = TouchCtlCheck(&NumpadWorkplace);
  switch (b) {
    case TOUCHCTL_ID_EMPTY: {
      return K_IDLE;
    }
    case ID_NUMPAD_SIGN: {
      if (Znamenko) {
        return ID_NUMPAD_SIGN;
      } else {
        return ID_NUMPAD_0;         // Nechce znamenko, na jeho pozici je 0
      }
    }
    default: {
      return b;
    }
  }
}

TYesNo DisplayExecuteNumPad(byte X, byte Y, byte Delka, TYesNo Znamenko) {
  // Edituje cislo UN.DT, Y je cislo radku. Vyuziva font Tahoma18 s neproporcionalni sirkou.
  // Pokud je Delka=1-8, edituje se cislo o zadane delce bez desetinne carky.
  // Pokud je Delka=DISPLAY_EDIT_DATE, edituje se datum. Pokud je Delka=DISPLAY_EDIT_TIME, edituje se cas.
  // Pokud je delka>10, je v MSB delka a v LSB pocet desetin, takze napr. 0x42 edituje cislo o delce 4 se 2 des. carkami
  // Pokud je Znamenko=YES, edituje se cislo se znamenkem (nelze pouzit u data a casu)

  // Sirka jedne cislice
  #define EDITACESIRKAZNAKU 2
  // Mezera mezi jednotlivymi cislicemi
  #define EDITACESIRKAOKRAJE 0

  byte __xdata__ SirkaObdelniku;
  byte __xdata__ Znaky[8];  // Jednotlive znaky
  byte __xdata__ Index;           // Index cislice, kterou prave edituju
  word __xdata__ AktualniX;       // Aktualni souradnice x, na kterou budu zapisovat
  byte __xdata__ i;
  bit Zobrazit=1;
  bit EditaceCasu=0, EditaceDatumu=0;
  unsigned long __xdata__ ZalohaUN=UN.DT;
  byte PocetDesetin=0;  // Pocet desetin editovaneho cisla - odvodi se z delky cisla
  bit Zaporne;

  // Nastavim font
  DisplaySetFont(FONT_NUM_TAHOMA18);

  if (Delka==DISPLAY_EDIT_DATE) {
    EditaceDatumu=1;
    Delka=8;
  } else if (Delka==DISPLAY_EDIT_TIME) {
    EditaceCasu=1;
    Delka=4;
  } else if (Delka>10) {
    // Jde o cislo s desetinou carkou
    PocetDesetin=Delka&0x0F;  // Pocet desetin je v LSB
    if (PocetDesetin>7) PocetDesetin=0;
    Delka>>=4;  // Posunu MSB na misto LSB
    if (Delka>8) Delka=8;
  }

  // Zjistim znamenko
  if ((UN.ST.X1 & 0x80) != 0) {
    Zaporne=1;
    UN.DT=~UN.DT + 1;
  } else {
    Zaporne=0;
  }//else

  // Vypoctu sirku obdelniku
  SirkaObdelniku=Delka*EDITACESIRKAZNAKU + 2*EDITACESIRKAOKRAJE;
  // Udelam pripadne korekce pro editaci datumu a casu
  if (EditaceDatumu) {
    SirkaObdelniku+=2*EDITACESIRKAZNAKU;  // Musim pripocist 2 tecky
  } else if (EditaceCasu || PocetDesetin>0) {
    SirkaObdelniku+=EDITACESIRKAZNAKU;  // Musim pripocist 1 dvojtecku, pripadne 1 desetinou carku
  }//else

  // Rozlozim cislo na jednotlive znaky
  UN.DT = bindec(UN.DT);
  Znaky[7] = hnib(UN.ST.X1);
  Znaky[6] = lnib(UN.ST.X1);
  Znaky[5] = hnib(UN.ST.X2);
  Znaky[4] = lnib(UN.ST.X2);
  Znaky[3] = hnib(UN.ST.X3);
  Znaky[2] = lnib(UN.ST.X3);
  Znaky[1] = hnib(UN.ST.X4);
  Znaky[0] = lnib(UN.ST.X4);

  for (Index=Delka; Index<8; Index++) Znaky[Index]=0;

  DisplaySetPlane(DISPLAY_PLANE2);
  DisplayMoveTo(X-1, Y);
  DisplayBox(SirkaObdelniku+2, 28);
  DisplaySetPlane(DISPLAY_PLANE1);

  Index=Delka-1;
  while (1) {
    switch(WaitForKey()) {
      case K_TOUCH: {
        if (Touch.Repeat) break;   // Nechci autorepeat
        i = CheckNumPad(Znamenko);
        if (i < ID_NUMPAD_SIGN) {
          BeepKey();
          Znaky[Index]=i;
          if (Index>0) Index--; else Index=Delka-1;   // Posunu se na dalsi znak
          Zobrazit=1;
          // Rucne to nastavim tak, aby se cislice zrovna zobrazila a blikla az za celou periodu blikani
          Blink.Show=1;
          Blink.Change=0;  // Stejne budu prekreslovat => nemusim nastavovat flag
        } else {
          switch (i) {
            case ID_NUMPAD_DEL: {
              BeepKey();
              for (Index=0; Index<8; Index++) Znaky[Index]=0;
              Index=Delka-1;
              Zobrazit=1;
              break;
            }
            case ID_NUMPAD_SIGN: {
              BeepKey();
              Zaporne=!Zaporne;
              Zobrazit=1;
              break;
            }
            case ID_NUMPAD_CANCEL: {
              UN.DT=ZalohaUN;
              BeepKey();
              return 0;
            }
            case ID_NUMPAD_OK: {
              BeepKey();
              UN.DT=(unsigned long)(10000000*(unsigned long)Znaky[7]+1000000*(unsigned long)Znaky[6]+100000*(unsigned long)Znaky[5]+10000*(unsigned long)Znaky[4]
                    +1000*(unsigned long)Znaky[3]+100*(unsigned long)Znaky[2]+10*(unsigned long)Znaky[1]+(unsigned long)Znaky[0]);
              if (Zaporne) UN.DS=-UN.DS;
              return 1;
            }
          }//switch
        }//else
        break;
      }
      case K_TIMEOUT: {
        UN.DT=ZalohaUN;
        BeepKey();
        return 0;
      }
    }//switch
    if (Blink.Change) {
      // Bud mam aktualni cislici skryt nebo zobrazit
      Zobrazit=1;  // Nastavim flag, aby prekreslil
      Blink.Change=0;  // Shodim flag
    }
    if (Zobrazit) {
      Zobrazit=0;  // Shodim flag
      DisplayMoveTo(X, Y+5);
      DisplayClearBox(SirkaObdelniku, 18);
      AktualniX=X+SirkaObdelniku-EDITACESIRKAOKRAJE-EDITACESIRKAZNAKU;  // Souradnice cislice uplne vpravo, tj. jednotek
      for (i=0; i<Delka; i++) {  // Vykreslim jednotlive cislice
        if (EditaceDatumu && (i==4 || i==6)) {
          // Edituju datum a prave mam vykreslit tecku
          DisplayMoveTo(AktualniX, Y + 5);
          putchar('.');
          AktualniX-=EDITACESIRKAZNAKU;
        } else if (EditaceCasu && i==2) {
          // Edituju cas a prave mam vykreslit dvojtecku
          DisplayMoveTo(AktualniX, Y + 5);
          putchar(':');
          AktualniX-=EDITACESIRKAZNAKU;
        } else if (PocetDesetin>0 && i==PocetDesetin) {
          // Edituju cas a prave mam vykreslit desetinnou carku (tecku)
          DisplayMoveTo(AktualniX, Y + 5);
          putchar('.');
          AktualniX-=EDITACESIRKAZNAKU;
        }//else
        // Edituju normalni cislo bez desetinne carky, takze vykresluju jen cislice
        if (Blink.Show || i!=Index) {
          DisplayMoveTo(AktualniX, Y + 5);
          putchar(0x30 + Znaky[i]);
        }//if
        AktualniX-=EDITACESIRKAZNAKU;
      }//for
      // Znamenko
      if (Znamenko) {  // Pouziva se znamenko
        DisplayMoveTo(X-4, Y);
        DisplayClearBox(3, 20);
        VymazPosledniSloupec();
        if (Zaporne) {
          DisplayCharTahoma18((X-3)*8-1, Y, '-');
        } else {
          DisplayCharTahoma18((X-3)*8-5, Y, '+');
        }//else
      }//if
    }//if
  }//while
}

#endif


