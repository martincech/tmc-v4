//*****************************************************************************
//
//    Delays.h  -  8051 timing utility
//    Version 1.0  (c) VymOs
//
//*****************************************************************************

#ifndef __Delays_H__
   #define __Delays_H__

#ifndef __Hardware_H__
   #include "Hardware.h"
#endif

// 17.8.2007: Upraveno pro pouziti promenneho X2 rezimu. Pri konstantnim X2 rezimu se fce chovaji shodne s puvodni verzi.
//            Je treba pocitat s tim, ze pro pouziti promenneho X2 vyjdou BDelayCount() a WDelayCount() na par instrukci.

// <cycles> je pocet cyklu uzivatelskeho kodu uvnitr smycky
// <loop_cycles> pocet cylku je rezie vnitrni smycky
// <in_cycles> je pocet cyklu uzivatelskeho kodu uvnitr smycky
// <out_cycles> je pocet cyklu uzivatelskeho kodu vne smycky
// <us> je zpozdeni v mikrosekundach
// <ms> je zpozdeni v milisekundach
// typ smycky a ridici promenna urcuji rezii smycky <loop_cycles>

// Namet na upgrade : pro zahrnuti vnejsi smycky
// chybi funkce DelayCount( us, in_cycles, out_cycles) =
// ( us * F/1000000 - out_cycles) / (in_cycles + loop_cycles)
// zrejme budou potreba 2 makra :
// 1. pocet vnitrnich cyklu
// 2. doplnujici zpozdeni ve vnejsi smycce pro dorovnani
// poctu cyklu mensiho nez <in_cycles> + <loop_cycles>
// (jedna iterace vnitrni smycky)

//-----------------------------------------------------------------------------
// Mezni hodnoty smycek
//-----------------------------------------------------------------------------

// max. zpozdeni [us] = max.pocet iteraci * (cycles + loop_cycles) * 12 / FXTAL / CPU_X2

#ifdef CPU_X2

  // CPU_X2 je definovan natvrdo, muzu s nim pocitat primo

  // max. us ve smycce byte preincrement
  #define BDELAY_MAX( cycles)    ( (256 * ( (cycles) + 2) * 12000L / CPU_X2) / (FXTAL / 1000))
  // min. us ve smycce byte preincrement
  #define BDELAY_MIN( cycles)    ( (1   * ( (cycles) + 2) * 12000L / CPU_X2) / (FXTAL / 1000))

  // max. us ve smycce word preincrement
  #define WDELAY_MAX( cycles)    ( (65536 * ( (cycles) + 9) * 120L / CPU_X2) / (FXTAL / 100000))
  // min. us ve smycce byte preincrement
  #define WDELAY_MIN( cycles)    ( (1     * ( (cycles) + 9) * 12000L / CPU_X2) / (FXTAL / 1000))

#else

  // CPU_X2 je promenna, min. hodnoty pocitam pro X1 a max. hodnoty pro X2 (tj. nejhorsi pripady)

  // max. us ve smycce byte preincrement
  #define BDELAY_MAX( cycles)    ( (256 * ( (cycles) + 2) * 12000L / 2) / (FXTAL / 1000))
  // min. us ve smycce byte preincrement
  #define BDELAY_MIN( cycles)    ( (1   * ( (cycles) + 2) * 12000L / 1) / (FXTAL / 1000))

  // max. us ve smycce word preincrement
  #define WDELAY_MAX( cycles)    ( (65536 * ( (cycles) + 9) * 120L   / 2) / (FXTAL / 100000))
  // min. us ve smycce byte preincrement
  #define WDELAY_MIN( cycles)    ( (1     * ( (cycles) + 9) * 12000L / 1) / (FXTAL / 1000))

#endif // CPU_X2

//-----------------------------------------------------------------------------
// Makra na vypocet zpozdeni smycky
//-----------------------------------------------------------------------------

// Upraveno pro pouziti promenne CPU_X2 (puvodni vzorec vychazel s promennou CPU_X2 jako slozity vypocet).
// Pri konstantni hodnote CPU_X2 je vypocet shodny jako predtim.

// byte preincrement [do while(--i)]
//#define BDelayCount( us, cycles)    ( ( (us) * (FXTAL * CPU_X2 / 12)) / ( ( (cycles) + 2) * 1000000))
#define BDelayCount( us, cycles)    ( CPU_X2 * (((us) * (FXTAL / 12)) / ( ( (cycles) + 2) * 1000000)) )

// word preincrement [do while(--i)]
//#define WDelayCount( us, cycles)    ( ( (us) * (FXTAL * CPU_X2 / 12000)) / ( ( (cycles) + 9) * 1000))
#define WDelayCount( us, cycles)    ( CPU_X2 * (word)(((us) * (FXTAL / 12000L)) / ( ( (cycles) + 9L) * 1000L)) )

//-----------------------------------------------------------------------------
// Funkce
//-----------------------------------------------------------------------------

//void SysDelay( word ms);
// Milisekundove zpozdeni <ms>

#endif
