//*****************************************************************************
//
//    PcaTmc.c  -  TMC project PCA services
//    Version 1.0  (c) VymOs
//
//*****************************************************************************

#include "Rd2\Pca.h"
#include "Rd2\Delays.h"       // Delay computing
#include "Fifo\TinyFifo.h"     // Rx 2 fifo

// Pomocna struktura :

typedef union {
   word Counter;
   byte Byte[ 2];
} TCounter;

//---- Zvukovy vystup ---------------------------------------------------------

#ifdef PCA_ASYNC_BEEP
   byte _pca_duration = 0;                       // citac delky tonu pro asynchronni pipnuti
#endif

#ifdef PCA_SOUND
   static word sound_pwm1;                       // prvni perioda zvuku
   static word sound_pwm2;                       // druha perioda zvuku
#endif

//---- Rx/Tx RS232 ------------------------------------------------------------

#define RX2_BITS       10                        // vcetne startbitu a stopbitu
#define RX2_RUNNING    (RX2_CCAPM & CCAPM_ECCF)  // bezi prijem, !RX2_RUNNING - prijat znak
#define RX2_CAPTURE    (RX2_CCAPM & (CCAPM_CAPN | CCAPM_CAPP)) // nastaveno zachytavani

#ifndef RX2_INVERT
   // normalni polarita - v klidu je H
   #define RX2_MARK       0                      // startbit
   #define RX2_SPACE      1                      // klidova uroven
   #define RX2_START_EDGE CCAPM_NEGATIVE_EDGE    // startbit je do 0
#else
   // invertovana polarita - v klidu je L
   #define RX2_MARK       1                      // startbit
   #define RX2_SPACE      0                      // klidova uroven
   #define RX2_START_EDGE CCAPM_POSITIVE_EDGE    // startbit je do 1
#endif

#define TX2_BITS      10                         // vcetne startbitu a stopbitu
#define TX2_RUNNING   (TX2_CCAPM & CCAPM_ECCF)   // bezi vysilani, !TX2_RUNNING - pripraveno na dalsi znak

#ifndef TX2_INVERT
   // normalni polarita - v klidu je H
   #define TX2_MARK       0                      // startbit
   #define TX2_SPACE      1                      // klidova uroven
   #define Tx2Bit( b)     (b)                    // vysilany bit
#else
   // invertovana polarita - v klidu je L
   #define TX2_MARK       1                      // startbit
   #define TX2_SPACE      0                      // klidova uroven
   #define Tx2Bit( b)     !(b)                   // vysilany bit
#endif

// Casovani bitu :

#define COM2_ONE_BIT_COUNT      (FXTAL * PCA_X2 / 4 / COM2_BAUD)
#define COM2_HALF_BIT_COUNT     (COM2_ONE_BIT_COUNT / 2)

#ifdef PCA_COM2
//-----------------------------------------------------------------------------
   #if COM2_RX_TIMEOUT > WDELAY_MAX( 0)
      #error "COM2_RX_TIMEOUT out of range"
   #endif
   #if 100000 > WDELAY_MAX( 0)
      #error "Unable set ComRxWait 100ms timeout"
   #endif

   // plny duplex - vlastni data a pocet pro Rx i Tx
   static volatile byte Data2Tx;                 // Tx byte
   static volatile byte Count2Tx;                // Tx bit count
   static volatile byte Data2Rx;                 // Rx byte
   static volatile byte Count2Rx;                // Rx bit count

   #define Com2RxEnable()   Count2Rx = RX2_BITS; RX2_CCAPM = RX2_START_EDGE // Nastavi cekani na prijem
   #define Com2RxStop()     RX2_CCAPM = CCAPM_STOP                          // Zastaveni prijmu

   //-- FIFO pro Rx COM2 : ----------------------------------------------------

   #define COM_ISIZE COM2_RX_BUFFER_SIZE                   // delka Rx fronty

   static volatile byte iread;                             // cteci ukazatel do Rx bufferu
   static volatile byte iwrite;                            // zapisovaci ukazatel do Rx bufferu
   static xdata    byte ibuff[ COM_ISIZE];                 // Rx fifo

   #define Com2DisableInt()   RX2_CCAPM &= ~CCAPM_ECCF     // zakaz preruseni od modulu
   #define Com2EnableInt()    RX2_CCAPM |=  CCAPM_ECCF     // povoleni preruseni od modulu

   #define Com2RFifoInit()     TFifoInit(  iread, iwrite)                // inicializace
   #define Com2RFifoEmpty()    TFifoEmpty( iread, iwrite)                // zadne nactene znaky
   #define Com2RFifoRead()     TFifoRead(  ibuff, iread,  COM_ISIZE)     // cteni znaku
   #define Com2RFifoFlush()    TFifoFlush( iread);                       // preskok znaku
   #define Com2RFifoFull()     TFifoFull(  iread, iwrite, COM_ISIZE)     // zaplnene fifo
   #define Com2RFifoWrite( ch) TFifoWrite( ibuff, iwrite, COM_ISIZE, ch) // zapis znaku

//-- FIFO pro Tx COM2 : ------------------------------------------------------

#define COM_OSIZE COM2_TX_BUFFER_SIZE                      // delka Tx fronty

volatile byte _tx2_running;                                // probiha vysilani

#if COM2_TX_BUFFER_SIZE <= 128
   static volatile byte oread;                             // cteci ukazatel do Tx bufferu
   static volatile byte owrite;                            // zapisovaci ukazatel do Tx bufferu
#else
   static volatile word oread;                             // cteci ukazatel do Tx bufferu
   static volatile word owrite;                            // zapisovaci ukazatel do Tx bufferu
#endif
static xdata byte obuff[ COM_OSIZE];                       // Tx fifo


#define Com2TFifoInit()     TFifoInit(  oread, owrite)                // inicializace
#define Com2TFifoEmpty()    TFifoEmpty( oread, owrite)                // zadne nactene znaky
#define Com2TFifoRead()     TFifoRead(  obuff, oread,  COM_OSIZE)     // cteni znaku
#define Com2TFifoFlush()    TFifoFlush( oread);                       // preskok znaku
#define Com2TFifoFull()     TFifoFull(  oread, owrite, COM_OSIZE)     // zaplnene fifo
#define Com2TFifoWrite( ch) TFifoWrite( obuff, owrite, COM_OSIZE, ch) // zapis znaku

//-----------------------------------------------------------------------------
#endif // PCA_COM2

//---- Vysilac RS232 ----------------------------------------------------------

#define TX3_BITS      10                         // vcetne startbitu a stopbitu
#define TX3_RUNNING   (TX3_CCAPM & CCAPM_ECCF)   // bezi vysilani, !TX3_RUNNING - pripraveno na dalsi znak

#ifndef TX3_INVERT
   // normalni polarita - v klidu je H
   #define TX3_MARK       0                      // startbit
   #define TX3_SPACE      1                      // klidova uroven
   #define Tx3Bit( b)     (b)                    // vysilany bit
#else
   // invertovana polarita - v klidu je L
   #define TX3_MARK       1                      // startbit
   #define TX3_SPACE      0                      // klidova uroven
   #define Tx3Bit( b)     !(b)                   // vysilany bit
#endif

#define COM3_ONE_BIT_COUNT      (FXTAL * PCA_X2 / 4 / COM3_BAUD)
#define COM3_HALF_BIT_COUNT     (COM3_ONE_BIT_COUNT / 2)

#ifdef PCA_COM3
   static volatile byte Data3;                   // Rx/Tx byte
   static volatile byte Count3;                  // Rx/Tx bit count
#endif

//-----------------------------------------------------------------------------
// Inicializace
//-----------------------------------------------------------------------------

void PcaInit( void)
// Inicializace modulu
{
   // klidovy stav PCA :
   EC         = 0;                               // zakaz preruseni od PCA
   IPH        = IPH_PPC;                         // IRQ priority level = 2
   CCON       = 0;                               // stop PCA, smazani vsech priznaku preruseni
   CMOD       = CMOD_CPS4;                       // hodiny fosc/4
   CL         = 0;                               // nulovani citace
   CH         = 0;
   CCAPM0     = CCAPM_STOP;                      // zakaz vsech komparatoru
   CCAPM1     = CCAPM_STOP;
   CCAPM2     = CCAPM_STOP;
   CCAPM3     = CCAPM_STOP;
   CCAPM4     = CCAPM_STOP;
   // klidovy stav na portech :
#ifdef PCA_COM2
   PcaTX2     = TX2_SPACE;                       // klidova uroven
   PcaRX2     = 1;                               // vstup
   Com2RFifoInit();                              // inicializace Rx bufferu
   Com2TFifoInit();                              // inicializace Tx bufferu
   _tx2_running = NO;                            // zadne vysilani
   Com2RxEnable();                               // povoleni prijmu
#endif
#ifdef PCA_COM3
   PcaTX3     = TX3_SPACE;                       // klidova uroven
#endif
   // povoleni preruseni :
   CR         = 1;                               // povol citani
   EC         = 1;                               // povol preruseni od PCA
} // PcaInit

#ifdef PCA_SOUND
//-----------------------------------------------------------------------------
// Zvuk
//-----------------------------------------------------------------------------

void PcaSoundRun( word count, byte attenuator)
// Spusti zvuk s periodou <count> a  utlumem <attenuator>
{
TCounter Counter;
word     pwm;

   pwm    = 50000;                               // min utlum - perioda 50% je 50000
   pwm  >>= (attenuator >> 1);                   // sudy utlum je 6dB (1/2)
   // normalizujeme na 50% == 10000 :
   if( attenuator & 0x01){
      pwm /= 7;                                  // lichy utlum pridej 3dB (1/V2) + odstraneni petinasobku
   } else {
      pwm /= 5;                                  // sudy utlum je 0dB + odstraneni petinasobku
   }
   sound_pwm1 = count * (dword)pwm / 20000;      // pwm 10 000 == 1/2
   if( sound_pwm1 < 65){
      sound_pwm1 = 65;                           // zpracovani IRQ je pomerne dlouhe
   }
   sound_pwm2 = count - sound_pwm1;              // doplnek do cele periody
   if( sound_pwm2 < 1){
      sound_pwm2 = 1;
   }
   DisableInts();                                // kriticka sekce
   PcaSoundOut      = 0;                         // prvni perioda
   Counter.Byte[ 1] = CL;                        // precti LSB citace
   Counter.Byte[ 0] = CH;                        // precti MSB citace
   Counter.Counter += sound_pwm1;                // nastav prvni periodu
   SOUND_COUNTER_L  = Counter.Byte[ 1];          // nastav LSB
   SOUND_COUNTER_H  = Counter.Byte[ 0];          // nastav MSB
   SOUND_CCAPM      = CCAPM_SW_TIMER;            // spust komparaci
   EnableInts();                                 // konec kriticke sekce
} // PcaSoundRun

//-----------------------------------------------------------------------------
// Zastaveni
//-----------------------------------------------------------------------------

void PcaNosound( void)
// Zastavi zvuk
{
   SOUND_CCAPM = CCAPM_STOP;                     // zastav komparaci
} // PcaNosound

#endif // PCA_SOUND

#ifdef PCA_COM2
//-----------------------------------------------------------------------------
// Vysilani
//-----------------------------------------------------------------------------

void Com2TxChar( byte c)
// Vyslani znaku
{
TCounter Counter;

   if( Com2TFifoFull()){
      return;                                    // neni misto ve fronte
   }
   Com2DisableInt();                             // vyhradni pristup
   if( !_tx2_running){
      // prazdne FIFO, vysilani je zastaveno
      _tx2_running     = YES;                    // zahajeni vysilani
      Data2Tx          = c;                      // vysilana data
      Count2Tx         = TX2_BITS;               // delka vysilanych dat
      DisableInts();                             // kriticka sekce
      Counter.Byte[ 1] = CL;                     // precti LSB citace
      Counter.Byte[ 0] = CH;                     // precti MSB citace
      Counter.Counter += COM2_ONE_BIT_COUNT;     // preruseni co nejdrive, radeji v baudovem ramci
      TX2_COUNTER_L    = Counter.Byte[ 1];       // nejpve LSB
      TX2_COUNTER_H    = Counter.Byte[ 0];       // potom MSB
      TX2_CCAPM        = CCAPM_SW_TIMER;         // compare mode - casovani bitu
      EnableInts();                              // konec kriticke sekce
   } else {
      // probiha vysilani, pridej do bufferu
      Com2TFifoWrite( c);
   }
   Com2EnableInt();
} // Com2TxChar

//-----------------------------------------------------------------------------
// Prijem
//-----------------------------------------------------------------------------

TYesNo Com2RxChar( byte data *c)
// Vraci prijaty znak, nebo NO pro timeout
{
word Timeout;

   Timeout = WDelayCount( COM2_RX_TIMEOUT, 6);
   do {
      if( !Com2RFifoEmpty()){
         // prijat znak
         Com2DisableInt();
         *c = Com2RFifoRead();
         Com2EnableInt();
         return( YES);
      }
   } while( --Timeout);
   return( NO);
} // Com2RxChar

#ifdef COM2_RX_WAIT
//-----------------------------------------------------------------------------
// Cekani na Rx
//-----------------------------------------------------------------------------

TYesNo Com2RxWait( byte Timeout)
// Ceka na znak az <Timeout> * 100 milisekund
{
word i;

   i = 1;
   Timeout++;
   do{
      WatchDog();
      do {
         if( !Com2RFifoEmpty()){
            return( YES);                        // pripraven znak
         }
      } while( --i);
      i = WDelayCount( 100000, 6);
   } while( --Timeout);
   return( NO);
} // Com2RxWait
#endif // COM2_RX_WAIT

#ifdef COM2_FLUSH_CHARS
//-----------------------------------------------------------------------------
// Preskok prijmu
//-----------------------------------------------------------------------------

void Com2FlushChars( void)
// Vynecha vsechny znaky az do meziznakoveho timeoutu
{
word Timeout;

   Timeout = WDelayCount( COM2_RX_TIMEOUT, 10);
   do {
      WatchDog();
      if( !Com2RFifoEmpty()){
         // prijat znak
         Com2DisableInt();
         Com2RFifoFlush();
         Com2EnableInt();
         Timeout = WDelayCount( COM2_RX_TIMEOUT, 10);  // znovu timeout
         continue;
      }
   } while( --Timeout);
} // Com2FlushChars
#endif // COM2_FLUSH_CHARS

#endif // PCA_COM2

#ifdef PCA_COM3
//-----------------------------------------------------------------------------
// Vysilani
//-----------------------------------------------------------------------------

void Com3TxChar( byte c)
// Vyslani znaku
{
TCounter Counter;

   Data3            = c;                         // vysilana data
   Count3           = TX3_BITS;                  // delka vysilanych dat
   DisableInts();                                // kriticka sekce
   Counter.Byte[ 1] = CL;                        // precti LSB citace
   Counter.Byte[ 0] = CH;                        // precti MSB citace
   Counter.Counter += COM3_ONE_BIT_COUNT;        // preruseni co nejdrive, radeji v baudovem ramci
   TX3_COUNTER_L    = Counter.Byte[ 1];          // nejpve LSB
   TX3_COUNTER_H    = Counter.Byte[ 0];          // potom MSB
   TX3_CCAPM        = CCAPM_SW_TIMER;            // compare mode - casovani bitu
   EnableInts();                                 // konec kriticke sekce
   while( TX3_RUNNING);                          // cekani na konec
} // Com3TxChar
#endif // PCA_COM3

//-----------------------------------------------------------------------------
// Prerusovaci rutina
//-----------------------------------------------------------------------------

static void PcaHandler( void) interrupt INT_PCA
// Prerusovaci rutina PCA
{
TCounter Counter;

#ifdef PCA_SOUND
   if( SOUND_FLAG){
      SOUND_FLAG = 0;
      Counter.Byte[ 1] = SOUND_COUNTER_L;        // precti LSB
      Counter.Byte[ 0] = SOUND_COUNTER_H;        // precti MSB
      if( PcaSoundOut){
         Counter.Counter += sound_pwm1;          // nastav prvni periodu
      } else {
         Counter.Counter += sound_pwm2;          // nastav druhou periodu
      }
      SOUND_COUNTER_L  = Counter.Byte[ 1];       // nastav LSB
      SOUND_COUNTER_H  = Counter.Byte[ 0];       // nastav MSB
      PcaSoundOut      = !PcaSoundOut;
   }
#endif
#ifdef PCA_COM2
   // Rx ----------------------------------------------------------------------
   if( RX2_FLAG){
      RX2_FLAG = 0;                              // zrus priznak preruseni
      Counter.Byte[ 1] = RX2_COUNTER_L;          // nejprve LSB
      Counter.Byte[ 0] = RX2_COUNTER_H;          // potom MSB
      if( RX2_CAPTURE){
         // capture - vzorkovani startbitu
         Counter.Counter += COM2_HALF_BIT_COUNT;
         RX2_COUNTER_L    = Counter.Byte[ 1];    // nejpve LSB
         RX2_COUNTER_H    = Counter.Byte[ 0];    // potom MSB
         RX2_CCAPM        = CCAPM_SW_TIMER;      // compare mode - casovani bitu
         Data2Rx          = 0;                   // prijimana data
         goto rx2_done;
      }
      Counter.Counter += COM2_ONE_BIT_COUNT;
      RX2_COUNTER_L    = Counter.Byte[ 1];       // nejpve LSB
      RX2_COUNTER_H    = Counter.Byte[ 0];       // potom MSB
      // startbit :
      if( Count2Rx == RX2_BITS){
         if( PcaRX2 == RX2_SPACE){
            // falesny startbit
            RX2_CCAPM = RX2_START_EDGE;          // nove cekani na startbit
            goto rx2_done;
         }
         Count2Rx--;                             // startbit ok
         goto rx2_done;
      }
      // stopbit :
      if( --Count2Rx == 0){
         Com2RxEnable();                         // novy prijem
         if( PcaRX2 == RX2_MARK){
            goto rx2_done;                       // chyba stopbitu (tj. ramce)
         }
         // uspesny prijem znaku
         if( Com2RFifoFull()){
            Com2RFifoFlush();                     // zahod nejstarsi znak
         }
         Com2RFifoWrite( Data2Rx);                // zapis znak
         goto rx2_done;
      }
      // datove bity :
      Data2Rx >>= 1;
      if( PcaRX2 == RX2_SPACE){
         Data2Rx |= 0x80;
      }
   }
rx2_done :;
   // Tx ----------------------------------------------------------------------
   if( TX2_FLAG){
      TX2_FLAG = 0;                              // zrus priznak preruseni
      Counter.Byte[ 1] = TX2_COUNTER_L;          // nejprve LSB
      Counter.Byte[ 0] = TX2_COUNTER_H;          // potom MSB
      Counter.Counter += COM2_ONE_BIT_COUNT;     // nasledujici bit
      TX2_COUNTER_L    = Counter.Byte[ 1];       // nejpve LSB
      TX2_COUNTER_H    = Counter.Byte[ 0];       // potom MSB
      // startbit (jen pro prvni znak) :
      if( Count2Tx == TX2_BITS){
         PcaTX2 = TX2_MARK;                      // startbit
         Count2Tx--;
         goto tx2_done;
      }
      if( Count2Tx == 0){
         // konec stopbitu = konec znaku
         if( Com2TFifoEmpty()){
            _tx2_running = NO;                   // dovysilano
            TX2_CCAPM    = CCAPM_STOP;           // zastav timer
            goto tx2_done;
         }
         // dalsi znak :
         Data2Tx  = Com2TFifoRead();             // znak z FIFO
         Count2Tx = TX2_BITS - 1;                // delka vysilanych dat bez startbitu
         PcaTX2   = TX2_MARK;                    // startbit
         goto tx3_done;
      }
      if( --Count2Tx == 0){
         PcaTX2   = TX2_SPACE;                   // zahaj stopbit
         goto tx2_done;
      }
      PcaTX2 = Tx2Bit( Data2Tx & 0x01);          // datovy bit
      Data2Tx >>= 1;
   }
tx2_done :;
#endif // PCA_COM2
#ifdef PCA_COM3
   // Tx ----------------------------------------------------------------------
   if( TX3_FLAG){
      TX3_FLAG = 0;                              // zrus priznak preruseni
      Counter.Byte[ 1] = TX3_COUNTER_L;          // nejprve LSB
      Counter.Byte[ 0] = TX3_COUNTER_H;          // potom MSB
      Counter.Counter += COM3_ONE_BIT_COUNT;     // nasledujici bit
      TX3_COUNTER_L    = Counter.Byte[ 1];       // nejpve LSB
      TX3_COUNTER_H    = Counter.Byte[ 0];       // potom MSB
      // startbit :
      if( Count3 == TX3_BITS){
         PcaTX3 = TX3_MARK;                      // startbit
         Count3--;
         goto tx3_done;
      }
      if( Count3 == 0){
         TX3_CCAPM = CCAPM_STOP;                 // konec stopbitu
         goto tx3_done;
      }
      if( --Count3 == 0){
         PcaTX3   = TX3_SPACE;                   // zahaj stopbit
         goto tx3_done;
      }
      PcaTX3 = Tx3Bit( Data3 & 0x01);            // datovy bit
      Data3 >>= 1;
   }
tx3_done :;
#endif
} // PcaHandler

