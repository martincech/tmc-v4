//*****************************************************************************
//
//    Ads7846.h -  Touch screen controller ADS 7846
//    Version 1.0, (c) Vymos
//
//*****************************************************************************

#ifndef __Ads7846_H__
   #define __Ads7846_H__

#ifndef __Hardware_H__
   #include "Hardware.h"        // kody klaves
#endif


// interni promenna, nepouzivat :
extern volatile byte _touch_counter;

// Tato funkce se zaradi do interruptu casovace 0 :
#define TouchTrigger()  if( _touch_counter){ _touch_counter--;}

// Struktura touchpanelu
typedef struct {
  word      X;          // X-ova souradnice stisku
  word      Y;          // Y-ova souradnice stisku
  byte      Repeat;     // YES/NO opakovany stisk
#ifdef TOUCH_READ_RAW
  word      RawX;       // Surova X-ova souradnice
  word      RawY;       // Surova Y-ova souradnice
#endif
} TTouch;
extern TTouch Touch;

//-----------------------------------------------------------------------------
// Funkce
//-----------------------------------------------------------------------------

TYesNo TouchInit( void);
// Inicializace Touch screen

TYesNo TouchGet();
// Je-li stisknuto, vraci YES a souradnice

//TYesNo TouchIsAutoRepeat( void);
// Pokud je touch panel v rezimu autorepeatu, vrati YES.

TYesNo TouchX( word data *x);
// Vraci souradnici X dotyku (Vraci YES je-li ustalena)

TYesNo TouchY( word data *y);
// Vraci souradnici Y dotyku (Vraci YES je-li ustalena)

word TouchAux( void);
// Mereni napeti na vstupu AUX

word TouchVbat( void);
// Mereni napeti na vstupu Vbat

#endif
