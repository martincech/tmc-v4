//*****************************************************************************
//
//    TinyFifo.h  - Tiny FIFO macros
//    Version 1.0  (c) VymOs
//
//*****************************************************************************

#ifndef __TinyFifo_H__
   #define __TinyFifo_H__


// Makra na prehledne funkce FIFO
// <xread>, <xwrite> jsou cteci a zapisovaci ukazatel
// <xbuf> je datovy buffer, <xsize> je delka bufferu

// *** POZOR <xsize> musi byt mocninou 2^n, max 128 pro byte xread,xwrite
// funguje i pro word xread, xwrite

#define TFifoInit(  xread, xwrite)                xread=0; xwrite=0
#define TFifoEmpty( xread, xwrite)                (xread == xwrite)
#define TFifoCount( xread, xwrite)                (xwrite - xread)
#define TFifoFull(  xread, xwrite, xsize)         ( xsize > 128 ? (xread + xsize) == xwrite : (byte)(xread + xsize) == xwrite)
#define TFifoWrite( xbuf,  xwrite, xsize, value)  xbuf[ xwrite++ & (xsize - 1)] = value
#define TFifoRead(  xbuf,  xread,  xsize)         xbuf[ xread++  & (xsize - 1)]
#define TFifoFlush( xread)                        xread++         // "cteni" bez navratu ctene hodnoty

#endif
