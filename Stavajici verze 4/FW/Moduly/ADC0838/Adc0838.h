//*****************************************************************************
//
//    Adc0838.h  -  ADC 0838 services
//    Version 1.0, (c) Vymos
//
//*****************************************************************************

#ifndef __Adc0838_H__
   #define __Adc0838_H__

#ifndef __Hardware_H__
   #include "Hardware.h"
#endif

// Konstanty pro adresaci kanalu :

// Pro interni pouziti :

#define ADC_DIFF   0x01           // SGL/DIF - diff vcetne startbitu
#define ADC_SINGLE 0x03           // SGL/DIF - single vcetne startbitu
#define ADC_ODD    0x04           // ODD/SIGN
#define ADC_SEL1   0x08           // SEL1
#define ADC_SEL0   0x10           // SEL0

// Pro uzivatele API :

#define ADC_SINGLE_CH0   (ADC_SINGLE)                                    // kanal 0
#define ADC_SINGLE_CH1   (ADC_SINGLE | ADC_ODD)                          // kanal 1
#define ADC_SINGLE_CH2   (ADC_SINGLE | ADC_SEL0)                         // kanal 2
#define ADC_SINGLE_CH3   (ADC_SINGLE | ADC_ODD | ADC_SEL0)               // kanal 3
#define ADC_SINGLE_CH4   (ADC_SINGLE | ADC_SEL1)                         // kanal 4
#define ADC_SINGLE_CH5   (ADC_SINGLE | ADC_ODD | ADC_SEL1)               // kanal 5
#define ADC_SINGLE_CH6   (ADC_SINGLE | ADC_SEL0 | ADC_SEL1)              // kanal 6
#define ADC_SINGLE_CH7   (ADC_SINGLE | ADC_ODD | ADC_SEL0 | ADC_SEL1)    // kanal 7

#define ADC_DIFF_CH0     (ADC_DIFF)                                      // kanal 0,1
#define ADC_DIFF_CH1     (ADC_DIFF | ADC_SEL0)                           // kanal 2,3
#define ADC_DIFF_CH2     (ADC_DIFF | ADC_SEL1)                           // kanal 4,5
#define ADC_DIFF_CH3     (ADC_DIFF | ADC_SEL0 | ADC_SEL1)                // kanal 6,7

#define ADC_DIFF_SWAP    ADC_ODD     // prehozeni +/- vstupu u diff, nizsi je -, ORovat s adresou

#define ADC_RANGE        256         // pocet kroku prevodniku. Rozsah je 0..ADC_RANGE-1


void AdcInit( void);
// Inicializuje sbernice

byte AdcRead( byte channel);
// Adresuje kanal <channel>, vrati prectenou hodnotu

#endif

