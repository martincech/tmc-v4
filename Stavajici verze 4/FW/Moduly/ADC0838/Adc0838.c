//*****************************************************************************
//
//    Adc0838.c  -  ADC 0838 services
//    Version 1.0, (c) Vymos
//
//*****************************************************************************

#include "Adc0838\Adc0838.h"


//-----------------------------------------------------------------------------
// Inicializace
//-----------------------------------------------------------------------------

void AdcInit( void)
// Inicializuje sbernice
{
   AdcDIO = 1;
   AdcCLK = 1;
   AdcDisable();
} // AdcInit

//-----------------------------------------------------------------------------
// Mereni
//-----------------------------------------------------------------------------

byte AdcRead( byte channel)
// Adresuje kanal <channel>, vrati prectenou hodnotu
{
byte i;
byte value;

   AdcEnable();      // chipselect
   AdcCLK = 0;       // hodiny zacinaji z 0
   // prepnuti multiplexoru, strobovani kladnou hranou :
   for( i = 0; i < 5; i++){
      AdcDIO = channel & 0x01;
      AdcWait();                // data setup
      AdcCLK = 1;               // strob
      AdcWait();
      AdcCLK = 0;
      channel >>= 1;
   }
   // jeden prazdny cyklus :
   AdcCLK = 1;
   AdcWait();
   AdcCLK = 0;

   AdcDIO = 1;       // nasleduje cteni z portu
   value  = 0;
   // cteni dat, pro jistotu pred zavernou hranou :
   for( i = 0; i < 8; i++){
      value <<= 1;
      AdcCLK = 1;
      AdcWait();
      if( AdcDIO){
         value++;
      }
      AdcCLK = 0;
   }
   AdcCLK = 1;       // vychozi postaveni DIO = 1, CLK = 1
   AdcWait();        // zpozdeni CS za hodinami
   AdcDisable();     // deselect
   return( value);
} // AdcRead

