//*****************************************************************************
//
//    Average.h - Klouzavy prumer
//    Version 1.0
//
//*****************************************************************************

#ifndef __Average_H__
   #define __Average_H__

#ifndef __Hardware_H__
   #include "Hardware.h"     // zakladni datove typy
#endif


// Struktura prumeru
typedef struct {
  TAverageValue Values[AVERAGE_CAPACITY]; // Jednotlive vzorky klouzaveho prumeru
  TAverageCount Length;                   // Aktualni delka klouzaveho prumeru
  TAverageCount Count;                    // Pocet prvku aktualne ulozenych ve Values[]
} TAverage;


void AverageSetLength(TAverage __xdata__ *Average, TAverageCount NewLength);
// Nastavi novou delku klouzaveho prumeru. Melo by nasledovat vynulovani prumeru.

void AverageClear(TAverage __xdata__ *Average);
// Smaze vsechny prvky

void AverageAdd(TAverage __xdata__ *Average, TAverageValue Value);
// Prida do prumeru prvek

TAverageValue AverageGetAverage(TAverage __xdata__ *Average);
// Vrati prumer klouzaveho prumeru

TYesNo AverageFull(TAverage __xdata__ *Average);
// Pokud je prumer zaplnen, vrati YES

TYesNo AverageSteady(TAverage __xdata__ *Average, TAverageValue MaxOffset);
// Pokud jsou vsechny vzorky vzdalene od prumeru max. +- <Stabilization> %, vrati YES


#endif
