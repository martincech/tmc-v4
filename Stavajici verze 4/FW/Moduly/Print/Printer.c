//*****************************************************************************
//
//    Printer.c    - COM Printer services
//    Version 1.0   (c) VymOs
//
//*****************************************************************************

#include "Print\Printer.h"
#include "System.h"      // Operacni system
#include "bcd.h"         // BCD aritmetika

#define __conio_H__             // zakaz CONIO

#ifdef PRINT_COM3
#include "RS232\Com3sw.h"   // SW Seriova linka 3

#define ComInit()            // SW COM nema inicializaci
#define ComTxChar Com3TxChar

#else
#include "RS232\Com.h"      // Interni UART
#endif

#define LINE_DELAY    1000                       // zpozdeni radku

static byte __xdata__ Column;                    // pracovni sloupec
static byte __xdata__ Line[PRT_ROW_WIDTH + 1];  // buffer pracovniho radku

//--- Lokalni funkce :

static void FlushLine (bool Slow);
// Odesle radek na tiskarnu

//-----------------------------------------------------------------------------
// Inicializace
//-----------------------------------------------------------------------------

void PrtInit (void)
// Inicializace po resetu
{
   PrtStart ();
   PrtStop ();
} // PrtInit

//-----------------------------------------------------------------------------
// Zahajeni
//-----------------------------------------------------------------------------

void PrtStart (void)
// Inicializuje linku tiskarny
{

   PrintPowerOn ();                     // zapnuti budice
   ComInit ();                          // Inicializace linky - sdileny COM
   Column = 0;                         // pocatecni sloupec
   // Nastavi tiskarnu tak, aby neblikala :
   PrtString ("\x1By\x02");            // Vypnuti LEDky
   PrtString ("\x1B[\x08\x18");        // Zpomaleni, aby to moc nezralo
   FlushLine (NO);                     // odesli buffer
} // PrtStart

//-----------------------------------------------------------------------------
// Ukonceni
//-----------------------------------------------------------------------------

void PrtStop (void)
// Uvolni linku tiskarny
{
   SysDelay (10);                      // cekej na dokonceni
   PrintPowerOff ();                    // vypnuti budice
} // PrtStop

//-----------------------------------------------------------------------------
// Znak
//-----------------------------------------------------------------------------

char PrtChar (char c)
// Tiskne znak
{
   if (c == '\r' || c == '\n') {
      Line[Column++] = '\r';
      FlushLine (YES);
      return c;
   }
   if (c == PRT_FF) {
      Line[Column++] = c;             // formfeed - nova stranka
      FlushLine (YES);
      return c;
   }
   if (Column >= PRT_ROW_WIDTH) {
      Line[PRT_ROW_WIDTH] = '\r';     // line wrap
      FlushLine (YES);                 // odesli buffer
   }
   Line[Column++] = c;
   return c;
} // PrtChar

//-----------------------------------------------------------------------------
// Retezec
//-----------------------------------------------------------------------------

void PrtString (char code *Text)
// Tiskne retezec z kodove pameti
{
   while (*Text) {
      PrtChar (*Text);
      Text++;
   }
} // PrtString

#if defined( PRT_HEX) || defined( PRT_DEC) || defined( PRT_FLOAT)
//-----------------------------------------------------------------------------
// Cislo Hexa
//-----------------------------------------------------------------------------
#include "cprinthe.h"

void PrtHex (dword x, byte width)
{
   couthex (x, width, PrtChar);
}
#endif // PRT_HEX


#ifdef PRT_DEC
//-----------------------------------------------------------------------------
// Cislo dekadicky
//-----------------------------------------------------------------------------
#include "cprintde.h"
void PrtDec (int32 x, byte width)
{
   coutdec (x, width, PrtChar);
}

#endif // PRT_DEC


#ifdef PRT_CHARS
//-----------------------------------------------------------------------------
// Opakovane znaky
//-----------------------------------------------------------------------------

void PrtChars (char c, byte cols)
// Prida sloupec znaku <c> o sirce <cols>
{
   byte i;

   for (i = 0; i < cols; i++) {
      PrtChar (c);
   }
} // PrtChars
#endif

//-----------------------------------------------------------------------------
// Flush
//-----------------------------------------------------------------------------

static void FlushLine (bool Slow)
// Odesle radek na tiskarnu
{
   byte i;

   for (i = 0; i < Column; i++) {
      ComTxChar (Line[i]);
   }
   Column = 0;
   if (Slow) {
      WatchDog ();
      SysDelay (LINE_DELAY);
   }
} // FlushLine
