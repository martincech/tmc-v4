//*****************************************************************************
//
//    Cfloat.c - simple display services
//    Version 1.1 (c) VymOs
//
//*****************************************************************************

#ifndef __cfloat_h__
#define __cfloat_h__

//-----------------------------------------------------------------------------
// BCD s desetinnou teckou
//-----------------------------------------------------------------------------
void cfloat( dword x, byte w, byte d);

#endif