//*****************************************************************************
//
//    Cfloat.c - simple display services
//    Version 1.1 (c) VymOs
//
//*****************************************************************************

#ifndef __conio_H__
#include "conio.h"

extern char putchar (char c);
// standardni zapis znaku
#endif

#include "bcd.h"
#include "cfloat.h"
//-----------------------------------------------------------------------------
// BCD s desetinnou teckou
//-----------------------------------------------------------------------------

void cfloat (dword x, byte w, byte d)
// Tiskne BCD cislo <x> jako float s celkovou sirkou <w>
// a <d> desetinnymi misty. Znamenko se zada jako <x> | PRT_MINUS
{
   if (x & FMT_MINUS) {
      x &= ~FMT_MINUS;
      putchar ('-');
   }
   else {
      putchar (' ');
   }
   cprinthex (x, FmtPrecision (w, d));
} // cfloat
