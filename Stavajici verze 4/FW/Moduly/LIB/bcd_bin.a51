$TITLE('Prevod binarniho bytu na BCD a naopak')

NAME		bcd_bin

bcd_bin		segment code
public		_bcdbin
public		_binbcd
rseg		bcd_bin

_bcdbin:;prevod cisla v R7 z BCD do binarniho tvaru a ulozeni zpet do R7
 	MOV	A,R7		;predani parametru
	SWAP	A
	ANL	A,#0FH
	MOV	B,#10D
	MUL	AB
	MOV	R6,A
	MOV	A,R7
	ANL	A,#0FH
	ADD	A,R6
	MOV	R7,A		;navratova hodnota    
	RET

_binbcd:;prevod binarniho cisla v R7 do BCD a ulozeni zpet do R7
	MOV	A,R7		;predani parametru
	MOV	B,#100D
	DIV	AB
	MOV	A,#10D
	XCH	A,B
	DIV	AB
	SWAP	A
	ADD	A,B
	MOV	R7,A		;navratova hodnota    	
	RET

	end

$EJECT
