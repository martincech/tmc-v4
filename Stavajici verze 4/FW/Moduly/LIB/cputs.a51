;------------------------------------------------------------------------------
;
; cputs.a51 - put string via putchar
;
;------------------------------------------------------------------------------


NAME    CPUTS

?PR?_cputs?CPUTS    SEGMENT CODE
        PUBLIC  _cputs
        EXTRN   CODE ( _putchar)

; //-----------------------------------------------------------------------------
; // Zapis retezce
; //-----------------------------------------------------------------------------
;
; void cputs( char code *string)

        RSEG  ?PR?_cputs?CPUTS
	USING	0
_cputs:
; // vystup retezce <string> z kodove pameti
; {
			; SOURCE LINE # 357
        MOV     DPL, r7
        MOV     DPH, r6
?C0020:
	CLR  	A
	MOVC 	A,@A+DPTR
	MOV  	R7,A
	JZ   	?C0022
        PUSH    DPL
        PUSH    DPH
        LCALL   _putchar
        POP     DPH
        POP     DPL
        INC     DPTR
	SJMP 	?C0020
?C0022:
;        MOV     R7, #0AH        ; zaverecne odradkovani
;        LCALL   _putchar
	RET
; }
        END
