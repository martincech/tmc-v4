NAME		nibble

nibble		segment code
public		_lnib
public		_hnib
rseg		nibble

_lnib:	;usekne horni polovinu nibblu
	mov	a,r7
	anl	a,#0FH
	mov	r7,a
	ret

_hnib:	;usekne dolni polovinu nibblu a horni da na misto dolniho
	mov	a,r7
	swap	a
	anl	a,#0FH
	mov	r7,a
	ret

	end
	
$EJECT