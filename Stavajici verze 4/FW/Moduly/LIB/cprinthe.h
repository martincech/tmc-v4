//*****************************************************************************
//
//    Cprinthe.c - simple display services
//    Version 1.1  (c) VymOs
//
//*****************************************************************************

#ifndef __cprinthe_H__
#define __cprinthe_H__
#include "cprint.h"
//-----------------------------------------------------------------------------
// Hexadecimalni
//-----------------------------------------------------------------------------
void couthex( dword x, byte width, PrintCharFnc printFnc);
void cprinthex( dword x, byte width);
#endif