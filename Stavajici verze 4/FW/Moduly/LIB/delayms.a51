NAME	delayms

delayms	segment code
public	_delayms
rseg	delayms

_delayms: 
	;generuje zpozdeni r7 * 1ms pro krystal 6MHz
	;celkove zpozdeni tedy je r7*246*T*T + 8*T, T = 2us pro 6MHz
	nop			; push ACC
	nop
LABEL:  mov     a,#61d		;1T
        djnz    ACC,$		;2T
        djnz    r7,LABEL	;2T
        nop			; pop   acc
	nop
        ret

	end

$EJECT