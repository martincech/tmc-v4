;------------------------------------------------------------------------------
;
; BBIN2BCD.A51 generated from: BCD.C51
;
;------------------------------------------------------------------------------


NAME    BBIN2BCD

?PR?_bbin2bcd?BBIN2BCD    SEGMENT CODE

        PUBLIC  _bbin2bcd


; //-----------------------------------------------------------------------------
; // bbin2bcd
; //-----------------------------------------------------------------------------

; word bbin2bcd( byte x)

        RSEG  ?PR?_bbin2bcd?BBIN2BCD
	USING	0
_bbin2bcd:
			; SOURCE LINE # 16
; // prevede 0..255 do bcd
; {
			; SOURCE LINE # 18
	  mov     a, r7                   ; vstupni parametr
	  mov     b, #0100D               ; stovky
	  div     ab
	  mov     r6, a                   ; MSB - uloz stovky
	  mov     a, #10D                 ; zbytek del desitkami
	  xch     a, b
	  div     ab
	  swap    a                       ; desitky do horniho nibblu
	  add     a, b                    ; pricti jednotky
	  mov     r7, a                   ; navratova hodnota
; } // bbin2bcd
			; SOURCE LINE # 31
	RET

        END
