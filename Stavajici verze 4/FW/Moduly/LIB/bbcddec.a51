;------------------------------------------------------------------------------
;
; bbcddec.a51 generated from: BCD.C51 + MODIFIED
;
;------------------------------------------------------------------------------


NAME    BBCDDEC

?PR?_bbcddec?BBCDDEC     SEGMENT CODE

        PUBLIC  _bbcddec

; //-----------------------------------------------------------------------------
; // bbcddec
; //-----------------------------------------------------------------------------
;
; byte bbcddec( byte x)

        RSEG  ?PR?_bbcddec?BBCDDEC
	USING	0
_bbcddec:
			; SOURCE LINE # 212
; // vrati x-1
; {
			; SOURCE LINE # 214
	  mov a, #99H                     ;
	  add a, r7                       ; plus X
	  da  a                           ; BCD korekce
	  mov r7, a                       ; navrat
; } // bbcddec
			; SOURCE LINE # 221
	RET

        END
