;------------------------------------------------------------------------------
;
; dbcd2bin.a51 generated from: BCD.C51 + MODIFIED
;
;------------------------------------------------------------------------------


NAME    DBCD2BIN

?PR?_dbcd2bin?DBCD2BIN    SEGMENT CODE

?DT?_dbcd2bin?BCD  SEGMENT DATA OVERLAYABLE

	EXTRN	CODE (?C?LMUL)
	EXTRN	CODE (?C?LADD)
        EXTRN   CODE (_wbcd2bin)
	PUBLIC	_dbcd2bin



	RSEG  ?DT?_dbcd2bin?BCD
?_dbcd2bin?BYTE:
          x?545:   DS   4

; //-----------------------------------------------------------------------------
; // dbcd2bin
; //-----------------------------------------------------------------------------
;
; dword dbcd2bin( dword x)

        RSEG  ?PR?_dbcd2bin?DBCD2BIN
	USING	0
_dbcd2bin:
	MOV  	x?545+03H,R7
	MOV  	x?545+02H,R6
	MOV  	x?545+01H,R5
	MOV  	x?545,R4
			; SOURCE LINE # 161
; // prevede 0..0x99999999 do bin
; {
			; SOURCE LINE # 163
;    return( (dword)wbcd2bin( x >> 16) * 10000  + wbcd2bin( x & 0xFFFF));
			; SOURCE LINE # 164
        mov     r7, ar5
        mov     r6, ar4
        LCALL   _wbcd2bin
	CLR  	A
	MOV  	R4,A
	MOV  	R5,A
	MOV  	R3,#010H
	MOV  	R2,#027H
	MOV  	R1,A
	MOV  	R0,A
	LCALL	?C?LMUL
	PUSH 	AR4
	PUSH 	AR5
	PUSH 	AR6
	PUSH 	AR7
        MOV     R7,x?545+03H
        MOV     R6,x?545+02H
	LCALL	_wbcd2bin
	CLR  	A
	MOV  	R4,A
	MOV  	R5,A
	POP  	AR3
	POP  	AR2
	POP  	AR1
	POP  	AR0
	LCALL	?C?LADD
; } // dbcd2bin
			; SOURCE LINE # 165
?C0006:
	RET

        END
