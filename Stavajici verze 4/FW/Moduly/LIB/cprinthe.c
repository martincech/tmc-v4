//*****************************************************************************
//
//    Cprinthe.c - simple display services
//    Version 1.1  (c) VymOs
//
//*****************************************************************************

#ifndef __conio_H__
#include "conio.h"

extern char putchar (char c);
// funkce pro zapis znaku
#endif
#include "bcd.h"
#include "cprinthe.h"

//-----------------------------------------------------------------------------
// Hexadecimalni
//-----------------------------------------------------------------------------

void couthex (dword x, byte format, PrintCharFnc printFnc)
// vystup hexa cisla <x> o sirce <width> znaku
{
byte printZeros = 0;
byte nibble = 0;
byte i = 0;
byte decimals = 0;
dword value = 0;
byte width = 0;
   
   printZeros = format & FMT_LEADING_0;         // je-li priznak, nepreskakuj nuly
   decimals = (format & FMT_DECIMALS) >> FMT_DECIMALS_SHIFT; // pocet za carkou   
   width = format & FMT_WIDTH;                     // pocet znaku
   if (width == 0) {
      width = FMT_WIDTH + 1;                     // plna sirka
   }
   value = x << ((8-width)*4);
   // jednotlive nibbly ve smeru od MSB v sirce width..LSB :
   for (i = width; i > 0; i--) {
      nibble = (value >> 28) & 0xF;
      value = value << 4;
      // nenulova cislice, predchazi nenulova cislice, posledni cislice :
      if (nibble || printZeros || (i == 1)) {
         printZeros = YES;                        // dal uz nepreskakuj
         printFnc (nibble2hex (nibble));
      }
      else {
         printFnc (' ');                          // nulova cislice, jen zarovnani doprava
      }
      if (decimals && (decimals == (i - 1))) {
         if (!printZeros) {
            printZeros = YES;
            printFnc ('0');                       // jednotky vzdy zobrazit
         }
         printFnc ('.');
      }
   }
} // couthex

void cprinthex (dword x, byte width)
// vystup hexa cisla <x> o sirce <width> znaku
{
   couthex(x, width, putchar);
}// cprinthex