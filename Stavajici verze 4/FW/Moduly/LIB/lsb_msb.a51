NAME		lsb_msb

lsb_msb		segment code
public		_lsb
public		_msb
rseg		lsb_msb

_lsb:	;vezme spodni cast wordu (r7) a da do r7
	ret

_msb:	;vezme horni cast wordu (r6) a da do r7
	mov	a,r6
	mov	r7,a	;r7 je navratova hodnota
	ret

	end

$EJECT