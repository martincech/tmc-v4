//*****************************************************************************
//
//    Cprintde.c - simple display services
//    Version 1.1 (c) VymOs
//
//*****************************************************************************

#ifndef __conio_H__
   #include "conio.h"

   extern char putchar( char c);
   // standardni zapis znaku
#endif

#include "bcd.h"
#include "cprintde.h"
#include "cprinthe.h"
//-----------------------------------------------------------------------------
// Dekadicke se znamenkem
//-----------------------------------------------------------------------------
void coutdec( int32 x, byte width, PrintCharFnc printFnc)
{
   dword out;
   if(x < 0){
      out = -x;
   }else{
      out = x;
   }
   if( !(width & FMT_UNSIGNED)){
      // se znamenkem
      if( x < 0){
         printFnc('-');
      } else {
         printFnc(' ');
      }
   }
   out = dbin2bcd( out);
   couthex( (dword)out, width, printFnc);
}
void cprintdec( int32 x, byte width)
// vystup dekadickeho cisla <x> o sirce <width> znaku
{
   coutdec(x, width, putchar);
} // cprintdec
