;------------------------------------------------------------------------------
;
; wbcd2bin.A51 generated from: BCD.C51 + MODIFIED
;
;------------------------------------------------------------------------------


NAME    WBCD2BIN

?PR?_wbcd2bin?WBCD2BIN    SEGMENT CODE

        PUBLIC  _wbcd2bin
	EXTRN	CODE (?C?IMUL)
        EXTRN   CODE (_bbcd2bin)

; //-----------------------------------------------------------------------------
; // wbcd2bin
; //-----------------------------------------------------------------------------

; word wbcd2bin( word x)

        RSEG  ?PR?_wbcd2bin?WBCD2BIN
	USING	0
_wbcd2bin:
	MOV  	R3,AR7
	MOV  	R2,AR6
			; SOURCE LINE # 151
; // prevede 0..0x9999 do bin
; {
			; SOURCE LINE # 153
;    return( (word)bbcd2bin( x >> 8) * 100  + bbcd2bin( x & 0xFF));
			; SOURCE LINE # 154
	MOV  	A,R2
	MOV  	R7,A
	LCALL	_bbcd2bin
	MOV  	R6,#00H
	MOV  	R4,#00H
	MOV  	R5,#064H
	LCALL	?C?IMUL
	MOV  	R4,AR6
	MOV  	R5,AR7
	MOV  	R7,AR3
	MOV  	A,R3
	LCALL	_bbcd2bin
	MOV  	R6,#00H
	MOV  	A,R5
	ADD  	A,R7
	MOV  	R7,A
	MOV  	A,R6
	ADDC 	A,R4
	MOV  	R6,A
; } // wbcd2bin
			; SOURCE LINE # 155
	RET

        END
