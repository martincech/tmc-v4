NAME		bindec

bindec		segment code
public		_bindec
rseg		bindec

_bindec:
	;prevede 3 byty (R5 - R7) z bin na 4 byty (R4 - R7) do dec
	mov	a,r4
	mov	b,a
	mov	a,r5
	mov	r0,a
	mov	a,r6
	mov	r1,a
	mov	a,r7
	mov	r2,a		;presun r4 - r7 na b, r0 - r2
	clr	a
	mov	r7,a
	mov	r6,a
	mov	r5,a
	mov	r4,a		;vynulovani r4 - r7
	mov	a,b		;MSB do akumulatoru
	mov	r3,#32d		;citac pruchodu (4 x 8 bitu)
HLEDA:	rlc	a		;posun vlevo
	mov	b,a		;schovej
	mov	a,r7		;ber LSB vysledku
	addc	a,r7		;secti s prenosem
	da	a		;preved do BCD
	mov	r7,a		;a vrat
	mov	a,r6		;NSB
	addc	a,r6		
	da	a
       	mov	r6,a		
	mov	a,r5		;NSB
	addc	a,r5	
	da	a
	mov	r5,a	
	mov	a,r4            ;MSB
	addc	a,r4
	da	a
	mov	r4,a
	djnz	r3,TEST24
 	ret
TEST24:	cjne	r3,#24,TEST16
	mov	a,r0		;r3 = 32 => nastav NSB
	jmp	HLEDA
TEST16:	cjne	r3,#16,TEST8
	mov	a,r1            ;r3 = 16 => nastav NSB
	jmp	HLEDA
TEST8:	cjne	r3,#8,OBNOV
	mov	a,r2 		;r3 = 8 => nastav LSB
	jmp	HLEDA	
OBNOV:	mov	a,b		;neni-li 8 nebo 16 nebo 32, obnov ACC
	jmp	HLEDA

	end
	
$EJECT
