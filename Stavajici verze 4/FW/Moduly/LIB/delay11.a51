$TITLE('Zpozdeni n x 1ms pro 11.0592MHz')

NAME 		DELAY11

DELAY11 	segment code
public 		_DELAY11
rseg		DELAY11

;DELAY11 GENERUJE ZPOZDENI D = 2T + 922T * n
;coz je pro T = 1.085 mikrosec.

_DELAY11:
Label:  MOV     A,#255D	 	;1T
	DJNZ    ACC,$	 	;510T
	MOV	A,#204D	 	;1T
	DJNZ    ACC,$	 	;408T
	DJNZ    R7,Label 	;2T
	RET		 	;2T

	END

$EJECT