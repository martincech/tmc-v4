//*****************************************************************************
//
//    Pid.c - PID regulator
//    Version 1.0
//
//*****************************************************************************

#include "Pid\Pid.h"

//-----------------------------------------------------------------------------
// Konstanty
//-----------------------------------------------------------------------------

#define VALUE_INVALID   -32768                          // Neplatna hodnota merene veliciny

#define OUTPUT_MIN      0                               // Minimalni vystup je 0%
#define OUTPUT_MAX      100                             // Maximalni vystup je 100%
#define OUTPUT_RANGE    (OUTPUT_MAX - OUTPUT_MIN)       // Rozsah 0..100%

//-----------------------------------------------------------------------------
// Inicializace
//-----------------------------------------------------------------------------

void PidInit(TPid __xdata__ *Pid) {
  // Inicializace regulatoru
  Pid->LastValue    = VALUE_INVALID;
  Pid->Proportional = 0;
  Pid->Integral     = 0;
  Pid->Derivative   = 0;
  Pid->Output       = 0;
  Pid->Step         = Pid->SamplingPeriod - 1;

  // Predvypocty konstant pro zrychleni
  Pid->IntegralScale = Pid->TiMax / Pid->SamplingPeriod;
}

//-----------------------------------------------------------------------------
// Krok regulatoru
//-----------------------------------------------------------------------------

void PidStep(TPid __xdata__ *Pid, int CurrentValue) {
  // Krok regulatoru
  // PID regulace ve standardni forme CO = Kp*e + (Kp/Ti)*int(e*dt) + Kp*Td*de/dt (de/dt je nahrazeno primo derivaci).
  // Napriklad regulace teploty v aute:
  // Error Kp     Ti      Prop. slozka  Integ. slozka
  // 2.0C  10     300 s   20 %          0.67% za 10sec

  long i;
  long Limit;
  long Kp;
  long KpDenominator;
  long LsbDenominator;

  // Vzorkovaci perioda
  if (++Pid->Step >= Pid->SamplingPeriod) {
    Pid->Step = 0;      // Nove citani, pokracuj ve vypoctu
  } else {
    return;             // Neaktivni perioda
  }

  // Pro zmenseni kodu
  Kp             = Pid->Kp;
  KpDenominator  = Pid->KpDenominator;
  LsbDenominator = Pid->LsbDenominator;

  // Odchylka
  Pid->Error = Pid->Target - CurrentValue;      // Regulacni rozdil

  // Proporcionalni slozka
  Pid->Proportional = (long)(Kp * (long)Pid->Error / KpDenominator);

  // Integracni slozka
  // Integral anti-windup: Pokud je proporcionalni slozka zasaturovana na 100%,  nastavim vstup integratoru na nulu, tj. neintegruji.
  // Slouzi to k zamezeni prekroceni cilove hodnoty pri nabehu. Integrace by se mela spravne zastavit pokud je vystup regulatoru
  // na 100% (tj. P+D), ale napr. pri otevreni dveri auta behem regulace se to chava lepe jen na P slozku - integrator je tak delsi
  // dobu zastaveny. Na druhou stranu nabeh topeni trva trosku dele (zacne se integrovat az za delsi dobu od startu topeni).
  if (Pid->Ti > 0) {
    // Platna integracni slozka
    i = Pid->Integral;
    if (Pid->Proportional < LsbDenominator * OUTPUT_RANGE) {
      // Priintegruju jen kdyz neni proporcionalni vystup v saturaci. Pokud je, necham v integracni slozce minulou hodnotu.
      // (Pid->TiMax / Pid->SamplingPeriod) je meritko zvetseni integracni slozky, pri max Ti LSB odchylky vyvola alespon integral += 1
      // Ve vzorci je Pid->SamplingPeriod vynechane, vykratilo by se.
      i += Kp * (long)Pid->TiMax * (long)Pid->Error / ((long)Pid->Ti * KpDenominator);
    }
    // Limitace integracni slozky (minimum je 0%, dolu neni treba omezovat)
    Limit = OUTPUT_MAX * (long)Pid->IntegralScale * LsbDenominator;        // maximalni integracni hodnota
    if (i > Limit) {
      i = Limit;        // Omezim na max. 100%
    } else if (i < 0) {
      i = 0;            // Integracni slozka muze byt jen kladna
    }
  } else {
    // Bez integracni slozky
    i = 0;
  }

  Pid->Integral = i;                     // Zapamatuj pro dalsi krok

  // Derivacni slozka
  if (Pid->LastValue == VALUE_INVALID || Pid->Td == 0) {
    // Jde o prvni krok regulatoru po startu => nederivuju, pockam na dalsi krok
    // Pripadne je derivacni cas nulovy, takze derivacni slozku nepouzivam
    Pid->Derivative = 0;
  } else {
    Pid->Derivative = Kp * (long)Pid->Td * (long)(Pid->LastValue - CurrentValue) / ((long)Pid->SamplingPeriod * KpDenominator);
    // Limitace derivacni slozky
    Limit = OUTPUT_MAX * LsbDenominator;
    if (Pid->Derivative > Limit) {
      Pid->Derivative = Limit;
    } else if (Pid->Derivative < -Limit) {
      Pid->Derivative = -Limit;
    }
  }
  Pid->LastValue = CurrentValue;        // Zapamatuju si aktualni hodnotu pro dalsi krok

  // Vypoctu celkovy vystup
  i /= Pid->IntegralScale;              // Integracni slozku pokratim
  i += Pid->Proportional;               // Prictu proporcionalni slozku
  i += Pid->Derivative;                 // Prictu derivacni slozku
  CurrentValue = (int)(i / LsbDenominator);        // Meritko

  // CurrentValue pouziju pro zrychleni

  // Limitace akcni veliciny
  if (CurrentValue > OUTPUT_MAX) {
    CurrentValue = OUTPUT_MAX;
  } else if (CurrentValue < OUTPUT_MIN) {
    CurrentValue = OUTPUT_MIN;
  }

  // Ulozim vystup
  Pid->Output = CurrentValue;
}
