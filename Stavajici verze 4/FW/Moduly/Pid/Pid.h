//*****************************************************************************
//
//    Pid.h - PID regulator
//    Version 1.0
//
//*****************************************************************************

#ifndef __Pid_H__
   #define __Pid_H__

#ifndef __Hardware_H__
   #include "Hardware.h"     // zakladni datove typy
#endif


// Struktura PID regulatoru
typedef struct {
  byte SamplingPeriod;      // Perioda kroku regulace v sekundach
  int  Kp;                  // Zesileni, muze byt i zaporne
  word KpDenominator;       // Jmenovatel zesileni (1 pro cele cislo, 10 pro Kp na desetiny, 100 na setiny, 1000 na tisiciny)
  word Ti;                  // Integracni cas v celych sekundach
  word TiMax;               // Maximalni hodnota Ti v sekundach, kvuli jemnemu pocitani integracni slozky
  word Td;                  // Derivacni cas v celych sekundach

  int Target;               // Cilova hodnota
  int LsbDenominator;       // Jmenovatel LSB merene veliciny, LSB = 1/LsbDenominator

  int LastValue;            // Hodnota merene veliciny v predchozim kroku

  byte Step;                // Aktualni cislo kroku

  int  Error;               // Rozdil mezi cilovou a aktualni hodnotou
  long Proportional;        // Proporcionalni slozka regulace (muze byt >100%, kladne i zaporne cislo) s meritkem LsbDenominator
  long Integral;            // Integracni slozka regulace 0..100%, s vetsim meritkem (IntegralScale * LsbDenominator), aby sly integrovat i male odchylky
  long Derivative;          // Derivacni slozka PID regulace -100..+100% s meritkem LsbDenominator

  byte Output;              // Vystup regulatoru 0-100%

  word IntegralScale;       // Predpocitana hodnota TiMax/SamplingPeriod pro zrychleni
} TPid;


void PidInit(TPid __xdata__ *Pid);
  // Inicializace regulatoru

void PidStep(TPid __xdata__ *Pid, int CurrentValue);
  // Krok regulatoru


#endif
