//*****************************************************************************
//
//    X9313.c  -  X9313 elektronicky potenciometr
//    Verze 1.0
//
//*****************************************************************************

#include "X9313\X9313.h"

#define NastavSdilenePorty() {E2PotINC=1; E2PotUD=1;}

void E2PotDown() {
  // Snizi odpor potenciometru o 1 krok
  NastavSdilenePorty();
  E2PotCS = 0;     // zaselektuji E2POT
  E2PotUD = 1;     // smer DOWN
  E2PotINC = 0;    // krok
  E2PotINC = 1;    // kvuli store
  E2PotCS = 1;     // CS do 1 to udela store pro INC = 1
  NastavSdilenePorty();
}

void E2PotUp() {
  // Zvysi odpor potenciometru o 1 krok
  NastavSdilenePorty();
  E2PotCS = 0;     // zaselektuji E2POT
  E2PotUD = 0;     // smer UP
  E2PotINC = 0;    // krok
  E2PotINC = 1;    // kvuli store
  E2PotCS = 1;     // CS do 1 to udela store pro INC = 1
  NastavSdilenePorty();
}

