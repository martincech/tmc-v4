//*****************************************************************************
//
//    Com.h - RS232 communication services
//    Version 1.0, (c) VymOs
//
//*****************************************************************************

#ifndef __Com_H__
   #define __Com_H__

#ifndef __Hardware_H__
   #include "Hardware.h"
#endif

#ifdef COM_TX_BUFFER_SIZE          // asynchronni vysilac
   // interni promenna, nepouzivat :
   extern volatile byte  _tx_running;
#endif

//-----------------------------------------------------------------------------
// Funkce
//-----------------------------------------------------------------------------

void ComInit( void);
// Inicializace komunikace

#ifdef COM_TX_BUFFER_SIZE          // asynchronni vysilac
   #define ComTxBusy()  _tx_running
   // Probiha vysilani
#else
   #define ComTxBusy() NO          // synchronni vysilani, vzdy pripraven

   void ComTxWait( void);
   // Cekani na vyslani znaku
#endif

void ComTxChar( byte c);
// Vyslani znaku

TYesNo ComRxChar( byte data *Char);
// Vraci prijaty znak. Po timeoutu vraci NO

TYesNo ComRxWait( byte Timeout);
// Ceka na znak az <Timeout> * 100 milisekund

void ComFlushChars( void);
// Vynecha vsechny znaky az do meziznakoveho timeoutu


#endif

