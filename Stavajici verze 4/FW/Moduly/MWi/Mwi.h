//*****************************************************************************
//
//    Mwi.h  -  Multiple 1-Wire bus services
//    Version 1.0, (c) Vymos
//
//*****************************************************************************

#ifndef __Mwi_H__
   #define __Mwi_H__

#ifndef __Hardware_H__
   #include "Hardware.h"
#endif

// Identifikacni retezec :

#define MWI_SERIAL_LENGTH 6              // delka serioveho cisla

typedef struct {
   byte family;                        // kod typu cipu
   byte serial[ MWI_SERIAL_LENGTH];    // seriove cislo
   byte crc;                           // zabezpeceni
} TMwiIdentification;

#define MWI_IDENTIFICATION_LENGTH sizeof( TMwiIdentification)


// Kody prikazu pro operace s ROM :

#define MWI_CMD_SEARCH_ROM       0xF0    // vyhledani a identifikace zarizeni
#define MWI_CMD_READ_ROM         0x33    // cteni identifikace (pro 1 zarizeni)
#define MWI_CMD_MATCH_ROM        0x55    // vyber zarizeni podle identifikace
#define MWI_CMD_SKIP_ROM         0xCC    // vyrazeni identifikace (pro 1 zarizeni)
#define MWI_CMD_ALARM_SEARCH     0xEC    // vyhledani a identifikace zarizeni s alarmem


// interni promenna, nepouzivat :
extern byte _mwi_old_crc;              // kontext CRC

//-----------------------------------------------------------------------------

void MwiAttach( void);
// Pripravi sbernici pro provoz

#ifdef MwiDATA
   void MwiRelease( void);
   // Uvolneni sbernice - jen pro sdilenou sbernici
#endif

void MwiResetAll( void);
// Resetuje sbernici

TYesNo MwiReset( byte channel);
// Resetuje sbernici, vraci signal pritomnosti.
// YES - zarizeni pritomno

bit MwiReadBit( byte channel);
// Vytvori Read slot a vrati hodnotu bitu

void MwiWriteBit( bit value);
// Vytvori Write slot s hodnotou <value>

byte MwiReadByte( byte channel);
// Precte byte ze sbernice

void MwiWriteByte( byte value);
// Zapise byte na sbernici

TYesNo MwiReadROM( byte channel, TMwiIdentification *ident);
// Precte identifikaci cipu

#define MwiResetCrc() _mwi_old_crc = 0
// Nuluje zabezpeceni, volat vzdy pred vypoctem

byte MwiCalcCrc( byte value);
// Vypocte z bytu zabezpeceni, vrati vysledek

#define MwiGetCrc() _mwi_old_crc
// Vrati vysledek vypoctu zabezpeceni

#define MwiPowerOff()   MwiDQO = DQO_LO
// Vypne napajeni na sbernici - pozor, predtim je nutne odpojit strong pullup!

#define MwiPowerOn()    MwiDQO = DQO_HI
// Zapne napajeni na sbernici

#endif

