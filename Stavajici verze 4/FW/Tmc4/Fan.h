//*****************************************************************************
//
//    Fan.c - Kontrola behu a poruchy ventilatoru
//    Version 1.0
//
//*****************************************************************************

#ifndef __Fan_H__
   #define __Fan_H__

#include "Hardware.h"

typedef struct {
  byte On;          // YES/NO zapnute ventilatory
  byte AutoMode;    // YES/NO automaticky nebo rucni rezim pomoci prepinace
  byte Power;       // Vykon ventilace v procentech
  byte Failure;     // YES/NO porucha ventilatoru
} TFan;
extern TFan __xdata__ Fan;

// Ventilatory jsou rucne prepnute na 50% vykon
#define IsFanLowPower()   (!Fan.AutoMode && Fan.Power < 100)

void FanInit();
  // Inicializace ventilatoru

void FanStart();
  // Start behu ventilatoru a nulovani chyby

void FanStop();
  // Stop behu ventilatoru a nulovani chyby

byte FanCalcPowerForDisplay(byte Power);
  // Prepocte vykon z rozsahu FAN_AUTO_MIN..FAN_AUTO_MAX na 0..100% pro zobrazeni

void FanCheck();
  // Zkontroluje beh a poruchu ventilatoru, volat 1x za sekundu.

#endif
