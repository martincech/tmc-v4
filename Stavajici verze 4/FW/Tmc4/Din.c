//*****************************************************************************
//
//    Din.c - Digitalni vstupy
//    Version 1.0
//
//*****************************************************************************

#include "Din.h"
#include "Hc165\Hc165.h"      // digital input

// Definice jednotlivych vstupu
byte __xdata__ DinCharging;             // YES/NO Dobiji se
byte __xdata__ DinDieselRun;            // YES/NO Bezi diesel
byte __xdata__ DinEmergency;            // YES/NO Nouzove ovladani z rozvadece
byte __xdata__ DinHeatingCoolingError;  // YES/NO Porucha topeni nebo chlazeni
byte __xdata__ DinHeatingFlame1;        // YES/NO Detekce plamene topeni 1 (topeni hori)
byte __xdata__ DinFan;                  // YES/NO Beh ventilatoru (vyhodnocovat spolecne s Ain)
byte __xdata__ DinTruckEngineRun;       // YES/NO Beh motoru vozidla
byte __xdata__ DinHeatingFlame2;        // YES/NO Detekce plamene topeni 2 (topeni hori)

//-----------------------------------------------------------------------------
// Inicializace
//-----------------------------------------------------------------------------

void DinInit() {
  // Inicializuje digitalnich vstupu
  DigitalInInit();   // Inicializace Hc165
}

//-----------------------------------------------------------------------------
// Cteni vstupu
//-----------------------------------------------------------------------------

void DinRead() {
  // Nacte vsechny digitalni vstupy a ulozi je do lokalnich promennych
  byte Value;

  Value = DigitalInRead();
  DinCharging            = (TYesNo)(Value & TmcDI1);
  DinDieselRun           = (TYesNo)(Value & TmcDI2);
  DinEmergency           = (TYesNo)(Value & TmcDI3);
  DinHeatingCoolingError = (TYesNo)(Value & TmcDI4);
  DinHeatingFlame1       = (TYesNo)(Value & TmcDI5);
  DinFan                 = (TYesNo)(Value & TmcDI6);
  DinTruckEngineRun      = (TYesNo)(Value & TmcDI7);
  DinHeatingFlame2       = (TYesNo)(Value & TmcDI8);
}



