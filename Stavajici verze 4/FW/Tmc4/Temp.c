//*****************************************************************************
//
//    Temp.c - Teplomery
//    Version 1.0
//
//*****************************************************************************

#include "Temp.h"
#include "Thermo\Thermo.h"      // Cteni teplot

// Definice teplot
int __xdata__  Temp[TEMP_COUNT];      // Teploty ve vnitrnim formatu
char __xdata__ Temp1C[TEMP_COUNT];    // Teploty zaokrouhlene na 1 stupen
int __xdata__  Temp01C[TEMP_COUNT];   // Teploty zaokrouhlene na desetinu stupne

// Celkovy pocet chyb (pro diagnostiku)
word __xdata__ TempTotalErrorCounter[TEMP_COUNT];       // Celkovy pocet neplatnych nacteni u jednotlivych teplomeru od zapnuti jednotky

#ifndef __TMCREMOTE__

// Promenne pro prumerovani teplot po celou dobu periody ukladani
// Perioda muze byt maximalne 99minut a merim kazdou sekundu => max. 99 * 60 = 5940 vzorku a max. suma muze byt 99 * 60 * 99.9C = 5880600 stupnu.
static long __xdata__ SumaTeplot[TEMP_COUNT];   // Suma na desetiny stupne
static word __xdata__ PocetTeplot[TEMP_COUNT];  // POcet prvku v sume

// Osetreni chybneho nacteni
static byte __xdata__ ErrorCounter[TEMP_COUNT]; // Pocet neplatnych nacteni u jednotlivych teplomeru
#define MAX_ERRORS      10                      // Maximalni pocet chyb pri nacitani teploty, pri kterem se ohlasi odpojene cidlo

//-----------------------------------------------------------------------------
// Inicializace
//-----------------------------------------------------------------------------

void TemperatureInit() {
  // Inicializuje teplomery (fce TempInit() uz je definovana)
  byte c;

  ThermoInit();
  // Nastavim pocatecni hodnoty teplot (kdyby bylo po startu odpojene cidlo, trcela by tam MAX_ERRORS sekund nahodna teplota) a vynuluju pocet chyb
  for (c = 0; c < TEMP_COUNT; c++) {
    Temp[c]                  = 0;
    Temp1C[c]                = 0;
    Temp01C[c]               = 0;
    ErrorCounter[c]          = 0;
    TempTotalErrorCounter[c] = 0;
  }//for
}

//-----------------------------------------------------------------------------
// Cteni teplot
//-----------------------------------------------------------------------------

void TemperatureRead() {
  // Nacte vsechny teploty a ulozi je do lokalnich promennych
  ThermoMeasure();
}

//-----------------------------------------------------------------------------
// Nulovani prumerovani
//-----------------------------------------------------------------------------

void TempClearAverages() {
  // Vynuluje prumerovani teplot
  byte c;
  for (c = 0; c < TEMP_COUNT; c++) {
    SumaTeplot[c]  = 0;
    PocetTeplot[c] = 0;
  }//for
}

//-----------------------------------------------------------------------------
// Vypocet prumeru teploty
//-----------------------------------------------------------------------------

int TempCountAverage(byte address) {
  // Vypocte a vrati prumer u zadane teploty. Teplota je na desetiny stupne (12.3C = 123)
  if (address >= TEMP_COUNT || PocetTeplot[address] == 0) {
    return TEMP_INVALID_01C;   // Nejaka divna adresa nebo v sume neni zadna tepota
  } else {
    // Nemusim nijak zaokrouhlovat, max. chyba bude 0.1C
    return (long)(SumaTeplot[address] / (long)PocetTeplot[address]);
  }//else
}

//-----------------------------------------------------------------------------
// Callback teploty
//-----------------------------------------------------------------------------

void AlogTemperature( byte channel, int value) {
  // Ulozi zadanou teplotu do lokalni promenne (tuto fci vola fce ThermoMeasure(), kvuli kompatibilite s Auto musi mit tento nazev)
  // Prototyp je definovany v Hardware.h, aby fce byla pristupna pro modul Thermo.c
  if (value == TEMP_INVALID) {
    // Teplotu se v tomto kroku nepodarilo nacist
    TempTotalErrorCounter[channel]++;           // Celkovy pocet chyb (pro diagnostiku)
    ErrorCounter[channel]++;
    if (ErrorCounter[channel] > MAX_ERRORS) {
      // Cidlo je opravdu odpojene
      ErrorCounter[channel] = MAX_ERRORS;       // Omezim, aby nepreteklo
      Temp[channel]    = AUTO_TEMP_ERROR;
      Temp1C[channel]  = TEMP_INVALID_1C;
      Temp01C[channel] = TEMP_INVALID_01C;
    }// else zatim necham puvodni hodnotu teplomeru
    return;
  }//if
  // Teplota v poradku
  Temp[channel]    = value;
  Temp1C[channel]  = TempConvert1C(value);
  Temp01C[channel] = TempConvert01C(value);
  // Nuluju pocet chybnych cteni
  ErrorCounter[channel] = 0;
  // Prictu zmerenou teplotu do sumy
  SumaTeplot[channel] += Temp01C[channel];  // Pricitam teplotu na desetiny
  PocetTeplot[channel]++;
} // AlogTemperature

//-----------------------------------------------------------------------------
// Vypocet prumeru teploty podle lozeni
//-----------------------------------------------------------------------------

#define Pricti(Temp)    \
  if (Temp != Error) {  \
    suma += Temp;       \
    count++;            \
  }

int TempLocalizationAverage(int Front, int Middle, int Rear, TLocalizationStatus Localization, int Error) {
  // Vypocte ze zadanych teplot prumer podle zadaneho lozeni. Teploty muzou byt ve vnitrnim formatu i zaokrouhlene, <Error> udava hodnotu pri chybe.
  long suma  = 0;
  byte count = 0;

  switch (Localization) {
    case LOC_CELE :
      // prumer ze vsech tri teplot
      Pricti(Front);
      Pricti(Middle);
      Pricti(Rear);
      break;
    case LOC_PREDNI_POLOVINA :
      // prumer ze prednich dvou teplot
      Pricti(Front);
      Pricti(Middle);
      break;
    case LOC_ZADNI_POLOVINA :
      // prumer ze zadnich dvou teplot
      Pricti(Middle);
      Pricti(Rear);
      break;
    case LOC_PREDNI_TRETINA :
      // prumer ze vsech tri teplot
      Pricti(Front);
      break;
    case LOC_STREDNI_TRETINA :
      // prumer ze vsech tri teplot
      Pricti(Middle);
      break;
    case LOC_ZADNI_TRETINA :
      // prumer ze vsech tri teplot
      Pricti(Rear);
      break;
  }
  if (count != 0) {
    return (suma / count);       // prumerna teplota z mericich cidel
  } else {
    return (Error);              // zadne cidlo nemeri
  }
}

#endif // __TMCREMOTE__

//-----------------------------------------------------------------------------
// Test rozsahu cilove teploty
//-----------------------------------------------------------------------------

TYesNo TempIsTargetOk(char Target) {
  if (Config.TempUnits == UNITS_CELSIUS && Target >= MIN_TARGET_TEMPERATURE_C && Target <= MAX_TARGET_TEMPERATURE_C
   || Config.TempUnits == UNITS_FAHRENHEIT && Target >= MIN_TARGET_TEMPERATURE_F && Target <= MAX_TARGET_TEMPERATURE_F) {
    return YES;
  }
  return NO;
}

//-----------------------------------------------------------------------------
// Prepocet teploty z Celsia na Fahrenheit
//-----------------------------------------------------------------------------

int TempToFahrenheit(int Celsius, TYesNo IsDecimal) {
  // Prepocte teplotu z Celsia na Fahrenheit. Pokud je IsDecimal true, teplota je v desetinach Celsia, jinak v celych stupnich.
  int i;

  // Pokud je teplota rovna poruse, nic neprevadim
  if (IsDecimal && Celsius == TEMP_INVALID_01C || Celsius == TEMP_INVALID_1C) {
    return Celsius;
  }

  i = 9 * Celsius / 5;
  i += IsDecimal ? 320 : 32;
  return i;
}

//-----------------------------------------------------------------------------
// Prepocet teploty z Fahrenheita na Celsius
//-----------------------------------------------------------------------------

#ifndef __TMCREMOTE__

int TempToCelsius(int Fahrenheit) {
  // Prepocte teplotu v celych stupnich Fahrenheita na Celsius, vysledek je ve vnitrnim formatu.
  int i;

  // Pokud je teplota rovna poruse, nic neprevadim
  if (Fahrenheit == TEMP_INVALID) {
    return Fahrenheit;
  }

  i = 50 * (Fahrenheit - 32) / 9;       // Prepocet na desetiny C
  return TempMk01C(i / 10, i % 10);     // Prepocet na vnitrni format v C
}

#endif // __TMCREMOTE__

//-----------------------------------------------------------------------------
// Prepocet teploty z Celsia na zvolene jednotky
//-----------------------------------------------------------------------------

int TempToDisplay(int Celsius, TYesNo IsDecimal) {
  // Prepocte teplotu z Celsia na zvolene jednotky v configu <Config.TempUnits>. Pokud je IsDecimal true, teplota je v desetinach Celsia, jinak v celych stupnich.
  if (Config.TempUnits == UNITS_CELSIUS) {
    return Celsius;
  }
  return TempToFahrenheit(Celsius, IsDecimal);
}

