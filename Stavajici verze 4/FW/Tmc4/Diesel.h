//*****************************************************************************
//
//    Diesel.c - Obsluha diesel motoru
//    Version 1.0, (c) Vymos
//
//*****************************************************************************

#ifndef __Diesel_H__
#define __Diesel_H__

#include "Hardware.h"
#include "DPF_Filter.h"

typedef struct {    
    byte On;                  // YES/NO bezi diesel
    word MotoHours;           // Pocet nabehanych motohodin v hodinach
    byte ChangeOil;           // YES/NO ma se vymenit olej s filtrem
    TDpfState DpfFilterState; // state of DPF filter regeneration
    byte Temperature;         // Teplota motoru
    byte Overheated;          // YES/NO prehraty diesel
    byte EcuError;            // YES/NO problem hlaseny z ECU motoru
    //internal
    word DpfFarWriteCounter;  // store dpf state to iep counter
    byte PrevOn;              // YES/NO diesel has been running in previous step
} TDiesel;

extern TDiesel __xdata__ Diesel;
extern TDiesel __xdata__ BackupDiesel;

#define DPF_FAR_WRITE_PERIOD_S  (60*5)

#define DSL_TEMP_STEPS        11       // Pocet kroku


void DieselInit(void);
// Nastaveni inicialniho stavu pocitadla motohodin

void DieselExecute(void);
// Aktualizace motohodin, volat min 1x za sekundu

void DieselSet(word Motohours, word Oil);
// Nastaveni poctu motohodin a vymeny oleje

void DieselQuitOil(void);
// Odkvituje vymenu oleje

//----- Interni funkce, nepouzivat :

word DieselReadMotohours(void);
// Cteni motohodin - vrati pocet motohodin

TYesNo DieselWriteMotohours(word MotoHour);
// Zapis motohodin

TYesNo DieselResetMotohours(void);
// Vymaze celou oblast

void DieselCheckTemp();
// Nacte teplotu dieselu a ulozi ji. Pokud se cidlo nevyuziva, ulozi nulu, pokud je cidlo odpojene (porucha), ulozi plnou hodnotu.
// Zajistuje i prumerovani.

void DieselStructureInit(TDiesel *diesel);
void SetChangeOil(TDiesel *diesel);
void DpfFilterCheck(TDiesel *state);
#endif
