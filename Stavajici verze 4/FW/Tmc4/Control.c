//*****************************************************************************
//
//    Control.c - ovladani ventilatoru, topeni a chlazeni
//    Version 1.0
//
//*****************************************************************************

#include "Control.h"
#include "Din.h"               // Digitalni vstupy
#include "Fan.h"               // Ventilatory
#include "Heating.h"           // Regulace topeni
#include "Cooling.h"           // Regulace chlazeni
#include "Servo.h"             // Kontrola stavu serv
#include "Temp.h"              // Teploty
#include "Alarm.h"             // Indikace poruch
#include "Co2.h"               // Koncentrace CO2
#include "Humidity.h"          // Relativni vlhkost
#include "AirPress.h"          // Tlak radiatoru a filtru
#include "FAir.h"              // Regulace cerstrveho vzduchu
#include "Diesel.h"            // Diesel filter DPF standstill ongoing
#include "Thermo\Thermo.h"     // Prevod teplot

TControl __xdata__ Control;

#ifndef __TMCREMOTE__

// Prepocet teploty na interni format regulatoru :
int __xdata__ RequestedTemperature;

// Pocitadlo od prepnuti na jiny rezim
static byte __xdata__ NewModeCounter = 0;
#define MAX_SENSORS_DELAY       30               // Cidla CO2 a vlhkosti zacnu merit az 30 sekund po prepnuti rezimu

// Teplota okoli je prilis nizka, regulaci omezim pouze na topeni (chlazeni zakazu)
#define OUTSIDE_TEMP_BELOW_COOLING      TempMk1C(10)         // Pri okolni teplote < 10C zakazu chlazeni, jen topim
#define IsOutsideBelowCooling() (Temp[TEMP_OUTSIDE] != AUTO_TEMP_ERROR && Temp[TEMP_OUTSIDE] < OUTSIDE_TEMP_BELOW_COOLING)

// Promenne pro zpozdeni prepnuti do nouyzoveho rezimu
static byte __xdata__ EmergencyCounter = 0;      // Pocitadlo trvani prepnuti do nouzoveho rezimu
#define MAX_EMERGENCY_TIME      4                // Maximalni doba v sekundach


//-----------------------------------------------------------------------------
// Inicializace
//-----------------------------------------------------------------------------

void ControlInit() {
  // Inicializace
  byte j;

  Control.Mode    = MODE_OFF;
  Control.NewMode = NO;
  Control.EmergencyMode = NO;
  for (j = TEMP_FRONT; j <= TEMP_REAR; j++) {
    Control.TempFaults[j].Fault = NO;
  }
  Control.OvershootSum = 0;
  HeatingInit();
  CoolingInit();
  Co2Init();
  HumidityInit();
  AirPressureInit();
}

//-----------------------------------------------------------------------------
// Kontrola poruch teplot ve skrini
//-----------------------------------------------------------------------------

static void CheckFaultsInBody() {
  // Zkontroluje poruchy vsech teplot ve skrini
  byte j;
  int Temp;   // Teplota v desetinach stupne

  for (j = TEMP_FRONT; j <= TEMP_REAR; j++) {
    // Cilova teplota i max. offsety jsou ve zvolenych jednotkach (C nebo F). Teploty ve skrini jsou ale vnitrne ve stupnich Celsia, musim je prepocitat
    if (Config.TempUnits == UNITS_CELSIUS) {
      Temp = Temp01C[j];    // Neni treba prepocet
    } else {
      Temp = TempToFahrenheit(Temp01C[j], YES);    // Prepocet na Fahrenheity
    }
    Control.TempFaults[j].Fault = NO; // Default bez chyby
    if (Temp == TEMP_INVALID_01C || Control.Mode == MODE_OFF) {
      // Teplota se nemeri nebo je prepinac rezimu vypnuty, chyba neni
      Control.TempFaults[j].OutsideRangeCounter = 0;
    } else if ((Temp >= (int)((int)10 * ((int)Config.TargetTemperature - (int)Config.MaxOffsetBelow))) && (Temp <= (int)((int)10 * ((int)Config.TargetTemperature + (int)Config.MaxOffsetAbove)))) {
      // Teplota je v pasmu cilove teploty
      Control.TempFaults[j].OutsideRangeCounter = 0;
      Control.TempFaults[j].RangeReached = YES;   // Nastavim flag, ze teplota dosahla pasma cilove teploty
    } else {
      // Teplota je mimo pasmo cilove teploty
      Control.TempFaults[j].OutsideRangeCounter++;
      if (Control.TempFaults[j].RangeReached) {
        // Predtim uz jsem nekdy dosahl pasma => jde o prekmit z pasma, ktery nesmi trvat moc dlouho
        if (Control.TempFaults[j].OutsideRangeCounter > TEPLOTA_MAX_DOBA_PREKMITU) {
          // Prekmit uz trva moc dlouho, vyhlasim alarm
          Control.TempFaults[j].OutsideRangeCounter = TEPLOTA_MAX_DOBA_PREKMITU;  // Aby mne to nepreteklo
          Control.TempFaults[j].Fault = YES;
        }//if
      } else {
        // Jeste jsem od prepnuti rezimu nedosahl pasma cilove teploty => jde o nabeh teploty na cilovou, coz muze trvat celkem dlouho
        if (Control.TempFaults[j].OutsideRangeCounter > TEPLOTA_MAX_DOBA_NABEHU) {
          // Nabeh teploty do pasma uz trva moc dlouho, vyhlasim alarm
          Control.TempFaults[j].OutsideRangeCounter = TEPLOTA_MAX_DOBA_NABEHU;  // Aby mne to nepreteklo
          Control.TempFaults[j].Fault = YES;
        }//if
      }//else
    }//else
    // Pokud je zadane cidlo mimo zvolene lozeni prepravek, nema urcite poruchu
    switch (j) {
      case TEMP_FRONT:  if (Config.Localization == LOC_ZADNI_POLOVINA || Config.Localization == LOC_STREDNI_TRETINA || Config.Localization == LOC_ZADNI_TRETINA) Control.TempFaults[j].Fault = NO; break;
      case TEMP_MIDDLE: if (Config.Localization == LOC_PREDNI_TRETINA || Config.Localization == LOC_ZADNI_TRETINA) Control.TempFaults[j].Fault = NO; break;
      case TEMP_REAR:   if (Config.Localization == LOC_PREDNI_POLOVINA || Config.Localization == LOC_PREDNI_TRETINA || Config.Localization == LOC_STREDNI_TRETINA) Control.TempFaults[j].Fault = NO; break;
    }//switch
    // Pokud je porucha, vyhlasim alarm
    if (Control.TempFaults[j].Fault) {
      AlarmSet();   // Vyhlasim alarm
    }
  }//for j
}

//-----------------------------------------------------------------------------
// Nulovani poruch teplot ve skrini
//-----------------------------------------------------------------------------

void ClearFaultsInBody() {
  // Vynuluje pocitadla poruch teploty (mimo pasmo cilove teploty) - volat po kazdem prepnuti rezimu
  byte Cislo;

  for (Cislo = 0; Cislo < PORUCHY_TEPLOT_POCET; Cislo++) {
    Control.TempFaults[Cislo].Fault               = NO;
    Control.TempFaults[Cislo].OutsideRangeCounter = 0;
    Control.TempFaults[Cislo].RangeReached        = NO;
  }//for
}

//-----------------------------------------------------------------------------
// Rozhodnuti o auto rezimu
//-----------------------------------------------------------------------------

static void CheckAutoMode() {
  // Pokud je v automatickem rezimu, podle aktualni teploty (zda je nad nebo pod cilovou teplotou) rozhodnu, zda budu topit
  // nebo chladit. Pri rozhodovani preferuju chlazeni.
  // Tato fce rozhoduje rezim pouze pri startu jednotky, tj. nebere v uvahu ve kterem rezimu se jednotka prave nachazi.
  if (Control.Mode != MODE_AUTO) {
    return;     // Neni v automatickem rezimu
  }

#ifdef USE_COOLING
  // Pouziva se chlazeni, tj. ma cenu rozhodovat, zda topit nebo chladit

  if (Control.AverageTemperature == AUTO_TEMP_ERROR) {
    // Vsechny teplomery ve skrini (podle lozeni) jsou v poruse, neni podle ceho rozhodnout, zda topit nebo chladit. Nastavim chlazeni a dal v programu
    // nastavim, aby nechladil (tj. bude se jen ventilovat)
    Control.AutoMode = AUTO_MODE_COOLING;
    return;
  }

  if (IsOutsideBelowCooling()) {
    // Okolni teplota je prilis nizka, prepnu na topeni nehlede na teplotu uvnitr skrine
    Control.AutoMode = AUTO_MODE_HEATING;
    return;
  }

  if (Control.AverageTemperature > RequestedTemperature + Config.AutoHysteresis) {
    // Teplota ve skrini je vyssi nez cilova + hystereze, zapnu chlazeni nehlede na okolni teplotu
    Control.AutoMode = AUTO_MODE_COOLING;
    return;
  }

  if (Control.AverageTemperature < RequestedTemperature - Config.AutoHysteresis) {
    // Teplota ve skrini je nizsi nez cilova - hystereze, zapnu topeni nehlede na okolni teplotu
    Control.AutoMode = AUTO_MODE_HEATING;
    return;
  }

  // Teplota ve skrini je v okoli cilove teploty v pasmu hystereze prechodu => beru v uvahu i teplotu okoli
  if (Temp[TEMP_OUTSIDE] != AUTO_TEMP_ERROR) {
    // Cidlo venkovni teploty meri v poradku
    if (Temp[TEMP_OUTSIDE] < Control.AverageTemperature - Config.AutoHysteresis) {
      // Okolni teplota je nizsi nez nizsi mez pasma hsytereze prechodu => topim
      Control.AutoMode = AUTO_MODE_HEATING;
    } else {
      // Okolni teplota je v pasmu hystereze kolem cilove teploty nebo nad => chladim. Timto lehce preferuji chlazeni.
      Control.AutoMode = AUTO_MODE_COOLING;
    }
  } else {
    // Venkovni teplotu nemam k dispozici, rozhodnu bez ni
    if (Control.AverageTemperature >= RequestedTemperature) {
      Control.AutoMode = AUTO_MODE_COOLING;
    } else {
      Control.AutoMode = AUTO_MODE_HEATING;
    }
  }//else

#else
  // Nepouziva chlazeni, prepnu vzdy na topeni

  Control.AutoMode = AUTO_MODE_HEATING;

#endif // USE_COOLING

}

//-----------------------------------------------------------------------------
// Kontrola nouzoveho rezimu
//-----------------------------------------------------------------------------

static void CheckEmergencyMode() {
  // Zjistim, zda neovlada nouzove z rozvadece
  Control.OldEmergencyMode = Control.EmergencyMode;     // Zapamatuju si predchozi krok

  Control.EmergencyMode = NO;         // Default neni
  if (DinEmergency == DIN_EMERGENCY_ON) {
    // Din indikuje prepnuti do nouzoveho rezimu. Prepnu az po uplynuti nejake doby kvuli ruseni.
    EmergencyCounter++;
    if (EmergencyCounter > MAX_EMERGENCY_TIME) {
      // Porucha uz trva dlouho
      EmergencyCounter = MAX_EMERGENCY_TIME;    // Aby pocitadlo nepreteklo
      Control.EmergencyMode = YES;              // Vyhlasim
    }
  } else {
    // Neni nouzovy rezim
    EmergencyCounter = 0;
  }
}

//-----------------------------------------------------------------------------
// Krok
//-----------------------------------------------------------------------------

void ControlExecute() {
  // Krok rizeni, volat 1x za sekundu

  // Zjistim, zda neovlada nouzove z rozvadece
  CheckEmergencyMode();
  if (Control.EmergencyMode || Diesel.DpfFilterState == DPF_STANDSTILL_ONGOING || BackupDiesel.DpfFilterState == DPF_STANDSTILL_ONGOING) {
    // Vse vypnu
    Control.Mode    = MODE_OFF;
    Control.NewMode = YES;
    ServoInit();        // Vynuluju poruchy serv, jinak po prepnuti zpet na normalni rezim blikaji poruchy serv, dokud serva nedobehnou na zadanou polohu
  }
  // Vypoctu prumernou teplotu podle lozeni
  Control.AverageTemperature = TempLocalizationAverage(Temp[TEMP_FRONT], Temp[TEMP_MIDDLE], Temp[TEMP_REAR], Config.Localization, AUTO_TEMP_ERROR);
  // Prepocet teploty na interni format regulatoru :
  if (Config.TempUnits == UNITS_CELSIUS) {
    RequestedTemperature = TempMk1C(Config.TargetTemperature);
  } else {
    RequestedTemperature = TempToCelsius(Config.TargetTemperature);          // Prepocet z Fahrenheitu na Celsia ve vnitrnim formatu, cele stupne Celsia nestaci
  }
  // Zkontroluju poruchy teplot ve skrini
  CheckFaultsInBody();
  // Vypoctu aktualni hodnoty tlaku sani a vytlaku chlazeni - pocitam i kdyz chlazeni nejede
  CoolingCalcPressure();

  if (Control.NewMode) {
    // Prave prepnul na jiny rezim, flag vynuluju az na konci fce, jeste se dale vyuziva
    CheckAutoMode();            // Pokud prepnul na autorezim, rozhodnu, zda se bude topit nebo chladit
    ClearFaultsInBody();
    FAirNewAutoMode();          // Inicializace PID regulatoru klapek
    // K prekresleni rezimu dojde automaticky, neni treba nastavovat promennou Redraw
  } else {
    // Puvodni rezim trva
    if (NewModeCounter < MAX_SENSORS_DELAY) {
      NewModeCounter++;
    }
    if (Control.Mode != MODE_OFF) {
      AirPressureCalc();
      if (NewModeCounter >= MAX_SENSORS_DELAY) {
        // Merim pouze pokud jede ventilace (cidla jsou jinak vypnuta)
        // Od zmeny rezimu ubehnul potrebny cas, muzu merit CO2 a vlhkost. Cidlo CO2 cca 20sek po zapnuti napajeni meri spatne
        Co2Calc();
        HumidityCalc();
      }
    }
  }

  switch (Control.Mode) {
    case MODE_OFF: {
      // Vypnute
      if (Control.NewMode) {    // Prave prepnul na tento rezim
        FanStop();
        CoolingStop();
        HeatingStop();
      }
      break;
    }
    case MODE_AUTO: {
      // Automat
      if (Control.NewMode) {    // Prave prepnul na tento rezim
        FanStart();
        if (Control.AutoMode == AUTO_MODE_HEATING) {
          // Automaticke topeni
          HeatingAutoStart();
          Control.OvershootSum = 0;         // Vynuluju integral prekmitu
        } else {
          // Automaticke chlazeni
          HeatingStop();
        }
      }
      if (Control.AutoMode == AUTO_MODE_HEATING) {
        // Automaticke topeni
        CoolingStop();    // Chlazeni muzu vypnout
        HeatingAutoExecute(Control.AverageTemperature);

#ifdef USE_COOLING
        // Pouziva se chlazeni, tj. ma cenu rozhodovat, prepnout do chlazeni

        // Pokud teplota hodne vzrostla a uz je plne zavrene topeni, prepnu se na chlazeni.
        // Pokud je teplota okoli prilis nizka, do rezimu chlazeni vubec neprepinam, ponecham v topeni (v pripade, ze je teplota uvnitr skrine vyssi nez
        // cilova, otevrou se klapky)
        if (Control.AverageTemperature == AUTO_TEMP_ERROR) {
          // Vsechna cidla ve skrini jsou v poruse (podle lozeni), prepnu hned na chlazeni
          Control.AutoMode = AUTO_MODE_COOLING;
          break;
        }

        if (HeatingFullyClosed() && !IsOutsideBelowCooling()) {
          // Servo topeni je uzavrene a okolni teplota je dostatecne vysoka, takze bych mohl prepnout do chlazeni
          if (Control.AverageTemperature >= RequestedTemperature + Config.AutoHysteresis) {
            // Teplota ve skrini prekrocila prvni hysterezi, pocitam dobu, po kterou trva prekmit
            Control.OvershootSum += Control.AverageTemperature - (RequestedTemperature + Config.AutoHysteresis);
            if (Control.OvershootSum >= TempMk1C(Config.MaxOvershootSum)) {
              // Prekmit uz trva moc dlouho, prepnu do chlazeni
              Control.AutoMode = AUTO_MODE_COOLING;
              FAirNewAutoMode();
            }
            break;
          }
          Control.OvershootSum = 0;             // Teplota je pod hysterezi, vynuluju integral prekmitu
        }

#endif // USE_COOLING

      } else {
        // Automaticke chlazeni
        HeatingStop();  // Topeni muzu vypnout
        CoolingAutoExecute(Control.AverageTemperature);
        // Pokud jsou v poruse vsechna cidla ve skrini (podle lozeni), zustavam v rezimu chlazeni
        if (Control.AverageTemperature == AUTO_TEMP_ERROR) {
          break;        // Zustavam v rezimu chlazeni
        }
        // Pokud teplota hodne poklesla, prepnu se na topeni
        // Prilis nizkou teplotu okoli zde nehlidam - pokud uz se jedno do chlazeni prepne, zustane to v chlazeni i pri pripadnem poklesu okolni teploty.
        // Zabranim tak pripanym oscilacim mezi topenim a chlazenim pri teplote oscilujici kolem 10C.
        if (Control.AverageTemperature <= RequestedTemperature - Config.AutoHysteresis) {
          // Teplota uz hodne poklesla, prepnu regulator na topeni
          Control.AutoMode = AUTO_MODE_HEATING;
          HeatingAutoStart();
          FAirNewAutoMode();
          Control.OvershootSum = 0;             // Vynuluju integral prekmitu
        }
      }//else
      break;
    }
    case MODE_VENTILATION: {
      // Ventilace
      if (Control.NewMode) {    // Prave prepnul na tento rezim
        FanStart();
        CoolingStop();
        HeatingStop();
      }
      break;
    }
  }//switch
  Control.NewMode = NO;         // Zpracoval jsem rezim, vynuluju
}

//-----------------------------------------------------------------------------
// Prepocet rezimu pro logovani
//-----------------------------------------------------------------------------

TLogControlMode ControlModeLog() {
  // Vrati aktualni rezim pro logovani
  switch (Control.Mode) {
    case MODE_OFF: {
      return MODE_LOG_OFF;
    }
    case MODE_AUTO: {
      if (Control.AutoMode == AUTO_MODE_HEATING) {
        return MODE_LOG_AUTO_HEATING;
      } else {
        return MODE_LOG_AUTO_COOLING;
      }
    }
    default: {
      return Control.Mode + 1;          // Dale je to posunute o 1
    }
  }//switch
}

//-----------------------------------------------------------------------------
// Nastaveni noveho rezimu
//-----------------------------------------------------------------------------

void ControlSetNewMode(TControlMode Mode) {
  // Nastavi novy rezim
  Control.Mode    = Mode;
  Control.AutoMode = AUTO_MODE_COOLING;  
#ifndef USE_COOLING
  // Pokud nepouziva chlazeni, standardne se preferuje chlazeni a po prepnuti do auto rezimu problikava v prvnim kroku na displeji rezim chlazeni
  Control.AutoMode = AUTO_MODE_HEATING;
#endif // USE_COOLING

  Control.NewMode = YES;           // Flag, ze se zmenil rezim, nuluje se automaticky po zpracovani rezimu

  // Snuluju pocitadlo po zmene rezimu, snuluju cidla
  NewModeCounter = 0;
  Co2Init();
  HumidityInit();
  AirPressureInit();
}

//-----------------------------------------------------------------------------
// Snulovani stridy
//-----------------------------------------------------------------------------

void ControlResetDutyCycle() {
  // Snulovani stridy plamene topeni a chodu kompresoru
  HeatingResetDutyCycle();
  CoolingResetDutyCycle();
}

#endif // __TMCREMOTE__
