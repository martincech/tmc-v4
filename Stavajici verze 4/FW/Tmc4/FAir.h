//*****************************************************************************
//
//    FAir.c - Ovladani cerstveho vzduchu
//    Version 1.0
//
//*****************************************************************************


#ifndef __FAir_H__
   #define __FAir_H__

#include "Hardware.h"     // pro podminenou kompilaci
#include "Pid\Pid.h"        // PID regulator

// Ovladani cerstveho vzduchu
typedef enum {
  FAIR_AUTO,                            // Cerstvy vzduch se reguluje automaticky
  FAIR_MANUAL,                           // Cerstvy vzduch se reguluje rucne
  _FAIR_LAST = FAIR_MANUAL,
  _FAIR_COUNT
} TFreshAirMode;

typedef enum {
  FLAP_AUTO,                            // Klapka se reguluje automaticky
  FLAP_MANUAL,                          // Klapka se reguluje rucne
  _FLAP_LAST = FLAP_MANUAL,
  _FLAP_COUNT
} TFlapMode;

typedef struct {
  TFlapMode Mode;                       // Typ rizeni
  byte      Percent;                    // Aktualni hodnota otevreni klapky v procentech
  byte      Position;                   // Poloha klapky pro zobrazeni a logovani
} TFlap;

// Struktura cerstveho vzduchu
typedef struct {
  TFreshAirMode Mode;                   // Typ rizeni
  byte          Percent;                // Aktualni hodnota cerstveho vzduchu v procentech
  byte          Position;               // Poloha klapky pro zobrazeni a logovani
  TFlap         FloorFlap;              // Podlahova klapka
  TPid          Pid;                    // PID regulace pootevirani klapek
} TFreshAir;
extern TFreshAir __xdata__ FreshAir;

#define FAIR_FLAP_STEPS  16             // Pocet kroku pro zobrazeni klapky

void FAirInit();
  // Inicializace

void FAirRegulatorUpdateParameters();
  // Preberu parametry z configu do PID regulatoru

void FAirNewMode(TFreshAirMode Mode);
  // Nastavi novy rezim cerstveho vzduchu

void FAirNewAutoMode();
  // Novy automaticky rezim (prepnuti z auto topeni na chlazeni a naopak)

void FAirManualIncrease();
  // Pridani cerstveho vzduchu v rucnim rezimu

void FAirManualDecrease();
  // Ubrani cerstveho vzduchu v rucnim rezimu

void FAirNewFloorFlapMode(TFlapMode Mode);
  // Nastavi novy rezim klapky v podlaze

void FAirFloorFlapManualIncrease();
  // Pridani (otevreni) podlahove klapky v rucnim rezimu

void FAirFloorFlapManualDecrease();
  // Ubrani (zavreni) podlahove klapky v rucnim rezimu

byte FAirCalculateServoPosition(byte Percent);
  // Prepocte hodnotu v procentech na polohu serva, procenta jsou v mezich FAIR_MIN az FAIR_MAX, pocet poloh serva je FAIR_FLAP_STEPS

void FAirExecute();
  // Nastavi serva naporu, recirkulace a podlahy do pozadovane polohy

#endif
