//*****************************************************************************
//
//    LogData.h - TMC log data structure
//    Version 1.0, (c) Vymos
//
//*****************************************************************************


#ifndef __LogData_H__
   #define __LogData_H__

#ifndef __Hardware_H__
   #include "Hardware.h"     // pro podminenou kompilaci
#endif

//#include "AirPress.h"          // Tlak radiatoru a filtru
//#include "Co2.h"               // Koncentrace CO2

//#error "LogData included"

//-----------------------------------------------------------------------------
// Struktura zaznamu Logu
//-----------------------------------------------------------------------------

// Komprimovany datum a cas
typedef struct {
   byte Min        : 6;
   byte Hour_lo    : 2;     // spodni 2 bity
   byte Hour_hi    : 3;     // horni  3 bity
   byte Day        : 5;
   byte Month      : 4;
   byte Year       : 3;     // rok % 8
   byte Changed    : 1;     // priznak zmeny nastaveni RTC
} TShortDateTime;   // Velikost 3 bajty

// Ukladani roku: z roku se do struktury ukladaji pouze spodni 3 bity. Rekonstrukce pak probiha tak, ze hornich 13 bitu se vezme z aktualniho roku
// a doplni se temito 3 bity ze struktury a mam rok zrekonstruovany. Pokud je takto vznikly rok vetsi nez aktualni rok, odecte se od nej 8,
// protoze novejsi datum nez aktualni to byt nemohl. Funguje to pro az 8 let stare zaznamy.

// zapis hodin do struktury :
#define mkhour( dt, h)     dt.Hour_lo = (h) & 0x03; dt.Hour_hi = ((h) >> 2) & 0x07
// cteni hodin ze struktury :
#define gethour( dt)       ((dt.Hour_lo) | (dt.Hour_hi << 2))
// zapis roku do struktury :
#define mkyear( dt, y)     dt.Year = (y) & 0x07

#define LOG_DUTY_CYCLE_LSB          10             // Krok stridy 0-100% pro ukladani tak, aby 100% vyslo na 4 bity (0-15, tj. 100/10=10). Schvalne volim krok 10, aby se presne ulozilo i 100%.
#define LOG_HEATING_WATER_TEMP_LSB  2              // Teplota vody na vstupu Webasta 0-100C pro ukladani tak, aby 100C vyslo na 6 bitu (0-64, tj. 100/2=50)
#define LOG_HUMIDITY_LSB            2              // Relativni vlhkost 0-100% pro ukladani tak, aby 100% vyslo na 6 bitu
#define LOG_AIR_PRESSURE_LSB        AIR_PRESSURE_LSB
#define LOG_CO2_LSB                 CO2_LSB

// Struktura pro zaznam
typedef struct SLog {
  // Cilovou teplotu necham na prvnim miste, kde je marker -127
  char TargetTemperature;                   // Cilova teplota na cele stupne vcetne znamenka

  word FrontTemp                    : 10,   // Absolutni hodnota na desetiny C
       FrontTempSign                : 1,    // Znamenko
       FrontTempOffset              : 1,    // YES/No teplota mimo meze
       FreshAirLsb                  : 4;    // LSB mnozstvi cerstveho vzduchu 0-100%

  word MiddleTemp                   : 10,   // Absolutni hodnota na desetiny C
       MiddleTempSign               : 1,    // Znamenko
       MiddleTempOffset             : 1,    // YES/No teplota mimo meze
       FloorPosition                : 4;    // Poloha serva v podlaze

  word RearTemp                     : 10,   // Absolutni hodnota na desetiny C
       RearTempSign                 : 1,    // Znamenko
       RearTempOffset               : 1,    // YES/No teplota mimo meze
       CoolingDutyCycle             : 4;    // Strida chodu kompresoru s dilkem LOG_DUTY_CYCLE_LSB

  word ChannelTemp                  : 10,   // Absolutni hodnota na desetiny C
       ChannelTempSign              : 1,    // Znamenko
       Voltage                      : 5;    // Napeti ve voltech

  word OutsideTemp                  : 10,   // Absolutni hodnota na desetiny C
       OutsideTempSign              : 1,    // Znamenko
       HeatingFlame1DutyCycle       : 4,    // Strida horeni plamene topeni 1 s dilkem LOG_DUTY_CYCLE_LSB
       LowVoltage                   : 1;    // YES/NO nizke napeti akumulatoru

  word RecirculationTemp            : 10,   // Absolutni hodnota na desetiny C
       RecirculationTempSign        : 1,    // Znamenko
       DieselTemperature            : 4,    // Teplota dieselu
       DieselUse                    : 1;    // YES/NO pouziti diesel motoru v prepravniku

  byte HeatingPosition              : 4,    // Poloha serva topeni
       HeatingServoStatus           : 2,    // Stav serva topeni
       RecirculationServoStatus     : 2;    // Stav serva recirkulace

  byte TopInductionServoStatus      : 2,    // Stav horniho naporoveho serva
       BottomInductionServoStatus   : 2,    // Stav spodniho naporoveho serva
       HeatingFlame2DutyCycle       : 4;    // Strida horeni plamene topeni 2 s dilkem LOG_DUTY_CYCLE_LSB

  byte Mode                         : 2,    // Zvoleny rezim (TLogControlMode)
       Humidity                     : 6;    // Relativni vlhkost v % s krokem LOG_HUMIDITY_LSB

  byte FloorServoStatus             : 2,    // Stav serva v podlaze
       HeatingWaterTemperature      : 6;    // Teplota vody na vstupu Webasta ve stupnich Celsia s krokem LOG_HEATING_WATER_TEMP_LSB

  byte Driver                       : 7,    // Cislo logovaneho ridice, 0-99
       GsmDummy                     : 1;    // GSM modul uz se nepouziva

  byte FiltersAirPressure           : 7,    // Tlakova ztrata na filtrech s dilkem LOG_AIR_PRESSURE_LSB
       ElMotorOn                    : 1;    // YES/NO zapnuty elektromotor

  byte FansAirPressure              : 7,    // Tlakova ztrata na ventilatorech s dilkem LOG_AIR_PRESSURE_LSB
       FanFailure                   : 1;    // YES/NO porucha ventilatoru

  byte Co2                          : 7,    // Koncentrace CO2 s dilkem LOG_CO2_LSB ppm
       CoolingFailure               : 1;    // YES/NO porucha chlazeni

  byte SuctionPressure              : 6,    // Tlak sani kompresoru 0-6.3 bar
       ChargingFailure              : 1,    // YES/NO porucha bobijeni
       DieselOn                     : 1;    // YES/NO diesel bezi

  byte DischargePressure;                   // Tlak vytlaku kompresoru 0-25.5 bar

  byte Localization                 : 3,    // Lozeni prepravek (TLocalizationStatus)
       HeatingCircuitState          : 3,    // Stav ohrevu vody ve spodnim okruhu (THeatingCircuitState)
       ChangeOil                    : 1,    // YES/NO je indikovana vymena oleje
       HeatingFailure               : 1;    // YES/NO porucha topeni

  byte Alarm                        : 1,    // YES/NO je hlasen alarm
       ManualFreshAir               : 1,    // YES/NO rucne/automaticky cerstvy vzduch
       ManualFloor                  : 1,    // YES/NO rucne/automaticky klapka v podlaze
       EmergencyMode                : 1,    // YES/NO nouzove ovladani z rozvadece
       TruckEngineRun               : 1,    // YES/NO Beh motoru vozidla
       FanAutoMode                  : 1,    // YES/NO ventilatory prepnute na automaticky regulator vykonu
       FiltersPressureOverLimit     : 1,    // YES/NO tlakova ztrata na vzduchovych filtrech je prilis vysoka
       Co2OverLimit                 : 1;    // YES/NO koncentrace CO2 je prilis vysoka

  byte FanPower                     : 7,    // Vykon ventilatoru 0-100%
       Co2Failure                   : 1;    // YES/NO porucha cidla CO2

  byte FreshAirMsb                  : 3,    // MSB mnozstvi cerstveho vzduchu 0-100%
       Dummy                        : 5;

  // Datum a cas zaznamu - az na konci, na toto misto se vyplnuje i datum a cas pri protokolovani
  TShortDateTime DateTime;

} TLog;  // 30 bajtu

// Konstanty pro ukladani do EEPROM FIFO :
#define TMC_FIFO_START            0  // Pocatecni adresa FIFO v EEPROM
#define TMC_FIFO_CAPACITY       1089 // Maximalni pocet ukladanych polozek
#define TMC_FIFO_MARKER_EMPTY  -127  // Znacka konce pri neuplnem zaplneni - teplota
#define TMC_FIFO_MARKER_FULL   -128  // znacka konce pri prepisovani
#define TMC_FIFO_PROTOCOL_TYPE -126  // Pokud je prvni bajt zaznamu roven tomuto, jde o zaznam s protokolem cinnosti ridice. Pokud ne, jde o normalni zaznam

// Datovy typ popisujici strukturu logu (v EEPROM) :
typedef TLog TLogArray[TMC_FIFO_CAPACITY];   // datovy logger 32676 bajtu


#endif
