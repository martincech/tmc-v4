//*****************************************************************************
//
//    Diag.c - diagnosticke zobrazeni
//    Version 1.0
//
//*****************************************************************************

#include "Diag.h"
#include "MenuPos.h"           // Souradnice pro zobrazeni
#include "Din.h"               // Digitalni vstupy
#include "Dout.h"              // Digitalni vystupy
#include "Ain.h"               // Analogove vstupy
#include "Temp.h"              // Teploty
#include "Heating.h"           // Regulace topeni
#include "Control.h"           // Automaticke rizeni
#include "Cooling.h"           // Strida behu chlazeni
#include "Accu.h"              // Napeti a proud akumulatoru
#include "CO2.h"               // Regulator CO2
#include "FAir.h"              // Regulace cerstrveho vzduchu
#include "Sed1335\Sed1335.h"    // display
#include "conio.h"      // jednoduchy display
#include "bcd.h"        // BCD aritmetika pro prevody
#include "Thermo\Thermo.h"     // Prevod teplot (pro diagnostiku)
#include "Diesel.h"
#include "..\Tmcr4\Tmcr.h"      // Stav komunikace

#ifndef __TMCREMOTE__

static void DrawDin (byte Input) {
   putchar (Input ? 'H' : 'L');
}

static void DrawAin (byte Input) {
   cprinthex (Input, 2); putchar (' ');
}

static void DrawAinVoltage (byte Conversion, byte Increment) {
   // Prevede prevod Conversion z ADC s referenci natvrdo 10V na volty. K vysledku pricte napeti <Increment> v desetinach voltu.
   word w;
   w = (word)((word)100 * (word)Conversion / 0x00FF + (word)Increment);
   cprintdec (w / 10, FMT_UNSIGNED | 2);
   cputs (".");
   cprintdec (w % 10, FMT_UNSIGNED | 1);
}

static void DrawDutyCycle (byte x, byte y, TDutyCycle xdata *DutyCycle) {
   DisplayGotoRC (x, y);
   cprintdec (DutyCycle->OnCounter, FMT_UNSIGNED | 4); putchar ('/'); 
   cprintdec (DutyCycle->OffCounter, FMT_UNSIGNED | 4); putchar ('='); 
   cprintdec (DutyCycle->Percent, FMT_UNSIGNED | 3); putchar ('%');
}

static void DrawFloat01 (byte value) {
   cprintdec (value / 10, FMT_UNSIGNED | 2);
   cputs (".");
   cprintdec (value % 10, FMT_UNSIGNED | 1);
}

static void DrawTempError (byte x, byte y, byte index) {
   DisplayGotoRC (x, y);
   cprintdec (TempTotalErrorCounter[index], FMT_UNSIGNED | 4);
}

static void DrawTempPidError (TPid __xdata__ *Pid) {
   int Rozdil;
   Rozdil = Pid->Error << 4;      // Bitovy posuv, teploty do regulatoru zadavam posunute
   cputs ("e="); if (Pid->Error < 0) putchar ('-'); else putchar (' '); cprintdec (TempGet1C (Rozdil), FMT_UNSIGNED | 2); putchar ('.'); cprinthex (bbin2bcd (TempGet001C (Rozdil)), FMT_LEADING_0 | 2);
}

static void DrawPid (TPid __xdata__ *Pid) {
   cputs (" p="); cprintdec (Pid->Proportional / Pid->LsbDenominator, 3);
   cputs (" i="); cprintdec (Pid->Integral / (Pid->IntegralScale * Pid->LsbDenominator), FMT_UNSIGNED | 3);
   cputs (" d="); cprintdec (Pid->Derivative / Pid->LsbDenominator, 3);
   cputs (" o="); cprintdec (Pid->Output, FMT_UNSIGNED | 3);
}

#endif // __TMCREMOTE__


void DiagRedraw () {
   // Prekresleni displeje
   int Rozdil;

   DisplaySetFont (FONT_SYSTEM);
   DisplayGotoRC(0,0);
   cprintdec(Control.AutoMode, 2);
   DisplayGotoRC(0,4);
   cprintdec(Control.Mode, 2);
   // Pocet chyb teplot
#ifndef __TMCREMOTE__
   DrawTempError (TEMP_Y1 / 8, 7, TEMP_OUTSIDE);
   DrawTempError (TEMP_Y2 / 8, 7, TEMP_CHANNEL);
   DrawTempError (TEMP_Y3 / 8, 7, TEMP_RECIRCULATION);
#ifdef USE_TWO_SENSORS
   DrawTempError (TEMP_BODY_Y1 / 8, 22, TEMP_FRONT);
   DrawTempError (TEMP_BODY_Y2 / 8, 22, TEMP_REAR);
#else
   DrawTempError (TEMP_BODY_Y1 / 8, 22, TEMP_FRONT);
   DrawTempError (TEMP_BODY_Y2 / 8, 22, TEMP_MIDDLE);
   DrawTempError (TEMP_BODY_Y3 / 8, 22, TEMP_REAR);
#endif // USE_TWO_SENSORS
#endif // __TMCREMOTE__

   // Prumer teplot ve skrini
   DisplayGotoRC (0, 12);
   Rozdil = Control.AverageTemperature;  // Vyuziju lokalni promennou Rozdil
   cputs ("avg "); if (Rozdil < 0) putchar ('-'); else putchar (' '); cprintdec (TempGet1C (Rozdil), FMT_UNSIGNED | 2); putchar ('.'); cprinthex (bbin2bcd (TempGet001C (Rozdil)), FMT_LEADING_0 | 2);

   // Otepleni / ochlazeni
   Rozdil = Temp01C[TEMP_CHANNEL] - Temp01C[TEMP_OUTSIDE];
#if (SENSORS_NUMBER_OF_ROWS == 1)
   DisplayGotoRC (TEMP_Y2 / 8 - 1, 0);
#else
   DisplayGotoRC (TEMP_Y2 / 8, 0);
#endif
   if (Rozdil < 0) {
      putchar ('-');
      Rozdil = -Rozdil;
   }
   else {
      putchar (' ');
   }
   cprintdec (Rozdil / 10, FMT_UNSIGNED | 2); putchar ('.'); cprinthex (bbin2bcd (Rozdil % 10), FMT_LEADING_0 | 1);

   // Regulator teploty
#ifndef __TMCREMOTE__
#if (SENSORS_NUMBER_OF_ROWS == 0)
   DisplayGotoRC (FAULTS_Y / 8, 0);
#elif (SENSORS_NUMBER_OF_ROWS == 1)
   DisplayGotoRC (MENU_Y / 8, 0);
#else
   DisplayGotoRC (FAULTS_Y / 8, 0);
#endif
   DrawTempPidError (&Heating.Pid);
   DrawPid (&Heating.Pid);
   cputs ("   "); cprintdec (TempGet1C (Control.OvershootSum), FMT_UNSIGNED | 3);
#endif // __TMCREMOTE__

   // Regulator CO2
#ifndef __TMCREMOTE__
#ifdef USE_CO2
#if (SENSORS_NUMBER_OF_ROWS == 1)
   DisplayGotoRC (MENU_Y / 8 + 1, 0);
#else
   DisplayGotoRC (MENU_Y / 8 + 1, 0);
#endif
   cputs ("CO2 e="); cprintdec (Co2.Pid.Error, 4);
   DrawPid (&Co2.Pid);
#endif // USE_CO2
#endif // __TMCREMOTE__

   // Regulator pootevirani klapek
#ifndef __TMCREMOTE__
   DisplayGotoRC (29, 0);
   cputs ("FA ");
   DrawTempPidError (&FreshAir.Pid);
   DrawPid (&FreshAir.Pid);
#endif // __TMCREMOTE__


#ifdef __TMCREMOTE__

   // Stav komunikace
   DisplayGotoRC (16, 27);
   cprintdec ((unsigned)PacketCounterTotalOk, FMT_UNSIGNED | 5);
   cputs ("/");
   cprintdec ((unsigned)PacketCounterTotalErr, FMT_UNSIGNED | 5);

#else
   // Din
   DisplayGotoRC (16, 30);
   cputs ("I:");
   DrawDin (DinCharging);
   DrawDin (DinDieselRun);
   DrawDin (DinEmergency);
   DrawDin (DinHeatingCoolingError);
   DrawDin (DinHeatingFlame1);
   DrawDin (DinFan);
   DrawDin (DinTruckEngineRun);
   DrawDin (DinHeatingFlame2);

   // Dout
   DisplayGotoRC (19, 34);
   cputs ("O:");
   DrawDin (DoutCooling);
   DrawDin (DoutHeatingAndPump);
   DrawDin (DoutVentilation);
   DrawDin (DoutHeatingPump);

   // Ain
   // Serva napor
   DisplayGotoRC (FLAPS_Y1 / 8 - 1, 1);
   DrawAinVoltage (AinServo[SERVO_INDUCTION1], 1);        // U serva naporu a podlahy pridavam 0.1V
   putchar (' ');
   DrawAinVoltage (AinServo[SERVO_INDUCTION2], 1);
   // Recirkulacni servo
   DisplayGotoRC (FLAPS_Y2 / 8 + 1, 0);
   DrawAinVoltage (AinServo[SERVO_RECIRCULATION], 1);
   // Servo v podlaze
   DisplayGotoRC (FLOOR_FLAPS_Y1 / 8 + 1, 12);
   DrawAinVoltage (AinServo[SERVO_FLOOR], 1);
   // Servo topeni
   DisplayGotoRC (6, 35);
   DrawAinVoltage (AinServo[SERVO_HEATING], 2);           // U serva topeni pridavam 0.2V

   // Strida chlazeni
   DrawDutyCycle (9, 26, &Cooling.DutyCycle);

   // Strida plamene topeni 1 a 2
   DrawDutyCycle (10, 26, &Heating.Flame1DutyCycle);
   DrawDutyCycle (11, 26, &Heating.Flame2DutyCycle);

   // Tlak chladiva
   DisplayGotoRC (15, 26);
   cputs ("P="); DrawFloat01 (Cooling.SuctionPressure); putchar ('/'); DrawFloat01 (Cooling.DischargePressure);

   // Teplota vody topeni
   DisplayGotoRC (6, 26);
   cputs ("W="); cprintdec (Heating.WaterTemperature, FMT_UNSIGNED | 2);

   // Prumerny vykon topeni
   DisplayGotoRC (6, 32);
   cprintdec (Heating.AveragePercent, FMT_UNSIGNED | 3);

   DisplayGotoRC (FLAPS_Y2 / 8 - 1, 0);
   cputs ("Ain:");
   DrawAin (AinSuctionPressure);
   DrawAin (AinDischargePressure);
   DrawAin (AinWaterTemperature);
   DrawAin (AinCo2);
   DrawAin (AinHumidity);
   DrawAin (AinFiltersPressure);
   DrawAin (AinLogoReady);
   DrawAin (AinDieselTemperature);
   DrawAin (AinDPF);

   // Diesel state
   DisplayGotoRC (19, 28);
   cputs ("D:");
   cprintdec (Diesel.DpfFilterState, FMT_UNSIGNED | 2);
   cprintdec (BackupDiesel.DpfFilterState, FMT_UNSIGNED | 2);
   
#endif // __TMCREMOTE__
}
