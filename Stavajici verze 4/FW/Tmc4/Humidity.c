//*****************************************************************************
//
//    Humidity.c - Mereni relativni vlhkosti
//    Version 1.0
//
//*****************************************************************************

#include "Tmc.h"
#include "Humidity.h"
#include "Ain.h"               // Analogove vstupy

THumidity __xdata__ Humidity;

#ifndef __TMCREMOTE__

//-----------------------------------------------------------------------------
// Inicializace
//-----------------------------------------------------------------------------

void HumidityInit() {
  // Inicializace
  Humidity.Value = 0;
}

//-----------------------------------------------------------------------------
// Vypocet
//-----------------------------------------------------------------------------

#ifdef USE_HUMIDITY

#define HUMIDITY_TABLE_COUNT    2
static byte code AinTable[HUMIDITY_TABLE_COUNT] = {0, 0xFF};         // Napeti Ain 0 a 10V
static byte code HumidityTable[HUMIDITY_TABLE_COUNT] = {0, 100};     // Relativni vlhkost v 0-100%

#endif // USE_HUMIDITY

void HumidityCalc() {
  // Vypoctu CO2
#ifdef USE_HUMIDITY
  Humidity.Value = AinCalcValue(AinHumidity, AinTable, HumidityTable, HUMIDITY_TABLE_COUNT);
#else
  Humidity.Value = 0;
#endif // USE_HUMIDITY
}

#endif // __TMCREMOTE__
