//*****************************************************************************
//
//    TruckEn.c - Beh motoru vozidla
//    Version 1.0
//
//*****************************************************************************

#include "TruckEn.h"
#include "Din.h"               // Digitalni vstupy (chod motoru vozidla)

byte __xdata__ TruckEngineOn;    // YES/NO nastartovany motor vozidla

#ifndef __TMCREMOTE__

//-----------------------------------------------------------------------------
// Nacteni behu motoru vozidla
//-----------------------------------------------------------------------------

void TruckEngineExecute() {
  // Nacteni behu motoru vozidla
  TruckEngineOn = (TYesNo)(DinTruckEngineRun == DIN_TRUCK_ENGINE_RUNNING);
}

#endif // __TMCREMOTE__
