//*****************************************************************************
//
//    Disinf.c - Dezinfekce skrine a kol
//    Version 1.0, (c) Vymos
//
//*****************************************************************************

#include "Disinf.h"
#include "Dout.h"              // Digitalni vystupy
#include "Ain.h"               // Analogove vstupy
#include "Menu.h"              // Kvuli DisplayMode
#include "FAir.h"              // Regulace cerstrveho vzduchu
#include "System.h"     // SysDelay()


TDisinfection __xdata__ Disinfection;

#ifndef __TMCREMOTE__

// Casovac
static byte __xdata__ Counter = 0;
#define MAX_POWER_TIME  15              // Maximalni doba startovani loga
#define DISLAY_TIME     3               // Doba zobrazeni hlasky na displeji

// Meze pro zjisteni pritomnosti Loga (zda na aute dezinfekce je nebo ne)
// Pokud je na Ain < 2V, neni Logo osazene. Pokud je osazene, nastavi Logo vyssi napeti.
#define AIN_NOT_USED      0x33     // 2V
#define AIN_LOGO_READY    0x99     // 6V

//-----------------------------------------------------------------------------
// Inicializace
//-----------------------------------------------------------------------------

void DisinfectionInit() {
  Disinfection.IsInstalled = NO;                // Zatim nevim, dam ze neni
  Disinfection.Mode        = DISINFECTION_OFF;
}

//-----------------------------------------------------------------------------
// Start dezinfekce skrine
//-----------------------------------------------------------------------------

void DisinfectionBodyStart() {
  if (!Disinfection.IsInstalled) {
    return;
  }

  Disinfection.Mode    = DISINFECTION_BODY;
  Disinfection.Status  = DISINFECTION_STATUS_INIT;

  // Klapky musi byt v automatu, aby se samy zavrely. Pokud by je mel v rucnim rezimu, dezinfekce by nefungovala.
  FAirNewMode(FAIR_AUTO);
  FAirNewFloorFlapMode(FLAP_AUTO);
}

//-----------------------------------------------------------------------------
// Start dezinfekce kol
//-----------------------------------------------------------------------------

void DisinfectionWheelsStart() {
  if (!Disinfection.IsInstalled) {
    return;
  }

  Disinfection.Mode    = DISINFECTION_WHEELS;
  Disinfection.Status  = DISINFECTION_STATUS_INIT;
}

//-----------------------------------------------------------------------------
// Automat dezinfekce
//-----------------------------------------------------------------------------

#define SwitchPower(On) DoutSetDisinfectionPower(On)
#define LogoReady() (AinLogoReady > AIN_LOGO_READY)
#define StartDesinfectionBody(On) DoutSetDisinfectionBody(On)
#define StartDesinfectionWheels(On) DoutSetDisinfectionWheels(On)

void DisinfectionExecute() {
  // Krok automatu, volat kazdou sekundu

  // Zjistim, zda je dezinfekce nainstalovana
  Disinfection.IsInstalled = (AinLogoReady > AIN_NOT_USED);

  // Pokud neni dezinfekce nainstalovana nebo je, ale je vypnuta, nedelam nic
  if (!Disinfection.IsInstalled || Disinfection.Mode == DISINFECTION_OFF) {
    // Pro jistotu vse povypinam
    SwitchPower(NO);
    StartDesinfectionBody(NO);
    StartDesinfectionWheels(NO);    
    return;
  }

  switch (Disinfection.Status) {
    case DISINFECTION_STATUS_INIT: {
      // Zapnu logo
      SwitchPower(YES);
      Disinfection.Status = DISINFECTION_STATUS_WAIT_FOR_LOGO;
      Counter = 0;          // Pocitam od nuly
      DisplayMode = DISPLAY_DISINFECTION_STARTED;
      break;
    }

    case DISINFECTION_STATUS_WAIT_FOR_LOGO: {
      // Cekam, az LOGO nastartuje, max. nejakou dobu
      Counter++;
      if (Disinfection.Mode == DISINFECTION_WHEELS && Counter > DISLAY_TIME) {
        // Start dezinfekce kol je uz zobrazen dost dlouho, muzu zase prejit na standardni zobrazeni
        DisplayMode = DISPLAY_TEMP;
        // Pokracuju dal na test MAX_POWER_TIME
      }
      if (Counter > MAX_POWER_TIME) {
        // Cekam uz dlouho, LOGO nenastartvalo
        Counter = MAX_POWER_TIME;       // Aby timer nepretekl
        SwitchPower(NO);                // Zase vypnu
        Disinfection.Status = DISINFECTION_STATUS_ERROR;        // Porucha bude trcet, dokud ji nezobrazim uzivateli a rucne neshodim
        Counter = 0;          // Pocitam od nuly
        DisplayMode = DISPLAY_DISINFECTION_ERROR;
        break;
      }

      if (!LogoReady()) {
        // Jeste cekam
        break;
      }

      // Logo je pripravene, zahajim dezinfekci podle typu, ktery chce
      if (Disinfection.Mode == DISINFECTION_BODY) {
        StartDesinfectionBody(YES);
      } else {
        StartDesinfectionWheels(YES);
      }
      Disinfection.Status = DISINFECTION_STATUS_RUNNING;
      break;
    }

    case DISINFECTION_STATUS_ERROR: {
      // Odpocitavam delku zobrazeni poruchy
      Counter++;
      if (Counter > DISLAY_TIME) {
        // Chybova hlaska je uz zobrazena dost dlouho, muzu ji zase smazat
        Disinfection.Mode = DISINFECTION_OFF;
        DisplayMode       = DISPLAY_TEMP;
        break;
      }
      break;
    }

    case DISINFECTION_STATUS_RUNNING: {
      // Cekam, az se cyklus dokonci
      if (LogoReady()) {
        // Cyklus jeste neskoncil
        break;
      }

      // Cyklus je dokoncen
      SwitchPower(NO);          // Vypnu logo radeji hned, i kdyz se vypne v dalsim pruchodu
      StartDesinfectionBody(NO);
      StartDesinfectionWheels(NO);
      Disinfection.Status  = DISINFECTION_STATUS_FINISHED;
      Disinfection.Mode    = DISINFECTION_OFF;
      DisplayMode          = DISPLAY_TEMP;
      break;
    }
  }
}


#endif // __TMCREMOTE__
