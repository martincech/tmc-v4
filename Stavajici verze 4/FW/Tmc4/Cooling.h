//*****************************************************************************
//
//    Cooling.c - Ovladani chlazeni
//    Version 1.0
//
//*****************************************************************************


#ifndef __Cooling_H__
   #define __Cooling_H__

#include "Hardware.h"     // pro podminenou kompilaci
#include "DutyCyc.h"           // Vypocet stridy

// Struktura chlazeni
typedef struct {
  byte On;                              // YES/NO prave sepnute chlazeni
  byte Failure;                         // YES/NO porucha chlazeni
  TDutyCycle DutyCycle;                 // Strida chodu kompresoru
  byte SuctionPressure;                 // Tlak sani kompresoru 0-25.5 bar (okamzita hodnota)
  byte SuctionPressureAverage;          // Tlak sani kompresoru 0-25.5 bar (prumer za periodu ukladani)
  byte DischargePressure;               // Tlak vytlaku kompresoru 0-25.5 bar (okamzita hodnota)
  byte DischargePressureAverage;        // Tlak vytlaku kompresoru 0-25.5 bar (prumer za periodu ukladani)
} TCooling;
extern TCooling __xdata__ Cooling;

void CoolingInit();
  // Inicializace

void CoolingStop();
  // Vypne chlazeni

void CoolingCalcPressure();
  // Vypocte aktualni hodnoty tlaku sani a vytlaku

void CoolingAutoExecute(int Average);
  // Krok automatickeho regulatoru chlazeni, volat 1x za sekundu. <Average> je prumerna teplota podle lozeni ve vnitrnim formatu

void CoolingResetDutyCycle();
  // Snulovani stridy

void CoolingResetPressure();
  // Snulovani prumerovani tlaku

#endif
