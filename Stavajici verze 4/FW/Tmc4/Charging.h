//*****************************************************************************
//
//    Charging.c - Dobijeni akumulatoru
//    Version 1.0
//
//*****************************************************************************


#ifndef __Charging_H__
   #define __Charging_H__

#include "Hardware.h"     // pro podminenou kompilaci

// Struktura dobijeni
typedef struct {
  byte  On;         // YES/NO dobiji se
  byte  Failure;    // YES/NO porucha dobijeni
} TCharging;
extern TCharging __xdata__ Charging;


void ChargingExecute();
  // Nacteni dobijeni

#endif
