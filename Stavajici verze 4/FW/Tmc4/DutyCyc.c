//*****************************************************************************
//
//    DutyCyc.c - Mereni stridy
//    Version 1.0
//
//*****************************************************************************

#include "Tmc.h"
#include "DutyCyc.h"

#ifndef __TMCREMOTE__

//-----------------------------------------------------------------------------
// Snulovani pocitadel
//-----------------------------------------------------------------------------

void DutyCycleReset(TDutyCycle xdata *DutyCycle) {
  DutyCycle->OnCounter  = 0;
  DutyCycle->OffCounter = 0;
  DutyCycle->Percent    = 0;
}

//-----------------------------------------------------------------------------
// Krok, volat 1x za sekundu
//-----------------------------------------------------------------------------

void DutyCycleExecute(TDutyCycle xdata *DutyCycle, TYesNo IsStateOn) {
  // Prictu pocitadlo
  if (IsStateOn) {
    if (DutyCycle->OnCounter < 0xFFFF) {
      DutyCycle->OnCounter++;
    }
  } else {
    if (DutyCycle->OffCounter < 0xFFFF) {
      DutyCycle->OffCounter++;
    }
  }

  // Vypoctu aktualni hodnotu stridy v procentech, nemelo by dojit k deleni nulou
  DutyCycle->Percent = (dword)100 * (dword)DutyCycle->OnCounter / ((dword)DutyCycle->OnCounter + (dword)DutyCycle->OffCounter);
}

#endif // __TMCREMOTE__
