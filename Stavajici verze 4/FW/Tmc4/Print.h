//*****************************************************************************
//
//    Print.c     - TMC Printer services
//    Version 1.0, (c) P.Veit, Vymos
//
//*****************************************************************************

#ifndef __Print_H__
   #define __Print_H__

#include "Tmc.h"     // konstanty a datove typy

// Globalni parametry pro PrintProtocol :
extern dword __xdata__ PocatecniDatum, KoncoveDatum;
extern word  __xdata__ PocatecniCas, KoncovyCas;
extern byte  __xdata__ PocetZaznamuNaRadek;


void PrintInit( void);
// Inicializuje tiskarnu

void PrintStart( void);
// Inicializuje linku tiskarny pred startem tisku

void PrintStop( void);
// Uvolni linku tiskarny

void PrintSetup( void);
// Nastaveni modu tiskarny

TYesNo PrintPrepare( void);
// Prohleda FIFO, nastavi ukazatele, vraci NO je-li prazdne

void PrintProtocol( void);
// Vytiskne zaznamy za cele obdobi, ktere je v pameti

#endif
