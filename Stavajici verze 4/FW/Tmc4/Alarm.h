//*****************************************************************************
//
//    Alarm.c - Ovladani alarmu podle poruch
//    Version 1.0
//
//*****************************************************************************


#ifndef __Alarm_H__
   #define __Alarm_H__

#include "Hardware.h"     // pro podminenou kompilaci

// Stav ztiseni sireny pri alarmu: aby sirenu ridic neztlumil omylem, po prvnim ztlumeni se pocita 5 minut a pokud alarm stale trva,
// sirena se opet rozezni. Pokud ridic sirenu ztlumi i podruhe, zustane uz sirena vypnuta naporad.
typedef enum {
  SIREN_FIRST_BEEPING,                  // Alarm piska a zatim ho obsluha nevypnula
  SIREN_FIRST_QUIET,                    // Obsluha alarm ztisila, cekam 5 minut
  SIREN_SECOND_BEEPING,                 // Alarm se podruhe rozeznel
  SIREN_SECOND_QUIET                    // Alarm je ztlumen uz naporad
} TSirenState;

typedef struct {
  byte ShowAlarm;         // YES/NO zda se prave indikuje alarm
  byte SirenState;        // TSirenState
  byte Quiet;             // YES/NO potlaceny reproduktor pri alarmu
} TAlarm;
extern TAlarm __xdata__ Alarm;



void AlarmInit();
  // Inicializuje alarmu

void AlarmChange();
  // Zapne / vypne hlaseni alarmu

void AlarmClear();
  // Vynuluje alarm (volat periodicky na zacatku kazdeho mericiho cyklu)

void AlarmExecute();
  // Obsluha piskani alarmu, volat kazdou sekundu na konci mericiho cyklu

void AlarmSet();
  // Nastavi alarm pri nejake poruse

TYesNo AlarmCheckFaultCounterMaxTime(TYesNo isFault, byte __xdata__ *faultCounter, byte *faultIndicator, byte maxFaultTime);

TYesNo AlarmCheckFaultCounter(TYesNo isFault, byte __xdata__ *faultCounter, byte *faultIndicator);
// počítadlo poruchy
#endif
