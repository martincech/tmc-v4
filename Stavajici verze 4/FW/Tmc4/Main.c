#include "Hardware.h"
#include "System.h"     // "operacni system"
#include "Rd2/PcaTmc.h"     // PCA projektu TMC
#include "Ads7846/Ads7846.h"    // touch screen
#include "Menu.h"              // Zobrazeni
#include "Din.h"               // Digitalni vstupy
#include "Dout.h"              // Digitalni vystupy
#include "Ain.h"               // Analogove vstupy
#include "Aout.h"              // Analogove vystupy
#include "Temp.h"              // Teplomery
#include "Accu.h"              // Napeti a proud akumulatoru
#include "Log.h"               // Ukladani hodnot
#include "Time.h"              // Aktualni datum a cas
#include "Beep.h"              // Repro
#include "FAir.h"              // Regulace cerstrveho vzduchu
#include "Cfg.h"               // Konfigurace
#include "Servo.h"             // Kontrola stavu serv
#include "Alarm.h"             // Indikace poruch
#include "Diesel.h"            // Diesel motor
#include "Charging.h"          // Dobijeni
#include "ElMotor.h"           // Elektromotor
#include "TruckEn.h"           // Motor vozidla
#include "Fan.h"               // Ventilatory
#include "Control.h"           // Automaticke rizeni topeni/chlazeni
#include "Print.h"             // Tisk protokolu
#include "Ras.h"               // Terminalovy server
#include "Disinf.h"            // Dezinfekce


//#define DEBUG_MODE      1       // Debug mod pro hledani problemu s rele, pri kterem se automaticky prepina mezi automatem a ventilaci
#ifdef DEBUG_MODE
byte xdata DebugModeCounter = 0;
#endif // DEBUG_MODE

// odpocet necinnosti :
#define USER_EVENT_TIMEOUT 250    // uzivatel neobsluhuje dele nez ... s

// casy pro blikani :
#define BLINK_ON   25           // cas zapnuti roviny
#define BLINK_OFF  20           // cas vypnuti roviny

// start odpocitavani 1 s :
#define SetTimer1s()  counter1s = 1000 / TIMER0_PERIOD

// Lokalni promenne :
volatile byte counter1s = 0;   // odpocitavani 1 s
volatile bit timer1s = 0;      // priznak odpocitani 1 s
char counter_blink = BLINK_ON; // pocitadlo blikani znamenko urcuje smer citani
volatile bit timer_blink = 0;  // priznak blikani
volatile bit blink_on = 1;     // blikani - roznout, zhasnout

TConfig __xdata__ Config; // buffer konfigurace v externi RAM, Tmc.h

static byte BrightnessCounter = 0; // Pocitadlo v preruseni
extern byte Brightness;            // Nastaveny podsvit MIN_PODSVIT az MAX_PODSVIT

byte _Timeout = 0; // pocitani necinnosti - kvuli tisku protokolu jsem to musel vytahnout ven z fce SysYield()


// ---------------------------------------------------------------------------------------------
// Cekani na napeti po startu
// ---------------------------------------------------------------------------------------------

#define DELAY_LOWBAT          1000      // Cekani po obnove napajeni v milisekundach

static void WaitForVoltage(void) {
    // Po zapnuti jednotky ceka na jeho vztust napajeni, po obnoveni napajeni jeste nejakou dobu cekam
    while (1) {
        if (TmcAccuOk ()) {
            SysDelay(DELAY_LOWBAT);
            if (TmcAccuOk ()) {
                return; // Po dobu DELAY_LOWBAT je napajeni v poradku
            }
        }
        WatchDog ();
    }
}

// ---------------------------------------------------------------------------------------------
// Main
// ---------------------------------------------------------------------------------------------

#define TestujPinRychle(Pin)    \
while (EA) {                    \
  Pin = 0;                      \
  WatchDog();                   \
  Pin = 1;                      \
}

#define TestujPinPomalu(Pin)    \
while (EA) {                    \
  Pin = 0;                      \
  SysDelay(1);                  \
  Pin = 1;                      \
  SysDelay(1);                  \
}

void main() {
    // Systemova inicializace : ---------------------------------------------------
    TimingSetup ();       // Nastaveni X2 modu
    EnableXRAM ();        // Povoleni pristupu k vnitrni XRAM
    WDTPRG = WDTPRG_2090; // start Watch Dogu
    WatchDog ();

    EnableInts ();             // Povoleni preruseni
    Timer0Run (TIMER0_PERIOD); // spust casovac 0
    Timer1Run (TIMER1_PERIOD); // spust casovac 1

    DisplayPodsvit (YES);

    // Inicializace modulu : ------------------------------------------------------

    WaitForVoltage(); // 4.1.2007: Pocka, az vzroste napeti. Pokud to zde neni, pri upgrad na novejsi verzi se neprovede SetDetaults() v CfgLoad(),
    // protoze indikace korektniho napeti jeste neni spravne nahozena (pripadne tam dojde k oscilaci).
    CfgLoad(); // Musi byt pred inicializaci zobrazeni (zobrazuje se tam identifikacni cislo)
    MenuInit();
    TouchInit();
    DinInit();
    DoutInit();
    AinInit();
    AoutInit();
    TimeInit();
    TemperatureInit();
    AccuInit();
    FanInit();
    LogInit(); // V inicializaci EEPROM je while(ready) => muze zpusobovat reset po zapnuti pri vadne EEPROM
    PcaInit();
    FAirInit();
    ServoInit();
    AlarmInit();
    DieselInit();
    ElMotorInit();
    DisinfectionInit();
    ControlInit();
    PrintInit();
    RasInit();

    BeepStartup (); // Musi byt az po PcaInit()
    SysDelay(3000);

    SetTimer1s (); // zahajeni odpoctu 1 s

    // Logovani startu a login ridice
    Driver = 0;                     // Start systemu zaprotokoluju s ridicem cislo 0
    TimeRead();                     // Nacteni aktualniho data a casu - pro zaprotokolovani startu systemu a logovani
    LogWriteProtocol(PRT_START, 0); // zaprotokolujeme nabeh systemu
    if (Config.Login) {
        Driver = 1;            // Default ridic 1, kdyby dal behem loginu Cancel
        MenuLogin();           // Nalogovani ridice
        DiagnosticCounter = 0; // Aby bylo mozne prejit do diagnostickeho rezimu i pokud pouziva logovani
    }

    while (1) {
        switch (SysWaitEvent()) {
            case K_TOUCH:
                HomeScreenProcessTouch();
                break;

            case K_BLINK_ON:
                // Blikani 2. rovinou
                MenuBlink(YES);
                break;

            case K_BLINK_OFF:
                // Blikani 2. rovinou
                MenuBlink(NO);
                break;

            case K_REDRAW:
                HomeScreenRedraw();
                break;

            case K_TIMEOUT:
                break;

            default:
                break;
        } //switch
    }     //while
}         // main

//-----------------------------------------------------------------------------
// Prepnuti kontextu na "operacni system" viz System.h
//-----------------------------------------------------------------------------

// Kontrola touchpanelu, vola se casteji v SysYield()
#define CheckTouch()    \
  if (TouchGet()) {     \
    _Timeout = 0;       \
    return( K_TOUCH);   \
  }


byte SysYield()
// Prepnuti kontextu na operacni system. Vraci udalost
{
    WatchDog ();
    CheckTouch ();
    // Terminal
    RasExecute(); // Volat co nejcasteji, aby nebylo zpozdeni ve zpracovani
    // casovac periodicke cinnosti :
    if (timer1s) {
        // uplynula 1s
        timer1s = 0;   // zrus priznak
        SetTimer1s (); // novy cyklus
        _Timeout++;    // pocitame necinnost
        if (DiagnosticCounter < MAX_DIAGNOSTIC_COUNTER) {
            DiagnosticCounter++; // Volba diagnostiky je jeste platna
        }

        // Debug prepinani rezimu
#ifdef DEBUG_MODE
      if (++DebugModeCounter >= 10) {
         DebugModeCounter = 0;
         switch (Control.Mode) {
         case MODE_AUTO:
            ControlSetNewMode (MODE_VENTILATION);
            break;

         case MODE_VENTILATION:
            ControlSetNewMode (MODE_AUTO);
            break;
         }
         Redraw = REDRAW_ALL;
      }
#endif // DEBUG_MODE

        // Cinnost:
        AlarmClear();      // Nulovani poruch na zacatku kazdeho mericiho cyklu
        TimeRead();        // Nacteni aktualniho data a casu
        DinRead();         // Nacteni digitalnich vstupu
        AinRead();         // Nacteni analogovych vstupu
        TemperatureRead(); // Nacteni teplot
#ifndef NO_DIESEL
        DieselExecute(); // Kontrola behu, motohodin a teploty dieselu
#endif
        TruckEngineExecute();                               // Nacteni behu motoru vozidla
        AccuCheck(Diesel.On || BackupDiesel.On || TruckEngineOn); // Vypocet napeti a proudu akumulatoru
        ChargingExecute();                                  // Kontrola dobijeni
        ElMotorExecute();                                   // Kontrola behu elektromotoru, volat az po nacteni behu dieselu a dobijeni
        FanCheck();                                         // Kontrola ventilatoru
        DisinfectionExecute();                              // Dezinfekce
        ControlExecute();                                   // Automaticke rizeni topeni/chlazeni
        FAirExecute();                                      // Vypocet polohy klapek - az po ControlExecute(), abych mohl vyuzivat promennou Control.AverageTemperature
        ServoCheck();                                       // Kontrola poruch serv
        LogExecute();                                       // Zapis do FIFO

        if (Control.EmergencyMode) {
            AlarmClear(); // V nouzovem rezimu nemuze byt alarm (ale muze se volanim fci vyhlasit, takze ho tady snuluju)
        }
        AlarmExecute(); // Obslouzim piskani alarmu na konci kazdeho mericiho cyklu
        CheckTouch ();

        Redraw |= REDRAW_PERIODIC; // Prekreslim nove nactene hodnoty
        return (K_REDRAW);
    }
    // casovac blikani :
    if (timer_blink) {
        // uplynul cas blikani
        timer_blink = 0; // zrus priznak
        if (blink_on) {
            return (K_BLINK_ON);
        }
        return (K_BLINK_OFF);
    }
    // testuj necinnost :
    if (_Timeout > USER_EVENT_TIMEOUT) {
        _Timeout = 0; // zahajime dalsi cekani
        return (K_TIMEOUT);
    }
    CheckTouch ();
    return (K_IDLE); // zadna udalost
}                    // SysYield

//-----------------------------------------------------------------------------
// Cekani na udalost viz System.h
//-----------------------------------------------------------------------------

byte SysWaitEvent(void)
// Cekani na udalost
{
    byte key;

    while (1) {
        key = SysYield();
        if (key != K_IDLE) {
            return (key); // neprazdna udalost
        }
    }
} // SysWaitEvent

//-----------------------------------------------------------------------------
// Nulovani timeoutu viz System.h
//-----------------------------------------------------------------------------

void SysFlushTimeout(void)
// Nuluje timeout klavesnice
{
    _Timeout = 0;
    // nastavit kontext v modulu Kbd, aby nezustal vysilat treba K_REPEAT
} // SysFlush_Timeout

//-----------------------------------------------------------------------------
// Nastaveni timeoutu viz System.h
//-----------------------------------------------------------------------------

void SysSetTimeout(void)
// Nastavi timeout klavesnice
{
    _Timeout = USER_EVENT_TIMEOUT + 1;
} // SysSetTimeout

//-----------------------------------------------------------------------------
// Prerusovaci rutina casovace 0
//-----------------------------------------------------------------------------

static void Timer0Handler(void) interrupt
INT_TIMER0
// Prerusovaci rutina casovace 0
{
   SetTimer0 (TIMER0_PERIOD);                // s touto periodou
   // vykonne funkce :
   CheckTrigger (counter1s, timer1s = 1);    // casovac 1 s
   PcaTrigger ();                             // handler asynchronniho pipani
   TouchTrigger ();                           // handler touch panelu
   // casovac blikani :
   counter_blink--;
   if (counter_blink == 0) {
      // docitali jsme, nastav na novou hodnotu
      timer_blink = 1;                       // nastav priznak
      if (blink_on) {
         counter_blink = -256 + BLINK_OFF;   // bylo roznuto,  perioda pro zhasni
      }
      else {
         counter_blink = BLINK_ON;           // bylo zhasnuto, perioda pro rozsvit
      }
      blink_on = !blink_on;                  // novy smer citani
   }
} // Timer0Handler

//-----------------------------------------------------------------------------
// Prerusovaci rutina casovace 1
//-----------------------------------------------------------------------------

static void Timer1Handler(void) interrupt
INT_TIMER1
// Prerusovaci rutina casovace 0
{
   SetTimer1 (TIMER1_PERIOD);                // s touto periodou
   // vykonne funkce :
   // Podsvit
   BrightnessCounter++;
   if (BrightnessCounter <= Brightness) {
      DisplayPodsvit (YES);
   }
   else {
      DisplayPodsvit (NO);
   }
   if (BrightnessCounter > BRIGHTNESS_MAX) {
      BrightnessCounter = 0;
   }
} // Timer1Handler

//-----------------------------------------------------------------------------
// Zpozdeni
//-----------------------------------------------------------------------------

void SysDelay(unsigned int n) {
    // Uprava pro krystal 18.432MHz
    unsigned char m; // m = R5, n = R6+R7
    // T = 1.085 mikrosec.
    while (n > 0) {
        // DELAY: SETB C/MOV A,R7/SUBB A,#0/MOV A,R6/SUBB A,#0/JC KONEC *7T*
        WDTRST = 0x1E;
        WDTRST = 0xE1;
        m = 255;
        do --m;
        while (m != 0); // MOV R5,#255/DJNZ R5,$ *511T*
        m = 255;
        do --m;
        while (m != 0); // MOV R5,#255/DJNZ R5,$ *511T*
        m = 253;
        do --m;
        while (m != 0); // MOV R5,#193/DJNZ R5,$ *387T*
        // Pro X2 mod musim 2x tolik!!
        m = 255;
        do --m;
        while (m != 0); // MOV R5,#255/DJNZ R5,$ *511T*
        m = 255;
        do --m;
        while (m != 0); // MOV R5,#255/DJNZ R5,$ *511T*
        m = 253;
        do --m;
        while (m != 0); // MOV R5,#193/DJNZ R5,$ *387T*
        --n;            // MOV A,R7/DEC R7/JNZ ODSKOK/DEC R6/ODSKOK: SJMP DELAY *5T(4T)*
    }                   // SJMP DELAY *2T*
}                       // SysDelay
