//*****************************************************************************
//
//    ExtSiren.c - Ovladani externi sireny
//    Version 1.0
//
//*****************************************************************************


#ifndef __ExtSiren_H__
   #define __ExtSiren_H__

#include "Hardware.h"     // pro podminenou kompilaci


void ExtSirenInit();
  // Inicializuje externi sirenu

#define ExtSirenOff() ExtSirenInit()
  // Vypnuti externi sireny

void ExtSirenExecute();
  // Obsluha externi sireny, volat kazdou sekundu

#endif
