//*****************************************************************************
//
//    Humidity.c - Mereni relativni vlhkosti
//    Version 1.0
//
//*****************************************************************************


#ifndef __Humidity_H__
   #define __Humidity_H__

#include "Hardware.h"     // pro podminenou kompilaci

// Struktura
typedef struct {
  byte Value;                           // Relativni vlhkost v %
} THumidity;
extern THumidity __xdata__ Humidity;

void HumidityInit();
  // Inicializace

void HumidityCalc();
  // Vypocte vlhkost

#endif
