//*****************************************************************************
//
//    ElMotor.c - Beh elektromotoru
//    Version 1.0
//
//*****************************************************************************

#include "ElMotor.h"
#include "Diesel.h"            // Diesel motor
#include "Charging.h"          // Dobijeni
#include "Tmc.h"
#include "Fifo/FArray.h"
#include <string.h>

#ifndef __TMCREMOTE__

// Beh elektromotoru - delam to stejne jako poruchy
TDiesel __xdata__ BackupDiesel;

void ElMotorInit()
{
    DieselStructureInit(&BackupDiesel);    
}
//-----------------------------------------------------------------------------
// Nacteni behu elektromotoru
//-----------------------------------------------------------------------------

void ElMotorExecute() {
  // Nacteni behu elektromotoru, volat az po nacteni behu dieselu a dobijeni
  if (Charging.On && !Diesel.On) {
      // Porucha uz trva dlouho
      BackupDiesel.On = YES;
  } else {
      BackupDiesel.On = NO;
  }//else
  DpfFilterCheck(&BackupDiesel);
}

#endif // __TMCREMOTE__
