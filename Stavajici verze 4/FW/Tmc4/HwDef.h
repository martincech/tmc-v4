//*****************************************************************************
//
//    HwDef.h  - TMC jednotlive HW konfigurace aut
//    Version 1.0  (c) VymOs
//
//*****************************************************************************

#ifndef __HwDef_H__
   #define __HwDef_H__


//-----------------------------------------------------------------------------
// Konfigurace jednotky v aute
//-----------------------------------------------------------------------------
#ifdef HW
   #define VERSION_HW   HW
#else
//#define VERSION_HW              1           // Standardni auto s cidlem na tlak filtru
//#define VERSION_HW              2           // Auto se vsemi cidly CO2, RH, tlak filtru a ventilatoru
//#define VERSION_HW              3           // Dodavka s bufikem a chlazenim
#define VERSION_HW              4           // Auto s tlakem filtru, cidlem CO2, RH (Thajsko)
//#define VERSION_HW              5           // Auto s cidlem CO2 a tlakem filtru (naves Drobex, novy standard pro auta s rizenim CO2)
//#define VERSION_HW              6           // Dodavka bez podlahove klapky, s bufikem a chlazenim, s cidlem CO2
//#define VERSION_HW              7           // Jako 6, ale na 24V (Thajsko)
//#define VERSION_HW              8           // Specialni verze pro test EMC, jako 5, ale bez cidel
//#define VERSION_HW              9           // Jako 5, ale jen se dvema cidly teploty (male auto)
//#define VERSION_HW              10          // Dodavka s podlahovou klapkou, s bufikem a chlazenim, s cidlem CO2 (Ultralight)
//#define VERSION_HW              11          // Jako 10, ale na 24V (Whitaker)
#endif
//-----------------------------------------------------------------------------
// Definice jednotlivych HW verzi - nemenit, pouze pridavat nove:
//-----------------------------------------------------------------------------

#if (VERSION_HW == 1)

  // Napajeni 12 nebo 24 V
  #define ACCU_MIN_VOLTAGE        23          //  Minimalni napeti pro 24V napajeni
  #define ACCU_MIN_VOLTAGE_DIESEL 25          //  Minimalni napeti pro 24V napajeni pri behu dieselu nebo motoru vozidla

  // Pouziti podlahovych serv
  #define USE_FLOOR_FLAPS         1           // Pouzivaji se podlahove klapky, zobrazuju je a kontroluju poruchu
  #define USE_FLOOR_FLAPS_CONTROL 1           // Podlahove klapky lze ovladat oddelene od naporovych, tj. na klapku lze kliknout a nastavit
//  #define FLOOR_FLAP_ULTRALIGHT   1           // Podlahova klapka Ultralightu, otevira se jen cca do pulky

  // Pouziti bufiku misto teplovodniho topeni se servem. Bufik se reguluje pouze zapinanim a vypinanim (stejne jako chlazeni).
//  #define USE_ON_OFF_HEATING      1           // Pouzivaji se bufiky

  // Pouziti chlazeni
  #define USE_COOLING             1           // Pouziva se chlazeni


  // Pouziti 2 nebo 3 teplotnich cidel ve skrini
//  #define USE_TWO_SENSORS         1           // Pouzivaji se jen 2 teplotni cidla

  // Pouziti ruznych cidel
  #define USE_FILTER_PRESSURE     1           // Pouziva se mereni tlaku na vzduchovych filtrech
//  #define USE_CO2                 1           // Pouziva se mereni CO2
//  #define USE_HUMIDITY            1           // Pouziva se mereni vlhkosti

//-----------------------------------------------------------------------------
#elif (VERSION_HW == 2)

  // Napajeni 12 nebo 24 V
  #define ACCU_MIN_VOLTAGE        23          //  Minimalni napeti pro 24V napajeni
  #define ACCU_MIN_VOLTAGE_DIESEL 25          //  Minimalni napeti pro 24V napajeni pri behu dieselu nebo motoru vozidla

  // Pouziti podlahovych serv
  #define USE_FLOOR_FLAPS         1           // Pouzivaji se podlahove klapky, zobrazuju je a kontroluju poruchu
  #define USE_FLOOR_FLAPS_CONTROL 1           // Podlahove klapky lze ovladat oddelene od naporovych, tj. na klapku lze kliknout a nastavit
//  #define FLOOR_FLAP_ULTRALIGHT   1           // Podlahova klapka Ultralightu, otevira se jen cca do pulky

  // Pouziti bufiku misto teplovodniho topeni se servem. Bufik se reguluje pouze zapinanim a vypinanim (stejne jako chlazeni).
//  #define USE_ON_OFF_HEATING      1           // Pouzivaji se bufiky

  // Pouziti chlazeni
  #define USE_COOLING             1           // Pouziva se chlazeni

  // Pouziti 2 nebo 3 teplotnich cidel ve skrini
//  #define USE_TWO_SENSORS         1           // Pouzivaji se jen 2 teplotni cidla

  // Pouziti ruznych cidel
  #define USE_FILTER_PRESSURE     1           // Pouziva se mereni tlaku na vzduchovych filtrech
  #define USE_CO2                 1           // Pouziva se mereni CO2
  #define USE_HUMIDITY            1           // Pouziva se mereni vlhkosti

//-----------------------------------------------------------------------------
#elif (VERSION_HW == 3)

  // Napajeni 12 nebo 24 V
  #define ACCU_MIN_VOLTAGE        11          // Minimalni napeti pro 12V napajeni
  #define ACCU_MIN_VOLTAGE_DIESEL 12          // Minimalni napeti pro 12V napajeni pri behu dieselu nebo motoru vozidla

  // Pouziti podlahovych serv
//  #define USE_FLOOR_FLAPS         1           // Pouzivaji se podlahove klapky, zobrazuju je a kontroluju poruchu
//  #define USE_FLOOR_FLAPS_CONTROL 1           // Podlahove klapky lze ovladat oddelene od naporovych, tj. na klapku lze kliknout a nastavit
//  #define FLOOR_FLAP_ULTRALIGHT   1           // Podlahova klapka Ultralightu, otevira se jen cca do pulky

  // Pouziti bufiku misto teplovodniho topeni se servem. Bufik se reguluje pouze zapinanim a vypinanim (stejne jako chlazeni).
  #define USE_ON_OFF_HEATING      1           // Pouzivaji se bufiky

  // Pouziti chlazeni
  #define USE_COOLING             1           // Pouziva se chlazeni

  // Pouziti 2 nebo 3 teplotnich cidel ve skrini
  #define USE_TWO_SENSORS         1           // Pouzivaji se jen 2 teplotni cidla

  // Pouziti ruznych cidel
  #define USE_FILTER_PRESSURE     1           // Pouziva se mereni tlaku na vzduchovych filtrech
//  #define USE_CO2                 1           // Pouziva se mereni CO2
//  #define USE_HUMIDITY            1           // Pouziva se mereni vlhkosti
  #define NO_DIESEL               1           // nepouziva se diesel, pouze motor auta

//-----------------------------------------------------------------------------
#elif (VERSION_HW == 4)

  // Napajeni 12 nebo 24 V
  #define ACCU_MIN_VOLTAGE        23          //  Minimalni napeti pro 24V napajeni
  #define ACCU_MIN_VOLTAGE_DIESEL 25          //  Minimalni napeti pro 24V napajeni pri behu dieselu nebo motoru vozidla

  // Pouziti podlahovych serv
  #define USE_FLOOR_FLAPS         1           // Pouzivaji se podlahove klapky, zobrazuju je a kontroluju poruchu
  #define USE_FLOOR_FLAPS_CONTROL 1           // Podlahove klapky lze ovladat oddelene od naporovych, tj. na klapku lze kliknout a nastavit
//  #define FLOOR_FLAP_ULTRALIGHT   1           // Podlahova klapka Ultralightu, otevira se jen cca do pulky

  // Pouziti bufiku misto teplovodniho topeni se servem. Bufik se reguluje pouze zapinanim a vypinanim (stejne jako chlazeni).
//  #define USE_ON_OFF_HEATING      1           // Pouzivaji se bufiky

  // Pouziti chlazeni
  #define USE_COOLING             1           // Pouziva se chlazeni

  // Pouziti 2 nebo 3 teplotnich cidel ve skrini
//  #define USE_TWO_SENSORS         1           // Pouzivaji se jen 2 teplotni cidla

  // Pouziti ruznych cidel
  #define USE_FILTER_PRESSURE     1           // Pouziva se mereni tlaku na vzduchovych filtrech
  #define USE_CO2                 1           // Pouziva se mereni CO2
  #define USE_HUMIDITY            1           // Pouziva se mereni vlhkosti

//-----------------------------------------------------------------------------
#elif (VERSION_HW == 5)

  // Napajeni 12 nebo 24 V
  #define ACCU_MIN_VOLTAGE        23          //  Minimalni napeti pro 24V napajeni
  #define ACCU_MIN_VOLTAGE_DIESEL 25          //  Minimalni napeti pro 24V napajeni pri behu dieselu nebo motoru vozidla

  // Pouziti podlahovych serv
  #define USE_FLOOR_FLAPS         1           // Pouzivaji se podlahove klapky, zobrazuju je a kontroluju poruchu
  #define USE_FLOOR_FLAPS_CONTROL 1           // Podlahove klapky lze ovladat oddelene od naporovych, tj. na klapku lze kliknout a nastavit
//  #define FLOOR_FLAP_ULTRALIGHT   1           // Podlahova klapka Ultralightu, otevira se jen cca do pulky

  // Pouziti bufiku misto teplovodniho topeni se servem. Bufik se reguluje pouze zapinanim a vypinanim (stejne jako chlazeni).
//  #define USE_ON_OFF_HEATING      1           // Pouzivaji se bufiky

  // Pouziti chlazeni
  #define USE_COOLING             1           // Pouziva se chlazeni

  // Pouziti 2 nebo 3 teplotnich cidel ve skrini
//  #define USE_TWO_SENSORS         1           // Pouzivaji se jen 2 teplotni cidla

  // Pouziti ruznych cidel
  #define USE_FILTER_PRESSURE     1           // Pouziva se mereni tlaku na vzduchovych filtrech
  #define USE_CO2                 1           // Pouziva se mereni CO2
//  #define USE_HUMIDITY            1           // Pouziva se mereni vlhkosti

//-----------------------------------------------------------------------------
#elif (VERSION_HW == 6)

  // Napajeni 12 nebo 24 V
  #define ACCU_MIN_VOLTAGE        11          // Minimalni napeti pro 12V napajeni
  #define ACCU_MIN_VOLTAGE_DIESEL 12          // Minimalni napeti pro 12V napajeni pri behu dieselu nebo motoru vozidla

  // Pouziti podlahovych serv
//  #define USE_FLOOR_FLAPS         1           // Pouzivaji se podlahove klapky, zobrazuju je a kontroluju poruchu
//  #define USE_FLOOR_FLAPS_CONTROL 1           // Podlahove klapky lze ovladat oddelene od naporovych, tj. na klapku lze kliknout a nastavit
//  #define FLOOR_FLAP_ULTRALIGHT   1           // Podlahova klapka Ultralightu, otevira se jen cca do pulky

  // Pouziti bufiku misto teplovodniho topeni se servem. Bufik se reguluje pouze zapinanim a vypinanim (stejne jako chlazeni).
  #define USE_ON_OFF_HEATING      1           // Pouzivaji se bufiky

  // Pouziti chlazeni
  #define USE_COOLING             1           // Pouziva se chlazeni

  // Pouziti 2 nebo 3 teplotnich cidel ve skrini
  #define USE_TWO_SENSORS         1           // Pouzivaji se jen 2 teplotni cidla

  // Pouziti ruznych cidel
  #define USE_FILTER_PRESSURE     1           // Pouziva se mereni tlaku na vzduchovych filtrech
  #define USE_CO2                 1           // Pouziva se mereni CO2
//  #define USE_HUMIDITY            1           // Pouziva se mereni vlhkosti
  #define NO_DIESEL               1           // nepouziva se diesel, pouze motor auta

//-----------------------------------------------------------------------------
#elif (VERSION_HW == 7)

  // Napajeni 12 nebo 24 V
  #define ACCU_MIN_VOLTAGE        23          //  Minimalni napeti pro 24V napajeni
  #define ACCU_MIN_VOLTAGE_DIESEL 25          //  Minimalni napeti pro 24V napajeni pri behu dieselu nebo motoru vozidla

  // Pouziti podlahovych serv
//  #define USE_FLOOR_FLAPS         1           // Pouzivaji se podlahove klapky, zobrazuju je a kontroluju poruchu
//  #define USE_FLOOR_FLAPS_CONTROL 1           // Podlahove klapky lze ovladat oddelene od naporovych, tj. na klapku lze kliknout a nastavit
//  #define FLOOR_FLAP_ULTRALIGHT   1           // Podlahova klapka Ultralightu, otevira se jen cca do pulky

  // Pouziti bufiku misto teplovodniho topeni se servem. Bufik se reguluje pouze zapinanim a vypinanim (stejne jako chlazeni).
  #define USE_ON_OFF_HEATING      1           // Pouzivaji se bufiky

  // Pouziti chlazeni
  #define USE_COOLING             1           // Pouziva se chlazeni

  // Pouziti 2 nebo 3 teplotnich cidel ve skrini
  #define USE_TWO_SENSORS         1           // Pouzivaji se jen 2 teplotni cidla

  // Pouziti ruznych cidel
  #define USE_FILTER_PRESSURE     1           // Pouziva se mereni tlaku na vzduchovych filtrech
  #define USE_CO2                 1           // Pouziva se mereni CO2
//  #define USE_HUMIDITY            1           // Pouziva se mereni vlhkosti
  #define NO_DIESEL               1           // nepouziva se diesel, pouze motor auta

//-----------------------------------------------------------------------------
#elif (VERSION_HW == 8)

  // Napajeni 12 nebo 24 V
  #define ACCU_MIN_VOLTAGE        23          //  Minimalni napeti pro 24V napajeni
  #define ACCU_MIN_VOLTAGE_DIESEL 25          //  Minimalni napeti pro 24V napajeni pri behu dieselu nebo motoru vozidla

  // Pouziti podlahovych serv
  #define USE_FLOOR_FLAPS         1           // Pouzivaji se podlahove klapky, zobrazuju je a kontroluju poruchu
  #define USE_FLOOR_FLAPS_CONTROL 1           // Podlahove klapky lze ovladat oddelene od naporovych, tj. na klapku lze kliknout a nastavit
//  #define FLOOR_FLAP_ULTRALIGHT   1           // Podlahova klapka Ultralightu, otevira se jen cca do pulky

  // Pouziti bufiku misto teplovodniho topeni se servem. Bufik se reguluje pouze zapinanim a vypinanim (stejne jako chlazeni).
//  #define USE_ON_OFF_HEATING      1           // Pouzivaji se bufiky

  // Pouziti chlazeni
  #define USE_COOLING             1           // Pouziva se chlazeni

  // Pouziti 2 nebo 3 teplotnich cidel ve skrini
//  #define USE_TWO_SENSORS         1           // Pouzivaji se jen 2 teplotni cidla

  // Pouziti ruznych cidel
//  #define USE_FILTER_PRESSURE     1           // Pouziva se mereni tlaku na vzduchovych filtrech
//  #define USE_CO2                 1           // Pouziva se mereni CO2
//  #define USE_HUMIDITY            1           // Pouziva se mereni vlhkosti

//-----------------------------------------------------------------------------
#elif (VERSION_HW == 9)

  // Napajeni 12 nebo 24 V
  #define ACCU_MIN_VOLTAGE        23          //  Minimalni napeti pro 24V napajeni
  #define ACCU_MIN_VOLTAGE_DIESEL 25          //  Minimalni napeti pro 24V napajeni pri behu dieselu nebo motoru vozidla

  // Pouziti podlahovych serv
  #define USE_FLOOR_FLAPS         1           // Pouzivaji se podlahove klapky, zobrazuju je a kontroluju poruchu
  #define USE_FLOOR_FLAPS_CONTROL 1           // Podlahove klapky lze ovladat oddelene od naporovych, tj. na klapku lze kliknout a nastavit
//  #define FLOOR_FLAP_ULTRALIGHT   1           // Podlahova klapka Ultralightu, otevira se jen cca do pulky

  // Pouziti bufiku misto teplovodniho topeni se servem. Bufik se reguluje pouze zapinanim a vypinanim (stejne jako chlazeni).
//  #define USE_ON_OFF_HEATING      1           // Pouzivaji se bufiky

  // Pouziti chlazeni
  #define USE_COOLING             1           // Pouziva se chlazeni

  // Pouziti 2 nebo 3 teplotnich cidel ve skrini
  #define USE_TWO_SENSORS         1           // Pouzivaji se jen 2 teplotni cidla

  // Pouziti ruznych cidel
  #define USE_FILTER_PRESSURE     1           // Pouziva se mereni tlaku na vzduchovych filtrech
  #define USE_CO2                 1           // Pouziva se mereni CO2
//  #define USE_HUMIDITY            1           // Pouziva se mereni vlhkosti

//-----------------------------------------------------------------------------
#elif (VERSION_HW == 10)

  // Napajeni 12 nebo 24 V
  #define ACCU_MIN_VOLTAGE        11          // Minimalni napeti pro 12V napajeni
  #define ACCU_MIN_VOLTAGE_DIESEL 12          // Minimalni napeti pro 12V napajeni pri behu dieselu nebo motoru vozidla

  // Pouziti podlahovych serv
  #define USE_FLOOR_FLAPS         1           // Pouzivaji se podlahove klapky, zobrazuju je a kontroluju poruchu
//  #define USE_FLOOR_FLAPS_CONTROL 1           // Podlahove klapky lze ovladat oddelene od naporovych, tj. na klapku lze kliknout a nastavit
  #define FLOOR_FLAP_ULTRALIGHT   1           // Podlahova klapka Ultralightu, otevira se jen cca do pulky

  // Pouziti bufiku misto teplovodniho topeni se servem. Bufik se reguluje pouze zapinanim a vypinanim (stejne jako chlazeni).
  #define USE_ON_OFF_HEATING      1           // Pouzivaji se bufiky

  // Pouziti chlazeni
  #define USE_COOLING             1           // Pouziva se chlazeni

  // Pouziti 2 nebo 3 teplotnich cidel ve skrini
  #define USE_TWO_SENSORS         1           // Pouzivaji se jen 2 teplotni cidla

  // Pouziti ruznych cidel
  #define USE_FILTER_PRESSURE     1           // Pouziva se mereni tlaku na vzduchovych filtrech
  #define USE_CO2                 1           // Pouziva se mereni CO2
//  #define USE_HUMIDITY            1           // Pouziva se mereni vlhkosti
  #define NO_DIESEL               1           // nepouziva se diesel, pouze motor auta
//-----------------------------------------------------------------------------
#elif (VERSION_HW == 11)

  // Napajeni 12 nebo 24 V
  #define ACCU_MIN_VOLTAGE        23          //  Minimalni napeti pro 24V napajeni
  #define ACCU_MIN_VOLTAGE_DIESEL 25          //  Minimalni napeti pro 24V napajeni pri behu dieselu nebo motoru vozidla

  // Pouziti podlahovych serv
  #define USE_FLOOR_FLAPS         1           // Pouzivaji se podlahove klapky, zobrazuju je a kontroluju poruchu
//  #define USE_FLOOR_FLAPS_CONTROL 1           // Podlahove klapky lze ovladat oddelene od naporovych, tj. na klapku lze kliknout a nastavit
//  #define FLOOR_FLAP_ULTRALIGHT   1           // Podlahova klapka Ultralightu, otevira se jen cca do pulky

  // Pouziti bufiku misto teplovodniho topeni se servem. Bufik se reguluje pouze zapinanim a vypinanim (stejne jako chlazeni).
  #define USE_ON_OFF_HEATING      1           // Pouzivaji se bufiky

  // Pouziti chlazeni
  #define USE_COOLING             1           // Pouziva se chlazeni

  // Pouziti 2 nebo 3 teplotnich cidel ve skrini
  #define USE_TWO_SENSORS         1           // Pouzivaji se jen 2 teplotni cidla

  // Pouziti ruznych cidel
  #define USE_FILTER_PRESSURE     1           // Pouziva se mereni tlaku na vzduchovych filtrech
  #define USE_CO2                 1           // Pouziva se mereni CO2
//  #define USE_HUMIDITY            1           // Pouziva se mereni vlhkosti
  #define NO_DIESEL               1           // nepouziva se diesel, pouze motor auta
//-----------------------------------------------------------------------------
#elif (VERSION_HW == 12)

    // Napajeni 12 nebo 24 V
  #define ACCU_MIN_VOLTAGE        11          // Minimalni napeti pro 12V napajeni
  #define ACCU_MIN_VOLTAGE_DIESEL 12          // Minimalni napeti pro 12V napajeni pri behu dieselu nebo motoru vozidla

  // Pouziti podlahovych serv
  #define USE_FLOOR_FLAPS         1           // Pouzivaji se podlahove klapky, zobrazuju je a kontroluju poruchu
//  #define USE_FLOOR_FLAPS_CONTROL 1           // Podlahove klapky lze ovladat oddelene od naporovych, tj. na klapku lze kliknout a nastavit
  #define FLOOR_FLAP_ULTRALIGHT   1           // Podlahova klapka Ultralightu, otevira se jen cca do pulky

  // Pouziti bufiku misto teplovodniho topeni se servem. Bufik se reguluje pouze zapinanim a vypinanim (stejne jako chlazeni).
  #define USE_ON_OFF_HEATING      1           // Pouzivaji se bufiky

  // Pouziti chlazeni
  #define USE_COOLING             1           // Pouziva se chlazeni

  // Pouziti 2 nebo 3 teplotnich cidel ve skrini
  #define USE_TWO_SENSORS         1           // Pouzivaji se jen 2 teplotni cidla

  // Pouziti ruznych cidel
  #define USE_FILTER_PRESSURE     1           // Pouziva se mereni tlaku na vzduchovych filtrech
  #define USE_CO2                 1           // Pouziva se mereni CO2
  #define USE_HUMIDITY            1           // Pouziva se mereni vlhkosti
  #define NO_DIESEL               1           // nepouziva se diesel, pouze motor auta
#elif (VERSION_HW == 13)

    // Napajeni 12 nebo 24 V
  #define ACCU_MIN_VOLTAGE        11          // Minimalni napeti pro 12V napajeni
  #define ACCU_MIN_VOLTAGE_DIESEL 12          // Minimalni napeti pro 12V napajeni pri behu dieselu nebo motoru vozidla

  // Pouziti podlahovych serv
  #define USE_FLOOR_FLAPS         1           // Pouzivaji se podlahove klapky, zobrazuju je a kontroluju poruchu
  //#define USE_FLOOR_FLAPS_CONTROL 1           // Podlahove klapky lze ovladat oddelene od naporovych, tj. na klapku lze kliknout a nastavit
  //#define FLOOR_FLAP_ULTRALIGHT   1           // Podlahova klapka Ultralightu, otevira se jen cca do pulky

  // Pouziti bufiku misto teplovodniho topeni se servem. Bufik se reguluje pouze zapinanim a vypinanim (stejne jako chlazeni).
  #define USE_ON_OFF_HEATING      1           // Pouzivaji se bufiky

  // Pouziti chlazeni
  #define USE_COOLING             1           // Pouziva se chlazeni

  // Pouziti 2 nebo 3 teplotnich cidel ve skrini
  #define USE_TWO_SENSORS         1           // Pouzivaji se jen 2 teplotni cidla

  // Pouziti ruznych cidel
  #define USE_FILTER_PRESSURE     1           // Pouziva se mereni tlaku na vzduchovych filtrech
  #define USE_CO2                 1           // Pouziva se mereni CO2
  #define USE_HUMIDITY            1           // Pouziva se mereni vlhkosti
  #define NO_DIESEL               1           // nepouziva se diesel, pouze motor auta

#elif (VERSION_HW == 14)

  // Napajeni 12 nebo 24 V
  #define ACCU_MIN_VOLTAGE        23          //  Minimalni napeti pro 24V napajeni
  #define ACCU_MIN_VOLTAGE_DIESEL 25          //  Minimalni napeti pro 24V napajeni pri behu dieselu nebo motoru vozidla

  // Pouziti podlahovych serv
  #define USE_FLOOR_FLAPS         1           // Pouzivaji se podlahove klapky, zobrazuju je a kontroluju poruchu
//  #define USE_FLOOR_FLAPS_CONTROL 1           // Podlahove klapky lze ovladat oddelene od naporovych, tj. na klapku lze kliknout a nastavit
  #define FLOOR_FLAP_ULTRALIGHT   1           // Podlahova klapka Ultralightu, otevira se jen cca do pulky

  // Pouziti bufiku misto teplovodniho topeni se servem. Bufik se reguluje pouze zapinanim a vypinanim (stejne jako chlazeni).
  #define USE_ON_OFF_HEATING      1           // Pouzivaji se bufiky

  // Pouziti chlazeni
  #define USE_COOLING             1           // Pouziva se chlazeni

  // Pouziti 2 nebo 3 teplotnich cidel ve skrini
  #define USE_TWO_SENSORS         1           // Pouzivaji se jen 2 teplotni cidla

  // Pouziti ruznych cidel
  #define USE_FILTER_PRESSURE     1           // Pouziva se mereni tlaku na vzduchovych filtrech
  #define USE_CO2                 1           // Pouziva se mereni CO2
//  #define USE_HUMIDITY            1           // Pouziva se mereni vlhkosti
  #define NO_DIESEL               1           // nepouziva se diesel, pouze motor auta
//-----------------------------------------------------------------------------

#elif (VERSION_HW == 15)

  // Napajeni 12 nebo 24 V
  #define ACCU_MIN_VOLTAGE        23          //  Minimalni napeti pro 24V napajeni
  #define ACCU_MIN_VOLTAGE_DIESEL 25          //  Minimalni napeti pro 24V napajeni pri behu dieselu nebo motoru vozidla

  // Pouziti podlahovych serv
  #define USE_FLOOR_FLAPS         1           // Pouzivaji se podlahove klapky, zobrazuju je a kontroluju poruchu
//  #define USE_FLOOR_FLAPS_CONTROL 1           // Podlahove klapky lze ovladat oddelene od naporovych, tj. na klapku lze kliknout a nastavit
  #define FLOOR_FLAP_ULTRALIGHT   1           // Podlahova klapka Ultralightu, otevira se jen cca do pulky

  // Pouziti bufiku misto teplovodniho topeni se servem. Bufik se reguluje pouze zapinanim a vypinanim (stejne jako chlazeni).
  #define USE_ON_OFF_HEATING      1           // Pouzivaji se bufiky

  // Pouziti chlazeni
  #define USE_COOLING             1           // Pouziva se chlazeni

  // Pouziti 2 nebo 3 teplotnich cidel ve skrini
  #define USE_TWO_SENSORS         1           // Pouzivaji se jen 2 teplotni cidla

  // Pouziti ruznych cidel
  #define USE_FILTER_PRESSURE     1           // Pouziva se mereni tlaku na vzduchovych filtrech
  #define USE_CO2                 1           // Pouziva se mereni CO2
  #define USE_HUMIDITY            1           // Pouziva se mereni vlhkosti
  #define NO_DIESEL               1           // nepouziva se diesel, pouze motor auta
//-----------------------------------------------------------------------------

#endif

#endif // __HwDef_H__
