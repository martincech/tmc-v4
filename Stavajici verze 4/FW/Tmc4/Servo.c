//*****************************************************************************
//
//    Servo.c - Kontrola stavu serv
//    Version 2.0, (c) Vymos
//
//*****************************************************************************

#include "Servo.h"
#include "Aout.h"              // Analogove vystupy
#include "Alarm.h"             // Indikace poruch
#include "Control.h"           // Automaticke rizeni

TServoStatus __xdata__ ServoStatus[_SERVO_COUNT]; // Stavy jednotlivych serv

#ifndef __TMCREMOTE__

static __xdata__ word timer[ _SERVO_COUNT];

// Lokalni funkce :

static byte ReadControl( byte address);
// Cteni ridiciho napeti

//-----------------------------------------------------------------------------
// Inicializace serv
//-----------------------------------------------------------------------------

void ServoInit( void)
// Inicializuje modul serv
{
  byte i;

  for( i = 0; i < _SERVO_COUNT; i++){
    timer[ i] = 0;
    ServoStatus[i] = SERVO_STATUS_OK;
  }
} // ServoInit

//-----------------------------------------------------------------------------
// Nulovani timeru
//-----------------------------------------------------------------------------

void ServoResetTimer(byte Cislo) {
  // Vynuluje timer u serva cislo <Cislo> - pouziva se pri prepnuti rezimu
  timer[Cislo] = 0;
}

//-----------------------------------------------------------------------------
// Test zavreni serva
//-----------------------------------------------------------------------------

#ifdef USE_COOLING

TYesNo ServoFullyClosed(byte Cislo) {
  // Pokud je servo <Cislo> plne zavrene, vrati YES.
  // Toleranci beru 10% rozsahu
  return (TYesNo)(AinServo[Cislo] < SERVO_MIN + (SERVO_MAX - SERVO_MIN) / 10);
}

#endif // USE_COOLING

//-----------------------------------------------------------------------------
// Hlidani serva
//-----------------------------------------------------------------------------

void ServoCheck( void)
// Kontroluje cinnost serv. Volat 1x za sekundu
{
byte i;
byte control_value;
byte measure_value;
byte difference;        // absolutni rozdil
TYesNo ServoMimoPolohu;    // Flag, ze je dane servo mimo nastavenou polohu a ma se citat doba timeoutu

   // Pruchod pres vsechna serva :
   for (i = 0; i < _SERVO_COUNT; i++) {
      control_value = ReadControl( i);
      measure_value = AinServo[i];

#ifdef EMC_TEST_MODE
  ServoStatus[i] = SERVO_STATUS_OK;
  continue;
#endif

#ifndef USE_FLOOR_FLAPS
      // Nepouzivaji se podlahove klapky, tj. ani porucha nemuze byt
      if (i == SERVO_FLOOR) {
        ServoStatus[i] = SERVO_STATUS_OK;
        continue;       // Pokracuju kontrolou dalsiho serva
      }
#endif // USE_FLOOR_FLAPS

#ifdef USE_ON_OFF_HEATING
      // Pouzivaji se bufiky, servo topeni neni nainstalovane, tj. nemuze byt jeho porucha
      if (i == SERVO_HEATING) {
        ServoStatus[i] = SERVO_STATUS_OK;
        continue;       // Pokracuju kontrolou dalsiho serva
      }
#endif // USE_ON_OFF_HEATING

      if( control_value < SERVO_OFF_LIMIT){
         // utrzeny potenciometr, ridici napeti pod urovni
         ServoStatus[i] = SERVO_STATUS_CONTROL;
         if (i != SERVO_HEATING) {
           // Alarm nastavim jen pro vzduchove serva, poruchu serva topeni resim zvlast v Heating.c51
           AlarmSet();             // Nastavim alarm
         }
         continue;
      }
      if( measure_value < SERVO_OFF_LIMIT){
         // zpetne napeti, mensi nez limit, servo vypnuto. POrucvhu pro vzduchova serva nenastavuju, zobrazi se jen krizek. Servo topeni se resi zvlast.
         ServoStatus[i] = SERVO_STATUS_OFF;
         continue;
      }

      // Servo v provozu, kontroluj: zmerene napeti musi odpovidat ridicimu s presnosti SERVO_ACCURACY
      ServoMimoPolohu = NO;    // Default je servo ve spravne poloze
      difference = control_value > measure_value ?
                   control_value - measure_value :
                   measure_value - control_value;

#ifdef FLOOR_FLAP_ULTRALIGHT
      // U Ultralightu se podlahova klapka otevira pouze o 45 stupnu, v rozsahu 2-6V. Bohuzel se ale servo na desce 6P krmi stejnym ridicim signalem jako naporova klapka, tj. 2-10V.
      // Zpetny signal se ale zasekne na 6V a dal neroste. Poruchu tedy kontroluju pouze do ridiciho signalu 6V, nad 6V musi byt zpetny aspon 6V, ale uz neroste.
      // Toto pujde odstranit, az kdyz se oddeli ridici signal pro naporove klapky a pro podlahovou klapku. Do te doby se t bude chovat takto blbe.
      if (control_value <= SERVO_UL_FLOOR_MAX) {
        // V rozsahu 2-6V, kontroluju soubeh ridiciho a kontrolniho signalu
        if (difference > SERVO_UL_FLOOR_ACCURACY) {
          // Nesouhlasi polohy, citej cas
          ServoMimoPolohu = YES;
        }
      } else {
        // Ridici signal je vic jak 6V, zpetny signal by mel byt zaseknuty na 6V s presnosti SERVO_UL_FLOOR_ACCURACY
        if (measure_value < SERVO_UL_FLOOR_MAX - SERVO_UL_FLOOR_ACCURACY) {
          // Nesouhlasi polohy, citej cas
          ServoMimoPolohu = YES;
        }
      }

#else
      // Standardni klapka, ktera se otevira naplno o 90 stupnu, tj. v rozsahu 2-10V
      if (difference > SERVO_ACCURACY) {
        // Nesouhlasi polohy, citej cas
        ServoMimoPolohu = YES;
      }//if
#endif

      if (ServoMimoPolohu) {
        // Servo je mimo nastavenou polohu, citam cas, po ktery je mimo polohu
        timer[ i]++;
        if (timer[ i] == 0) {
          timer[ i] = 0xFFFF;                                  // limitace pri pretoku
        }
        if (timer[ i] > SERVO_RESPONSE) {
          ServoStatus[i] = SERVO_STATUS_TIMEOUT;  // prekrocen cas
          if (i != SERVO_HEATING) {
            // Alarm nastavim jen pro vzduchove serva, poruchu serva topeni resim zvlast
            AlarmSet();             // Nastavim alarm
          }
        } else {
          ServoStatus[i] = SERVO_STATUS_RUNNING;  // zatim v casovem limitu
        }
      } else {
        // Poloha serva je v poradku
        timer[i] = 0;
        ServoStatus[i] = SERVO_STATUS_OK;
      }//else
   }
} // ServoCheck

//-----------------------------------------------------------------------------
// Cteni rizeni
//-----------------------------------------------------------------------------

static byte CalculateInput(byte Output) {
  // Podle zadane hodnoty DA prevodniku 0..DAC_VALUE_MAX vypocte odpovidajici AD prevod 0..ADC_RANGE
  return (word)((word)Output * (word)ADC_RANGE / (word)DAC_VALUE_MAX);
}

static byte ReadControl( byte address)
// Cteni ridiciho napeti (analogoveho vystupu)
{
   switch (address) {
     default:
     case SERVO_INDUCTION1:
     case SERVO_INDUCTION2:
     case SERVO_RECIRCULATION:
        return (CalculateInput(AoutInductionServo));

     case SERVO_FLOOR:
        return (CalculateInput(AoutFloorServo));

     case SERVO_HEATING:
        return (CalculateInput(AoutHeatingServo));
   }
} // ReadControl

#endif // __TMCREMOTE__

