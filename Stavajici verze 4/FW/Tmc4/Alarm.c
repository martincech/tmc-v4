//*****************************************************************************
//
//    Alarm.c - Ovladani alarmu podle poruch
//    Version 1.0
//
//*****************************************************************************

#include "Alarm.h"
#include "Beep.h"              // Repro v jednotce
#include "Dout.h"              // Digitalni vystupy
#include "ExtSiren.h"          // Ovladani externi sireny

TAlarm __xdata__ Alarm;

// Piskani vestaveneho reproduktoru v jednotce
static byte __xdata__ BeepCounter;      // Citac piskani
#define BEEP_PERIOD     10              // Perioda piskani v sekundach

// Ztlumeni sireny
static word __xdata__ SilentCounter;    // Citac ztlumeni piskani - pozor, musi byt word
#define SILENT_PERIOD     300           // Perioda prvniho ztlumeni sireny v sekundach

#define DEFAULT_MAX_FAULT_TIME 120               // Maximalni doba trvani poruchy v sekundach 

//-----------------------------------------------------------------------------
// Inicializace
//-----------------------------------------------------------------------------

void AlarmInit() {
  // Inicializuje alarmu
  Alarm.SirenState = SIREN_FIRST_BEEPING;       // Repro je pripraven k piskani
  Alarm.Quiet      = NO;                        // Repro zapnuty
  Alarm.ShowAlarm  = NO;                        // Neindikuje se alarm
  BeepCounter = 0;                              // Nuluju pocitadlo piskani
#ifndef __TMCREMOTE__
  ExtSirenInit();                               // Inicializace externi sireny
#endif // __TMCREMOTE__
}

//-----------------------------------------------------------------------------
// Zapnuti/vypnuti reproduktoru a majaku (hlaseni alarmu)
//-----------------------------------------------------------------------------

#ifndef __TMCREMOTE__

void AlarmChange() {
  // Zapne / vypne hlaseni alarmu

  Alarm.Quiet = !Alarm.Quiet;

  if (Alarm.ShowAlarm && Alarm.Quiet) {
    // Alarm je aktivni, piska a obsluha prave sirenu ztlumila
    switch (Alarm.SirenState) {
      case SIREN_FIRST_BEEPING:
        // Poprve ztlumil sirenu
        Alarm.SirenState = SIREN_FIRST_QUIET;   // Cekam po urcitou dobu
        SilentCounter    = 0;                   // Zacinam pocitat od zacatku
        break;

      case SIREN_SECOND_BEEPING:
        // Podruhe ztlumil sirenu, necham ji ztlumenou naporad
        Alarm.SirenState = SIREN_SECOND_QUIET;
        break;
    }
    return;
  }

  // V ostatnich pripadech nastavim automat ztlumeni sireny na zacatek
  Alarm.SirenState = SIREN_FIRST_BEEPING;       // Repro je pripraven k piskani
}

#endif // __TMCREMOTE__

//-----------------------------------------------------------------------------
// Nulovani alarmu
//-----------------------------------------------------------------------------

#ifndef __TMCREMOTE__

void AlarmClear() {
  // Vynuluje alarm (volat periodicky na zacatku kazdeho mericiho cyklu)
  Alarm.ShowAlarm = NO;
}

#endif // __TMCREMOTE__

//-----------------------------------------------------------------------------
// Obsluha alarmu
//-----------------------------------------------------------------------------

void AlarmExecute() {
  // Obsluha piskani reproduktoru a blikani majaku pri alarmu, volat kazdou sekundu na konci mericiho cyklu

#ifdef EMC_TEST_MODE
  AlarmClear();
#endif

  if (!Alarm.ShowAlarm) {
    // Neni alarm
    TmcMajak(NO);                   // Vypnu majak
#ifndef __TMCREMOTE__
    ExtSirenOff();              // Vypnu externi sirenu
#endif // __TMCREMOTE__
    BeepCounter = BEEP_PERIOD - 1;  // Pripravim si pocitadlo tak, aby to pisklo ihned jak se poprve vyskytne alarm (aby to necekalo nekolik sekund)
    Alarm.SirenState = SIREN_FIRST_BEEPING;     // Repro je pripraven k piskani
    Alarm.Quiet      = NO;                      // Pri kazdem novem vyskytu alarmu hned sirenu rozjedu
    return;
  }

  // Je alarm

  // Zapnu majak, ten spinam pri alarmu vzdy, nehlede na to, zda ma v TMC reproduktor ztlumeny nebo ne
  TmcMajak(YES);

  // Automat ztlumeni sireny
  if (Alarm.SirenState == SIREN_FIRST_QUIET) {
    // Sirena je poprve ztlumena
    if (SilentCounter > SILENT_PERIOD) {
      // Ubehla perioda prvniho ztlumeni a alarm porad trva, znovu sirenu rozjedu
      Alarm.Quiet      = NO;
      Alarm.SirenState = SIREN_SECOND_BEEPING;
    } else {
      // Zatim cekam dal a sirenu necham ztlumenou
      SilentCounter++;
    }
  }

  if (Alarm.Quiet) {
    // Alarm je aktivni, ale nechce piskat
#ifndef __TMCREMOTE__
    ExtSirenOff();       // Vypnu externi sirenu
#endif // __TMCREMOTE__
    BeepCounter = BEEP_PERIOD - 1;  // Pripravim si pocitadlo tak, aby to pisklo ihned jak se poprve vyskytne alarm (aby to necekalo nekolik sekund)
    return;
  }

  // Reproduktor neni ztlumeny

  // Obsluha externi sireny
#ifndef __TMCREMOTE__
  ExtSirenExecute();        // Zapnu externi sirenu
#endif // __TMCREMOTE__

  // Reproduktor
  BeepCounter++;
  if (BeepCounter >= BEEP_PERIOD) {
    // Ubehla perioda piskani
    BeepCounter = 0;            // Nulovani citace
    BeepAlarm();                // Pisknu
  }
}

//-----------------------------------------------------------------------------
// Nastaveni alarmu
//-----------------------------------------------------------------------------

void AlarmSet() {
  // Nastavi alarm pri nejake poruse
  Alarm.ShowAlarm = YES;
}

#ifndef __TMCREMOTE__
TYesNo AlarmCheckFaultCounterMaxTime(TYesNo isFault, byte __xdata__ *faultCounter, byte *faultIndicator, byte maxFaultTime){
  if (isFault) {
    // Je v poru�e
    (*faultCounter)++;
    if ((*faultCounter) >= maxFaultTime) {
      // Porucha uz trva dlouho, vyhlasim alarm
      (*faultCounter)  = maxFaultTime;    // Aby pocitadlo nepreteklo
      (*faultIndicator)= YES;
      AlarmSet();
      return YES;
    }
  } else {
    // Napeti je v poradku
    (*faultIndicator) = NO;
    (*faultCounter)   = 0;          // Pocitam opet od nuly
  }  
  return NO;
}

TYesNo AlarmCheckFaultCounter(TYesNo isFault, byte __xdata__ *faultCounter, byte *faultIndicator)
// po��tadlo poruchy
{
   return AlarmCheckFaultCounterMaxTime(isFault, faultCounter, faultIndicator, DEFAULT_MAX_FAULT_TIME);
}
#endif