//*****************************************************************************
//
//    DutyCyc.c - Mereni stridy
//    Version 1.0
//
//*****************************************************************************


#ifndef __DutyCyc_H__
   #define __DutyCyc_H__

#include "Hardware.h"     // pro podminenou kompilaci

// Struktura stridy
typedef struct {
  word OnCounter;                       // Pocitadlo casu ve stavu On
  word OffCounter;                      // Pocitadlo casu ve stavu Off
  byte Percent;                         // Strida v procentech
} TDutyCycle;

void DutyCycleReset(TDutyCycle xdata *DutyCycle);
  // Snulovani pocitadel

void DutyCycleExecute(TDutyCycle xdata *DutyCycle, TYesNo IsStateOn);
  // Krok stridy, volat 1x za sekundu


#endif
