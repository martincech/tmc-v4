#
# Inprise C++Builder - (C) Copyright 1999 by Borland International
#

CC       := c51
AS       := a51
LINK     := bl51
OH       := oh51
LIB      := lib51
MAKE     := make

# C51 compiler flags
CFLAGS:=OT(11, SIZE) WL(0) $(DEFINES)
# A51 compiler flags
AFLAGS:=
# BL51 linker flags
LFLAGS:= RS(256) PL(68) PW(78)

%.obj: %.a51
	-$(AS) $< $(AFLAGS)

%.obj: %.c
	-$(CC) $< $(CFLAGS)

%.hex: $(basename %)
	$(OH) $<

%.lib:
	-$(MAKE) -f $(dir $@)\Makefile